package idv.equalhsiao.remote.test.po;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BalanceSheetRepository extends JpaRepository<BalanceSheetEntity, Integer> {

	List<BalanceSheetEntity> findByYearAndSeason(String year, Short season);

	List<BalanceSheetEntity> findByYearAndSeasonAndAnnouncementDateBetween(String year, Short season, Date startDate,
			Date endDate);

	List<BalanceSheetEntity> findByStockIdInAndYearAndSeason(List<String> stockIds, String year, Short season);

	BalanceSheetEntity findByStockIdAndYearAndSeason(String stockId,String year,Short season);
	
	Long countByYearAndSeason(String year,Short season);
}
