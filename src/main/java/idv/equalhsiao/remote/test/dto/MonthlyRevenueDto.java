package idv.equalhsiao.remote.test.dto;

import lombok.Data;

@Data
public class MonthlyRevenueDto {
	private String stockId;
	private String stockName;
	private String industry;
	private Integer volume;
	private Float closePrice;
	private Float preMonthlyComparePrecentage;
	private Float preYearComparePrecentage;
	private Float preMonthlyCompareGrandTotalPrecentage;
}
