package idv.equalhsiao.remote.test.po;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "incomestatement")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class IncomeStatementEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String stockId;
	private String stockName;
	private String industry;
	private Long income;
	private Long operatingCost;
	private Long unrealizedSales;
	private Long realizedSales;
	private Long netOperatingProfit;
	private Long operatingExpenses;
	private Long otherIncome;
	private Long businessInterest;
	private Long nonOperatingIncome;
	private Long netProfitBeforeTax;
	private Long incomeTaxExpense;
	private Long periodIncome;
	private Long discountinuedUnitProfit;
	private Long otherComprehensiveProfit;
	private Long parentCompanyPeriodIncome;
	private Long comprehensiveParentCompanyPeriodIncome;
	private Float eps;
	private String year;
	private Short season;
	private Timestamp announcementDate;

}
