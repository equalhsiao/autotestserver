package idv.equalhsiao.remote.test.po;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "futures")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class FuturesEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String type;
	private String category;
	private String exchange;
	private String name;
	private String quotationUnit;
	private Float closePrice;
	private Float riseFall;
	private Float riseFallPercentage;
	private Float openingPrice;
	private Float highestPrice;
	private Float lowestPrice;
	private Integer volume;
	private Integer openPosition;
	private String getDate;
}
