package idv.equalhsiao.remote.test.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StockPageController {

	@GetMapping(value = "stock")
	public String stock() {
		return "stock";
	}
	
	@GetMapping(value = "chart")
	public String chart() {
		return "chart";
	}
}
