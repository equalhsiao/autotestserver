package idv.equalhsiao.remote.test.dto;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class ImportantMessageDto {
	private String stockId;
	private String stockName;
	private Integer volume;
	private Float closePrice;
	private Integer serialNumber;
	private Integer currentIncome;
	private Float preYearIncomePercentage;
	private Integer preSeasonIncome;
	private Float preSeasonIncomePercentage;
	private Integer recentFourSeasonIncome;

	private Integer currentNetProfitBeforeTax;
	private Float preYearNetProfitBeforeTaxPercentage;
	private Integer preSeasonNetProfitBeforeTax;
	private Float preSeasonNetProfitBeforeTaxPercentage;
	private Integer recentFourSeasonNetProfitBeforeTax;

	private Integer currentPeriodIncome;
	private Float preYearPeriodIncomePercentage;
	private Integer preSeasonPeriodIncome;
	private Float preSeasonPeriodIncomePercentage;
	private Integer recentFourSeasonPeriodIncome;

	private Float currentEPS;
	private Float preYearEPSPercentage;
	private Float preSeasonEPS;
	private Float preSeasonEPSPercentage;
	private Float recentFourSeasonEPS;

	private Timestamp announcementDate;
	private String year;
	private Integer month;
	private Integer day;
}
