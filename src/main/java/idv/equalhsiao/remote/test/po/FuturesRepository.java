package idv.equalhsiao.remote.test.po;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FuturesRepository extends JpaRepository<FuturesEntity, Integer> {

	FuturesEntity findByCategoryAndNameAndExchangeAndGetDate(String category, String name, String exchange,
			String getDate);

	List<FuturesEntity> findByTypeAndGetDate(String type, String getDate);
}
