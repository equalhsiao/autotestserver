package idv.equalhsiao.remote.test.po;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "importantmessage")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class ImportantMessageEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String stockId;
	private String stockName;
	private Integer serialNumber;
	private Integer currentIncome;
	private Float preYearIncomePercentage;
	private Integer preSeasonIncome;
	private Float preSeasonIncomePercentage;
	private Integer recentFourSeasonIncome;

	private Integer currentNetProfitBeforeTax;
	private Float preYearNetProfitBeforeTaxPercentage;
	private Integer preSeasonNetProfitBeforeTax;
	private Float preSeasonNetProfitBeforeTaxPercentage;
	private Integer recentFourSeasonNetProfitBeforeTax;

	private Integer currentPeriodIncome;
	private Float preYearPeriodIncomePercentage;
	private Integer preSeasonPeriodIncome;
	private Float preSeasonPeriodIncomePercentage;
	private Integer recentFourSeasonPeriodIncome;

	private Float currentEps;
	private Float preYearEpsPercentage;
	private Float preSeasonEps;
	private Float preSeasonEpsPercentage;
	private Float recentFourSeasonEps;

	private Timestamp announcementDate;
	private String year;
	private Integer month;
	private Integer day;
}
