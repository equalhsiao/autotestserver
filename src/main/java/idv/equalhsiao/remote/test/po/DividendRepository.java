package idv.equalhsiao.remote.test.po;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DividendRepository extends JpaRepository<DividendEntity, Integer> {

	List<DividendEntity> findByAnnouncementDateBetweenAndYear(Date announcementDateStart, Date announcementDateEnd,
			String year);

//	List<DividendEntity> findByYearAndIndustryAndAnnouncementDateBetween(String year, String industry,
//			Date announcementDateStart, Date announcementDateEnd);

	List<DividendEntity> findByStockIdInAndYear(List<String> stockIds,String year);

	DividendEntity findByStockIdAndYearAndDividendYear(String stockId, String year, String dividendYear);

	List<DividendEntity> findByYear(String year);

}
