package idv.equalhsiao.remote.test.po;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IncomeStatementRepository extends JpaRepository<IncomeStatementEntity, Integer> {

	@Query(value = "SELECT stock_id FROM incomestatement WHERE year= :year AND season= :season AND  announcement_date BETWEEN :announcementDateStart AND :announcementDateEnd", nativeQuery = true)
	List<String> findStockIdByYearAndSeasonAndAnnouncementDate(@Param("year") String year,
			@Param("season") Short season, @Param("announcementDateStart") Date announcementDateStart,
			@Param("announcementDateEnd") Date announcementDateEnd);

	@Query(value = "SELECT stock_id FROM incomestatement WHERE year= :year AND season= :season ", nativeQuery = true)
	List<String> findStockIdByYearAndSeason(@Param("year") String year, @Param("season") Short season);

	@Query(value = "SELECT stock_id FROM incomestatement WHERE year= :year AND season= :season AND industry= :industry AND  announcement_date BETWEEN :announcementDateStart AND :announcementDateEnd", nativeQuery = true)
	List<String> findStockIdByYearAndSeasonAndIndustryAndAnnouncementDate(@Param("year") String year,
			@Param("season") Short season, @Param("industry") String industry,
			@Param("announcementDateStart") Date announcementDateStart,
			@Param("announcementDateEnd") Date announcementDateEnd);

	@Query(value = "SELECT stock_id FROM incomestatement WHERE year= :year AND season= :season AND industry= :industry ", nativeQuery = true)
	List<String> findStockIdByYearAndSeasonAndIndustry(@Param("year") String year, @Param("season") Short season,
			@Param("industry") String industry);

	IncomeStatementEntity findByStockIdAndYearAndSeason(String stockId, String year, Short season);

	List<IncomeStatementEntity> findByYearAndSeason(String year, Short season);

	List<IncomeStatementEntity> findByYearAndSeasonAndAnnouncementDateBetween(String year, Short season,
			Date announcementDateStart, Date announcementDateEnd);

	List<IncomeStatementEntity> findByYearAndSeasonAndAnnouncementDateBetween(String year, Short season,
			Timestamp announcementDateStart, Timestamp announcementDateEnd);

	List<IncomeStatementEntity> findByYearAndSeasonAndIndustryAndAnnouncementDateBetween(String year, Short season,
			String industry, Date announcementDateStart, Date announcementDateEnd);

	List<IncomeStatementEntity> findByYearAndSeasonAndIndustry(String year, Short season, String industry);

	List<IncomeStatementEntity> findByStockIdInAndYearAndSeason(List<String> stockIds, String year, Short season);

	List<IncomeStatementEntity> findByStockIdAndYearAndSeasonInOrderBySeasonDesc(String stockId, String year,
			List<Short> season);
}
