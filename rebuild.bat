"C:\Program Files\Git\bin\bash.exe" --login -c "docker stop autotestserver && docker rm autotestserver"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker rmi autotestserver"
"C:\Program Files\Git\bin\bash.exe" --login -c "mvn install -Dmaven.test.skip=true "
"C:\Program Files\Git\bin\bash.exe" --login -c "docker build -t autotestserver ."
"C:\Program Files\Git\bin\bash.exe" --login -c "docker run -d -p 8033:8033 --name autotestserver -e SPRING_ACC_INITSTARTTEST=true -v /${PWD}/newman:/app/newman  autotestserver:latest /bin/bash"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker logs autotestserver -f"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker exec -it autotestserver bash"
pause