package idv.equalhsiao.remote.test.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix="spring.router")
public class UserProperties {
	private String username;
	private String password;
	private String initpage;
	private String ip;
}
