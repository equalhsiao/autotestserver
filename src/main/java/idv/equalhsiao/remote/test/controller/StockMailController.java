package idv.equalhsiao.remote.test.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.context.Context;

import idv.equalhsiao.remote.test.config.CommonResp;
import idv.equalhsiao.remote.test.dto.BalanceSheetDto;
import idv.equalhsiao.remote.test.dto.DividendDto;
import idv.equalhsiao.remote.test.dto.ImportantMessageDto;
import idv.equalhsiao.remote.test.dto.IncomeStatementDto;
import idv.equalhsiao.remote.test.dto.Mail;
import idv.equalhsiao.remote.test.dto.MonthlyRevenueDto;
import idv.equalhsiao.remote.test.po.BalanceSheetEntity;
import idv.equalhsiao.remote.test.po.BalanceSheetRepository;
import idv.equalhsiao.remote.test.po.DividendEntity;
import idv.equalhsiao.remote.test.po.DividendRepository;
import idv.equalhsiao.remote.test.po.FuturesEntity;
import idv.equalhsiao.remote.test.po.FuturesRepository;
import idv.equalhsiao.remote.test.po.ImportantMessageEntity;
import idv.equalhsiao.remote.test.po.ImportantMessageRepository;
import idv.equalhsiao.remote.test.po.IncomeStatementEntity;
import idv.equalhsiao.remote.test.po.IncomeStatementRepository;
import idv.equalhsiao.remote.test.po.MonthlyRevenueEntity;
import idv.equalhsiao.remote.test.po.MonthlyRevenueRepository;
import idv.equalhsiao.remote.test.po.StockIndustryPriceEntity;
import idv.equalhsiao.remote.test.po.StockIndustryPriceRepository;
import idv.equalhsiao.remote.test.service.MailService;
import idv.equalhsiao.remote.test.service.StockService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "stock")
@Slf4j
public class StockMailController {

	@Autowired
	private FuturesRepository futuresRepository;

	@Autowired
	private ImportantMessageRepository importantMessageRepsitory;

	@Autowired
	private BalanceSheetRepository balanceSheetRepository;

	@Autowired
	private IncomeStatementRepository incomeStatementRepository;

	@Autowired
	private DividendRepository dividendRepository;

	@Autowired
	private MonthlyRevenueRepository monthlyRevenueRepository;

	@Autowired
	private StockIndustryPriceRepository stockIndustryPriceRepository;

	@Autowired
	private StockService stockService;

	@Autowired
	private MailService mailService;

	// 寄期貨email
	@GetMapping(value = "/sendFutures")
	private CommonResp<Map<String, Object>> sendFutures() throws Exception {
		log.info("期貨寄信開始");
		Calendar cal = Calendar.getInstance();
		// 取得昨天的價格
		cal.add(Calendar.DATE, -1);
		Date now = cal.getTime();
		Integer month = now.getMonth() >= 11 ? 1 : now.getMonth() + 1;
		Integer day = now.getDate();
		String getDate = (month >= 10 ? "" + month : "0" + month) + "/" + (day >= 10 ? "" + day : "0" + day);
		log.info("期貨日期:" + getDate);
		List<FuturesEntity> list = futuresRepository.findByTypeAndGetDate("基本金屬", getDate);
		Map<String, List<FuturesEntity>> map = new HashMap<String, List<FuturesEntity>>();
		for (FuturesEntity e : list) {
			List<FuturesEntity> futuresList = map.getOrDefault(e.getCategory(), new ArrayList<FuturesEntity>());
			futuresList.add(e);
			map.put(e.getCategory(), futuresList);
		}
		if (map.size() > 0) {
			Mail mail = mailService.genMail("期貨 " + month + "月 " + day + "日");
			Context context = new Context();
			context.setVariable("templateName", "futures");
			context.setVariable("futuresMap", map);
			mailService.sendMail(mail, context);

		}
		log.info("期貨寄信結束");
		return new CommonResp();
	}

	// 寄中股行業email
	@GetMapping(value = "/sendCNStockIndustryPrice")
	private CommonResp<Map<String, Object>> sendCNStockIndustryPrice(
			@RequestParam(value = "year", required = false) String year,
			@RequestParam(value = "month", required = false) Integer month,
			@RequestParam(value = "day", required = false) Integer day) throws Exception {
		log.info("中股行業寄信開始");
		Calendar cal = Calendar.getInstance();
		// 取得昨天的價格
		cal.add(Calendar.DATE, -1);
		Date now = cal.getTime();
		// 轉成西元年
		if (year == null) {
			year = String.valueOf(Integer.valueOf(now.getYear()) + 1900);
		}
		if (month == null) {
			month = now.getMonth() + 1;
		}
		if (day == null) {
			day = now.getDate();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		List<StockIndustryPriceEntity> stockIndustryPriceList = stockIndustryPriceRepository
				.findByCountryAndUseDate("CN", sdf.parse(year + "/" + month + "/" + day));
		if (stockIndustryPriceList.size() > 0) {
			Mail mail = mailService.genMail("中股行業 " + year + "年 " + month + "月 " + day + "日");
			Context context = new Context();
			context.setVariable("templateName", "stockIndustryPrice");
			context.setVariable("country", "CN");
			context.setVariable("stockIndustryPriceList", stockIndustryPriceList);
			mailService.sendMail(mail, context);
		}
		log.info("中股行業寄信結束");
		return new CommonResp();

	}

	// 寄美股行業email
	@GetMapping(value = "/sendUSStockIndustryPrice")
	private CommonResp<Map<String, Object>> sendUSStockIndustryPrice(
			@RequestParam(value = "year", required = false) String year,
			@RequestParam(value = "month", required = false) Integer month,
			@RequestParam(value = "day", required = false) Integer day) throws Exception {
		log.info("美股行業寄信開始");
		Calendar cal = Calendar.getInstance();
		// 取得昨天的價格
		cal.add(Calendar.DATE, -1);
		Date now = cal.getTime();
		// 轉成西元年
		if (year == null) {
			year = String.valueOf(Integer.valueOf(now.getYear()) + 1900);
		}
		if (month == null) {
			month = now.getMonth() + 1;
		}
		if (day == null) {
			day = now.getDate();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		List<StockIndustryPriceEntity> stockIndustryPriceList = stockIndustryPriceRepository
				.findByCountryAndUseDate("US", sdf.parse(year + "/" + month + "/" + day));
		if (stockIndustryPriceList.size() > 0) {
			Mail mail = mailService.genMail("美股行業 " + year + "年 " + month + "月 " + day + "日");
			Context context = new Context();
			context.setVariable("templateName", "stockIndustryPrice");
			context.setVariable("country", "US");
			context.setVariable("stockIndustryPriceList", stockIndustryPriceList);
			mailService.sendMail(mail, context);
		}
		log.info("美股行業寄信結束");
		return new CommonResp();
	}

	// 寄重大訊息email
	@GetMapping(value = "/sendImportantMessage")
	private CommonResp<Map<String, Object>> sendImportantMessage(@RequestParam(value = "year") String year,
			@RequestParam(value = "month") Integer month, @RequestParam(value = "day", required = false) Integer day)
			throws Exception {
		Map<String, Calendar> dateMap = genStatrDateAndEndDate();
		Calendar startDate = dateMap.get("startDate");
		Calendar endDate = dateMap.get("endDate");
		if (day == null) {
			day = Calendar.getInstance().getTime().getDate() ;
		}
		log.info("重大訊息寄信" + year + "年 " + month + "月 " + day + "日 開始");
		List<ImportantMessageEntity> list = importantMessageRepsitory.findByYearAndMonthAndDay(year,
				month, day);

//		List<ImportantMessageEntity> list = importantMessageRepsitory.findByYearAndMonthAndDay(year, month, day);

		// 查詢股價
		Map<String, Map<String, String>> openDataPriceMap = stockService.getPriceFromOpenData();
		List<ImportantMessageDto> importantMessageDtoList = list.stream().map(e -> {
			ImportantMessageDto dto = new ImportantMessageDto();
			// 股價
			Map<String, String> openDataMap = openDataPriceMap.get(e.getStockId());
			if (openDataMap != null) {
				if (openDataMap.containsKey("volume") && !StringUtils.isEmpty(openDataMap.get("closePrice"))) {
					Integer volume = Integer.valueOf(openDataMap.get("volume")) / 1000;
					dto.setVolume(volume);
				}
				if (openDataMap.containsKey("closePrice") && !StringUtils.isEmpty(openDataMap.get("closePrice"))) {
					dto.setClosePrice(Float.valueOf(openDataMap.get("closePrice")));
				}
			}
			dto.setSerialNumber(e.getSerialNumber());
			dto.setStockId(e.getStockId());
			dto.setStockName(e.getStockName());
			dto.setYear(e.getYear());
			dto.setDay(e.getDay());
			dto.setMonth(e.getMonth());
			dto.setAnnouncementDate(e.getAnnouncementDate());

			dto.setCurrentEPS(e.getCurrentEps());
			dto.setCurrentIncome(e.getCurrentIncome());
			dto.setCurrentNetProfitBeforeTax(e.getCurrentNetProfitBeforeTax());
			dto.setCurrentPeriodIncome(e.getCurrentPeriodIncome());

			dto.setPreYearEPSPercentage(e.getPreYearEpsPercentage());
			dto.setPreYearIncomePercentage(e.getPreYearIncomePercentage());
			dto.setPreYearNetProfitBeforeTaxPercentage(e.getPreYearNetProfitBeforeTaxPercentage());
			dto.setPreYearPeriodIncomePercentage(e.getPreYearPeriodIncomePercentage());

			dto.setPreSeasonEPS(e.getPreSeasonEps());
			dto.setPreSeasonEPSPercentage(e.getPreSeasonEpsPercentage());
			dto.setPreSeasonIncome(e.getPreSeasonIncome());
			dto.setPreSeasonIncomePercentage(e.getPreSeasonIncomePercentage());
			dto.setPreSeasonNetProfitBeforeTax(e.getPreSeasonNetProfitBeforeTax());
			dto.setPreSeasonNetProfitBeforeTaxPercentage(e.getPreSeasonNetProfitBeforeTaxPercentage());
			dto.setPreSeasonPeriodIncome(e.getPreSeasonPeriodIncome());
			dto.setPreSeasonPeriodIncomePercentage(e.getPreSeasonPeriodIncomePercentage());

			dto.setRecentFourSeasonEPS(e.getRecentFourSeasonEps());
			dto.setRecentFourSeasonIncome(e.getRecentFourSeasonIncome());
			dto.setRecentFourSeasonNetProfitBeforeTax(e.getRecentFourSeasonNetProfitBeforeTax());
			dto.setRecentFourSeasonPeriodIncome(e.getRecentFourSeasonPeriodIncome());
			return dto;
		}).collect(Collectors.toList());
		if (list.size() > 0) {
			Mail mail = mailService.genMail("公布注意交易資訊 " + year + "年 " + month + "月 " + day + "日");
			Context context = new Context();
			context.setVariable("templateName", "importantMessage");
			context.setVariable("importantMessageList", importantMessageDtoList);
			mailService.sendMail(mail, context);
		}
		log.info(list.size() + "筆資料");

		log.info("重大訊息寄信" + year + "年 " + month + "月 " + day + "日 結束" );
		return new CommonResp();
	}

	// 寄資產損益表email
	@GetMapping(value = "/sendBalanceSheet")
	private CommonResp<Map<String, Object>> sendBalanceSheet(@RequestParam(value = "year") String year,
			@RequestParam(value = "season") Short season) throws Exception {
		log.info("資產損益表寄信開始");
		Map<String, Calendar> dateMap = genStatrDateAndEndDate();
		Calendar startDate = dateMap.get("startDate");
		Calendar endDate = dateMap.get("endDate");
		List<BalanceSheetEntity> list = balanceSheetRepository.findByYearAndSeasonAndAnnouncementDateBetween(year,
				season, startDate.getTime(), endDate.getTime());
		List<String> stockIds = list.stream().map(e -> e.getStockId()).collect(Collectors.toList());
		List<BalanceSheetDto> balanceSheetList = stockService.balanceSheet(stockIds, year, season);
		log.info(balanceSheetList.size() + "筆資料");
		if (balanceSheetList.size() > 0) {
			Mail mail = mailService.genMail("資產損益表 " + year + "年 第" + season + "季");
			Context context = new Context();
			context.setVariable("templateName", "balanceSheet");
			context.setVariable("year", year);
			context.setVariable("season", season);
			context.setVariable("balanceSheetList", balanceSheetList);
			mailService.sendMail(mail, context);
		}
		log.info("資產損益表寄信結束");
		return new CommonResp();
	}

	// 寄月營收email
	@GetMapping(value = "/sendMonthlyRevenue")
	private CommonResp<Map<String, Object>> sendMonthlyRevenue(@RequestParam(value = "year") String year,
			@RequestParam(value = "month") Integer month) throws Exception {
		log.info("月營收寄信開始");
		Map<String, Calendar> dateMap = genStatrDateAndEndDate();
		Calendar startDate = dateMap.get("startDate");
		Calendar endDate = dateMap.get("endDate");
		Integer revenueMonthly = month - 1;
		List<MonthlyRevenueEntity> list = monthlyRevenueRepository.findByAnnouncementTimeBetweenAndYearAndMonth(
				startDate.getTime(), endDate.getTime(), year, revenueMonthly);
		List<String> stockIds = list.stream().map(e -> e.getStockId()).collect(Collectors.toList());
		List<MonthlyRevenueDto> monthlyRevenueDtoList = stockService.monthlyRevenue(stockIds, year, revenueMonthly);
		log.info(monthlyRevenueDtoList.size() + "筆資料");
		Map<String,List<MonthlyRevenueDto>> monthlyRevenueMap = new HashMap<String,List<MonthlyRevenueDto>>();
		for(MonthlyRevenueDto monthlyRevenueDto : monthlyRevenueDtoList ) {
			String industry = monthlyRevenueDto.getIndustry();
			List<MonthlyRevenueDto> tmpList = monthlyRevenueMap.getOrDefault(industry, new ArrayList<MonthlyRevenueDto>());
			tmpList.add(monthlyRevenueDto);
			monthlyRevenueMap.put(industry, tmpList);
		}
		if (monthlyRevenueDtoList.size() > 0) {
			Mail mail = mailService.genMail("月營收報告 " + year + "年 " + revenueMonthly + "月");
			Context context = new Context();
			context.setVariable("templateName", "monthlyRevenue");
			context.setVariable("year", year);
			context.setVariable("monthlyRevenueMap", monthlyRevenueMap);
			mailService.sendMail(mail, context);
		}
		log.info("月營收寄信結束");
		return new CommonResp();
	}

	// 寄現金股利email
	@GetMapping(value = "/sendDividend")
	private CommonResp<Map<String, Object>> sendDividend(@RequestParam(value = "year") String year) throws Exception {
		log.info("現金股利寄信開始");
		Map<String, Calendar> dateMap = genStatrDateAndEndDate();
		Calendar startDate = dateMap.get("startDate");
		Calendar endDate = dateMap.get("endDate");
		List<DividendEntity> list = dividendRepository.findByAnnouncementDateBetweenAndYear(startDate.getTime(),
				endDate.getTime(), year);
		List<String> stockIds = list.stream().map(e -> e.getStockId()).collect(Collectors.toList());
		List<DividendDto> dividendDtoList = stockService.dividend(stockIds, year);
		log.info(dividendDtoList.size() + "筆資料");
		if (dividendDtoList.size() > 0) {
			Mail mail = mailService.genMail("現金股利 " + year + "年");
			Context context = new Context();
			context.setVariable("templateName", "dividend");
			context.setVariable("year", year);
			context.setVariable("dividendList", dividendDtoList);
			mailService.sendMail(mail, context);
		}
		log.info("現金股利寄信結束");
		return new CommonResp();
	}

	// 寄損益表email
	@GetMapping(value = "/sendIncomeStatement")
	private CommonResp<Map<String, Object>> sendMailIncomeStatement(@RequestParam(value = "year") String year,
			@RequestParam(value = "season") Short season) throws Exception {
		log.info("損益表寄信開始");
		Map<String, Calendar> dateMap = genStatrDateAndEndDate();
		Calendar startDate = dateMap.get("startDate");
		Calendar endDate = dateMap.get("endDate");
		List<IncomeStatementEntity> list = incomeStatementRepository.findByYearAndSeasonAndAnnouncementDateBetween(year,
				season, new Timestamp(startDate.getTimeInMillis()), new Timestamp(endDate.getTimeInMillis()));
		List<String> stockIds = list.stream().map(e -> e.getStockId()).collect(Collectors.toList());
		Map<String, List<IncomeStatementDto>> incomeStatementMap = stockService.incomeStatement(stockIds, year, season);
		log.info(incomeStatementMap.size() + "筆資料");
		if (incomeStatementMap.size() > 0) {
			String preYear = String.valueOf(Integer.valueOf(year) - 1);
			Mail mail = mailService.genMail("綜合損益表" + year + "年 第" + season + "季");
			Context context = new Context();
			context.setVariable("templateName", "incomeStatement");
			context.setVariable("year", year);
			context.setVariable("season", season);
			context.setVariable("preYear", preYear);
			context.setVariable("incomeStatementMap", incomeStatementMap);
			mailService.sendMail(mail, context);
		}
		log.info("損益表寄信結束");
		return new CommonResp();
	}

	private Map<String, Calendar> genStatrDateAndEndDate() {
		Map<String, Calendar> dateMap = new HashMap<String, Calendar>();
		Calendar nowCalendar = Calendar.getInstance();
		Date now = nowCalendar.getTime();

		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		int nowYear = now.getYear() + 1900;
		int nowMonth = now.getMonth();
		int nowDate = now.getDate();
		int nowHour = now.getHours();
		// 晚上8點 (早上8點~晚上8點)
		if (nowHour >= 20) {
			startDate.set(nowYear, nowMonth, nowDate, 8, 0, 0);
			endDate.set(nowYear, nowMonth, nowDate, 20, 0, 0);

		}
		// 早上8點 (昨天晚上8點到隔天早上8點)
		else {
			startDate.set(nowYear, nowMonth, nowDate, 20, 0, 0);
			startDate.add(Calendar.DATE, -1);
			endDate.set(nowYear, nowMonth, nowDate, 8, 0, 0);
		}
		dateMap.put("startDate", startDate);
		dateMap.put("endDate", endDate);
		return dateMap;
	}
}
