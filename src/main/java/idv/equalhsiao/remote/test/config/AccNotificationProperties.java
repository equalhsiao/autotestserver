package idv.equalhsiao.remote.test.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "spring.acc.notification")
public class AccNotificationProperties {
    private Integer checkMillisecond;
    private Integer checkCount;
    private Integer checkRetryCount;
}
