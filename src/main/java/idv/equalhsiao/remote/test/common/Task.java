package idv.equalhsiao.remote.test.common;

import idv.equalhsiao.remote.test.config.CommonResp;
import java.util.Map;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.TimeoutException;

/**
 * 任務執行
 */
@Slf4j
@Data
public abstract class Task {

  private String taskName;

  public Task() {
  }

  public Task(String taskName) {
    super();
    this.taskName = taskName;
  }

  /**
   * 需要override的方法
   */
  public abstract void task(CommonResp<Map<String, Object>> response) throws Exception;

  void run(CommonResp<Map<String, Object>> response) throws Exception {
    try {
      Map<String, Object> responseData = response.getData();
      responseData.put("task", taskName);
      Thread.sleep(1000);
      log.info("task:" + taskName);
      task(response);
    }
    //重新嘗試任務
    catch (TimeoutException timeoutException) {
      log.info("timeoutException retrying...");
      log.info("task retry:" + taskName);
      //超過時間retry
      task(response);
    } catch (ElementNotInteractableException elementNotInteractableException) {
      log.info("elementNotInteractableException retrying...");
      log.info("task retry:" + taskName);
      //不可點擊retry
      task(response);
    }
  }
}
