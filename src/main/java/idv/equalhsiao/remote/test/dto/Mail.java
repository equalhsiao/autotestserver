package idv.equalhsiao.remote.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Mail {
	private static final String EMAIL_VALID = "Email should be valid";
	@NotBlank
	@JsonProperty(required = true)
	private String subject;

	@Email(message = Mail.EMAIL_VALID)
	@NotBlank
	@JsonProperty(required = true)
	private String[] to;

	@Email(message = Mail.EMAIL_VALID)
	@JsonProperty(required = false)
	private String from;

	@JsonProperty(required = false)
	private Set<@Email(message = Mail.EMAIL_VALID) String> carbonCopy;

	@NotBlank
	@JsonProperty(required = true)
	private String templateName;

	@JsonProperty(required = false)
	private String content;

//	@Valid
//	@JsonProperty(required = false)
//	private Set<AttachmentFile> attachmentFile;
//
//	@Valid
//	@JsonProperty(required = false)
//	private Set<AttachmentUrl> attachmentUrl;

//	@JsonProperty(required = false)
//	private Map<String, Object> variables;
	
	
}
