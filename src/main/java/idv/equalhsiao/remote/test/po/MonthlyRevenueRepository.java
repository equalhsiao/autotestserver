package idv.equalhsiao.remote.test.po;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MonthlyRevenueRepository extends JpaRepository<MonthlyRevenueEntity, Integer> {

	MonthlyRevenueEntity findByStockId(String stockId);

	List<MonthlyRevenueEntity> findByStockIdInAndYearAndMonth(List<String> stockId, String year, Integer month);

//	List<MonthlyRevenueEntity> findByAnnouncementTimeBetweenAndYearAndMonth(Timestamp announcementTimeStart,
//			Timestamp announcementTimeEnd, String year, Integer month);

	List<MonthlyRevenueEntity> findByAnnouncementTimeBetweenAndYearAndMonth(Date announcementTimeStart,
			Date announcementTimeEnd, String year, Integer month);

	List<MonthlyRevenueEntity> findByYearAndMonth(String year, Integer month);

	List<MonthlyRevenueEntity> findByIndustryAndYearAndMonth(String industry, String year, Integer month);

	List<MonthlyRevenueEntity> findByAnnouncementTimeBetweenAndIndustryAndYearAndMonth(Date announcementTimeStart,
			Date announcementTimeEnd, String industry, String year, Integer month);

}
