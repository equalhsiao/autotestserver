package idv.equalhsiao.remote.test.config;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class SeleniumConfiguration {

  @Autowired
  private SeleniumProperties seleniumProperties;

  @Bean
  public WebDriver webDriver() {
    return new ChromeDriver(SeleniumConfiguration.setWebDriver(seleniumProperties));
  }

  public static ChromeOptions setWebDriver(SeleniumProperties seleniumProperties) {
    ChromeOptions chromeOptions = new ChromeOptions();
    String osName = System.getProperty("os.name");
    log.info("OS name:" + osName);
    boolean isWindows = System.getProperty("os.name")
        .toLowerCase().startsWith("windows");
    if (isWindows) {
      System.setProperty(seleniumProperties.getDriver(), seleniumProperties.getWindowsLocation());
      chromeOptions.setBinary(seleniumProperties.getWindowsBinary());
    } else {
      System.setProperty(seleniumProperties.getDriver(), seleniumProperties.getLinuxLocation());
      chromeOptions.setBinary(seleniumProperties.getLinuxBinary());
    }
    if (seleniumProperties.getHeadless()) {
      chromeOptions.setHeadless(true);
      chromeOptions.addArguments("--disable-dev-shm-usage");
      chromeOptions.addArguments("--headless");
      chromeOptions.addArguments("--no-sandbox");
    }
    return chromeOptions;
  }
}
