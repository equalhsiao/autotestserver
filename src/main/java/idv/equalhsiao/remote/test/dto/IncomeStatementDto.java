package idv.equalhsiao.remote.test.dto;

import lombok.Data;

@Data
public class IncomeStatementDto {

	private String stockId;
	private String stockName;
	private String industry;
	private Float closePrice;
	private Integer volume;
	private Float eps;
	private Float nonOperatingIncomePercentage;
	private Float grossMargin;
	private Float priceToDreamRatio;
	//去年同季
	private Float preYearEPS;
	private Float preYearGrossMargin;
	private Float preYearContiueNetIncomePercentage;
	private Float preYearParentNetIncomePercentage;
	private Float preYearComprehensiveNetIncomePercentage;
	//前季
	private Float preSeasonEPS;
	private Float preSeasonGrossMargin;
	private Float preSeasonContiueNetIncomePercentage;
	private Float preSeasonParentNetIncomePercentage;
	private Float preSeasonComprehensiveNetIncomePercentage;
	
}
