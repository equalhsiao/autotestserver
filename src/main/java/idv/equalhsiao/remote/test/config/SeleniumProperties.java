package idv.equalhsiao.remote.test.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "spring.selenium")
public class SeleniumProperties {
    private String driver;
    private String windowsLocation;
    private String linuxLocation;
    private String windowsBinary;
    private String linuxBinary;
    private Boolean headless;
}
