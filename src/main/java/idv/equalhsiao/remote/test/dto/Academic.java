package idv.equalhsiao.remote.test.dto;

import lombok.Data;

@Data
public class Academic {
	//學群
	private String academicGroup;
	//學類
	private String science;
	//學校
	private String school;
	//學系
	private String department;
}
