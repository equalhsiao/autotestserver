package idv.equalhsiao.remote.test.dto;

import lombok.Data;

@Data
public class BalanceSheetDto {
	private String stockId;
	private String stockName;
	private Float closePrice;
	private Integer volume;
	private Long totalAssets; // 總資產
	private Long totalLiabilities;// 總負債
	private Long totalCapitalStock; // 總股本=股本-註銷股本
	private Long capitalReserve; // 資本公積
	private Long retainedSurplus;// 保留盈餘
	private Long treasuryStock;// 庫藏股票
	private Long totalEquity; // 權益總計
	private Long preRecepitShares; // 預收股款
	private Float referenceShareValue; // 每股參考淨值
	
	private Float preYearTotalAssets;//去年同季總資產增減(%)
	private Float preYearTotalLiabilities;//去年同季總負債增減(%)
	private Float preYearTotalCapitalStock; //去年同季總股本增減(%)
	private Float preYearCapitalReserve;//去年同季資本公積增減(%)
	private Float preYearRetainedSurplus; //去年同季保留盈餘增減(%)
	private Float preYearTotalEquity; //去年同季權益總計增減(%)
	private Long preYearTreasuryStock; //去年同季庫藏股票
	private Long preYearPreRecepitShares; //去年同季預收股款
	private Float preYearReferenceShareValue; //去年同季每股參考淨值
	
	private Float preSeasonTotalAssets;//前季單季總資產增減(%)
	private Float preSeasonTotalLiabilities;//前季單季總負債增減(%)
	private Float preSeasonTotalCapitalStock; //前季單季總股本增減(%)
	private Float preSeasonCapitalReserve;//前季單季資本公積增減(%)
	private Float preSeasonRetainedSurplus; //前季單季保留盈餘增減(%)
	private Float preSeasonTotalEquity; //前季單季權益總計增減(%)
	private Long preSeasonTreasuryStock; //前季單季庫藏股票
	private Long preSeasonPreRecepitShares; //前季單季預收股款
	private Float preSeasonReferenceShareValue; //前季單季每股參考淨值
}
