package idv.equalhsiao.remote.test.po;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImportantMessageRepository extends JpaRepository<ImportantMessageEntity, Integer> {

	List<ImportantMessageEntity> findByYearAndMonthAndAnnouncementDateBetween(String year, Integer month,
			Date startDate, Date endDate);

	List<ImportantMessageEntity> findByYearAndMonthAndDay(String year, Integer month, Integer day);

	ImportantMessageEntity findByStockIdAndSerialNumberAndYearAndMonthAndDay(String stockId, Integer serialNumber,
			String year, Integer month, Integer day);
}
