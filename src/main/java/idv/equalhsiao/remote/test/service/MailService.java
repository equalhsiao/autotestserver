package idv.equalhsiao.remote.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import idv.equalhsiao.remote.test.dto.Mail;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class MailService {

	private final String[] RECIPIENT = new String[] { "xyz90161@gmail.com", "zhizhis24@gmail.com" };
	private final String[] TEST_RECIPIENT = new String[] { "xyz90161@gmail.com" };

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private ITemplateEngine templateEngine;

	public void sendMail(Mail mail, Context context) throws Exception {
		String emailContent = templateEngine.process("main", context);
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			helper.setText(emailContent, true);
			helper.setTo(mail.getTo());
			helper.setSubject(mail.getSubject());
		};
		mailSender.send(messagePreparator);
	}

	public Mail genMail(String subject) {
		Mail mail = new Mail();
		mail.setFrom("xyz90161@gmail.com");
		mail.setTo(RECIPIENT);
		mail.setSubject(subject);
		return mail;
	}
}
