package idv.equalhsiao.remote.test.po;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "stockindustryprice")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class StockIndustryPriceEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String country;
	private String mainIndustry;
	private String subIndustry;
	private Float riseFallPercentage;
	private Float yesterdayRiseFallPercentage;
	private Float averagePeRatio;
	private Date useDate;
}
