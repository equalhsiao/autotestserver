package idv.equalhsiao.remote.test.controller;

import idv.equalhsiao.remote.test.config.SeleniumConfiguration;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import idv.equalhsiao.remote.test.config.CommonResp;
import idv.equalhsiao.remote.test.config.LearningUserProperties;
import idv.equalhsiao.remote.test.config.SeleniumProperties;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "JWLearning")
@Slf4j
public class JWLearningController {

	private LearningUserProperties learningUserProperties;
	private SeleniumProperties seleniumProperties;

	@Autowired
	public JWLearningController(LearningUserProperties learningUserProperties, SeleniumProperties seleniumProperties) {
		this.learningUserProperties = learningUserProperties;
		this.seleniumProperties = seleniumProperties;
	}

	@GetMapping("/login")
	private CommonResp<Map<String, Object>> login() throws IOException, Exception {
		CommonResp<Map<String, Object>> response = new CommonResp<Map<String, Object>>();
		WebDriver webDriver = new ChromeDriver(SeleniumConfiguration.setWebDriver(seleniumProperties));
		Map<String, Object> map = new HashMap<String, Object>();
		String URL = "https://jwcpas.learningvilla.com/RWD/LoginPage.aspx";
		WebDriverWait wait = new WebDriverWait(webDriver, 10);
		webDriver.get(URL);
		WebElement userID = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("UserID")));
		userID.sendKeys(learningUserProperties.getUsername());
		WebElement password = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Password")));
		password.sendKeys(learningUserProperties.getPassword());
		WebElement signIn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("btnSignIn")));
		signIn.submit();
		WebElement tabContent = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tabContent")));
		Thread.sleep(10000);
		log.info("SUCCESS");
		webDriver.close();
		webDriver.quit();
		response.setData(map);
		return response;
	}
}
