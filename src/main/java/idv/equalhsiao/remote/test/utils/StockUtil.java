package idv.equalhsiao.remote.test.utils;

public class StockUtil {

	public final static String[] industryArray = new String[] { "水泥工業", "食品工業", "塑膠工業", "紡織纖維", "電機機械", "電器電纜", "化學工業",
			"生技醫療業", "玻璃陶瓷", "造紙工業", "鋼鐵工業", "橡膠工業", "汽車工業", "半導體業", "電腦及週邊設備業", "光電業", "通信網路業", "電子零組件業", "電子通路業",
			"資訊服務業", "其他電子業", "油電燃氣業", "建材營造", "航運業", "觀光事業", "金融保險業", "貿易百貨", "其他", "存託憑證" };

	public static Float calPercentage(Long currentNum, Long preNum) {
		if (currentNum == null || preNum == null) {
			return null;
		}
		return (Float.valueOf(currentNum - preNum) / Math.abs(preNum)) * 100;
	}

	public static Float decimal(Float f) {
		if (f == null) {
			return null;
		}
		return Math.round((f * 1000f)) / 1000f;
	}

	public static Float getPredictionEPS(Float eps, Short season) {
		if (season == 1) {
			return eps * 4;
		} else if (season == 2 || season == 3) {
			return (eps / season) * 4;
		}
		return eps;
	}
	
	// 計算前一季
	public static Short preSeasonCal(Short seasonShort) {
		return (short) (((seasonShort - 1) % 4) == 0 ? 4 : seasonShort - 1);
	}
}
