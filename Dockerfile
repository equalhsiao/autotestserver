FROM maven:3.6.1-jdk-8
WORKDIR /app
# download webdriver
RUN apt-get update && curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -y zip unzip nodejs npm
RUN npm install -g newman newman-reporter-html newman-reporter-htmlfull2 newman-reporter-htmlextra
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install
ADD https://chromedriver.storage.googleapis.com/86.0.4240.22/chromedriver_linux64.zip /app
RUN unzip /app/chromedriver_linux64.zip
ADD https://repo1.maven.org/maven2/co/elastic/apm/elastic-apm-agent/1.18.1/elastic-apm-agent-1.18.1.jar /app
COPY ./elasticapm/elasticapm.properties /app
COPY ./target/AutoTestServer-0.0.1-SNAPSHOT.jar /app/
#ENTRYPOINT ["java","-jar","/app/AutoTestServer-0.0.1-SNAPSHOT.jar"]
#監測效能
ENTRYPOINT ["java","-javaagent:/app/elastic-apm-agent-1.18.1.jar","-jar","/app/AutoTestServer-0.0.1-SNAPSHOT.jar"]
EXPOSE 8033