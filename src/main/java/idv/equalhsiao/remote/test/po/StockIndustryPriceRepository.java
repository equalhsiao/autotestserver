package idv.equalhsiao.remote.test.po;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StockIndustryPriceRepository extends JpaRepository<StockIndustryPriceEntity, Integer> {

	List<StockIndustryPriceEntity> findByUseDate(Date useDate);

	List<StockIndustryPriceEntity> findByCountryAndUseDate(String country, Date useDate);

	List<StockIndustryPriceEntity> findBySubIndustryContainingAndUseDateBetween(String subIndustry, Date startDate, Date endDate);
}
