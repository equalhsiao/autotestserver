package idv.equalhsiao.remote.test.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import idv.equalhsiao.remote.test.config.CommonResp;
import idv.equalhsiao.remote.test.config.SeleniumProperties;
import idv.equalhsiao.remote.test.config.UserProperties;
import idv.equalhsiao.remote.test.dto.Academic;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "ClassInfo")
@Slf4j
public class ClassInfoController {

  private UserProperties userProperties;
  private SeleniumProperties seleniumProperties;

  @Autowired
  public ClassInfoController(UserProperties userProperties, SeleniumProperties seleniumProperties) {
    this.userProperties = userProperties;
    this.seleniumProperties = seleniumProperties;
  }

  @GetMapping("/getClassInfo")
  private CommonResp<Map<String, Object>> restart() throws IOException {
//		System.setProperty(seleniumProperties.getDriver(), seleniumProperties.getLocation());
    ChromeOptions chromeOptions = new ChromeOptions();
    if (System.getProperty("os.name").indexOf("Windows") >= 0) {
      System.setProperty(seleniumProperties.getDriver(), seleniumProperties.getWindowsLocation());
      chromeOptions.setBinary(seleniumProperties.getWindowsBinary());
    } else {
      System.setProperty(seleniumProperties.getDriver(), seleniumProperties.getLinuxLocation());
      chromeOptions.setBinary(seleniumProperties.getLinuxBinary());
    }
    CommonResp<Map<String, Object>> response = new CommonResp<Map<String, Object>>();
    chromeOptions.setHeadless(true);
    chromeOptions.addArguments("--disable-dev-shm-usage");
    chromeOptions.addArguments("--headless");
    chromeOptions.addArguments("--no-sandbox");
    WebDriver webDriver = new ChromeDriver(chromeOptions);
    Map<String, Object> map = new HashMap<String, Object>();
    String contextUrl = "http://campus4.ncku.edu.tw/uac/cross_search/class_info/";
    WebDriverWait wait = new WebDriverWait(webDriver, 10);
    List<Academic> result = new LinkedList<Academic>();
    FileWriter fileWriter = new FileWriter("temp.csv");
    PrintWriter printWriter = new PrintWriter(fileWriter);
    for (int i = 1; i <= 19; i++) {
      log.info("process:" + i + "/19");
      String fullUrl = contextUrl + i + ".html";
      webDriver.get(fullUrl);
      WebElement academicGroupElement = wait
          .until(ExpectedConditions.visibilityOfElementLocated(By.tagName("h1")));
      String academicGroup = academicGroupElement.getText();
      List<WebElement> subAcademicGroup = wait
          .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("mm")));
      List<WebElement> tableList = wait
          .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.tagName("table")));
      for (int subAcademicIndex = 0; subAcademicIndex <= subAcademicGroup.size() - 1;
          subAcademicIndex++) {
        WebElement subAcademic = subAcademicGroup.get(subAcademicIndex);
        WebElement table = tableList.get(subAcademicIndex);
        List<WebElement> trList = table.findElements(By.tagName("tr"));
        for (int trIndex = 0; trIndex <= trList.size() - 1; trIndex++) {
          WebElement tr = trList.get(trIndex);
          List<WebElement> tdList = tr.findElements(By.tagName("td"));
          List<WebElement> aList = tdList.get(1).findElements(By.tagName("a"));
          for (int aIndex = 0; aIndex <= aList.size() - 1; aIndex++) {
            WebElement a = aList.get(aIndex);
            printWriter.printf("%s,%s,%s,%s\r\n", tdList.get(0).getText(), a.getText(),
                subAcademic.getText(), academicGroup);
          }
        }
      }
    }
    printWriter.close();
    webDriver.close();
    webDriver.quit();
    map.put("data", result);
    response.setData(map);
    return response;
  }
}
