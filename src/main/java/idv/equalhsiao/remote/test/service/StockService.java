package idv.equalhsiao.remote.test.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import idv.equalhsiao.remote.test.dto.BalanceSheetDto;
import idv.equalhsiao.remote.test.dto.DividendDto;
import idv.equalhsiao.remote.test.dto.IncomeStatementDto;
import idv.equalhsiao.remote.test.dto.MonthlyRevenueDto;
import idv.equalhsiao.remote.test.po.BalanceSheetEntity;
import idv.equalhsiao.remote.test.po.BalanceSheetRepository;
import idv.equalhsiao.remote.test.po.DividendEntity;
import idv.equalhsiao.remote.test.po.DividendRepository;
import idv.equalhsiao.remote.test.po.EPSRepository;
import idv.equalhsiao.remote.test.po.IncomeStatementEntity;
import idv.equalhsiao.remote.test.po.IncomeStatementRepository;
import idv.equalhsiao.remote.test.po.MonthlyRevenueEntity;
import idv.equalhsiao.remote.test.po.MonthlyRevenueRepository;
import idv.equalhsiao.remote.test.utils.StockUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StockService {

	private final String OPEN_DATA_URL = "http://www.twse.com.tw/exchangeReport/STOCK_DAY_ALL?response=open_data";

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		// Do any additional configuration here
		return builder.build();
	}

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private IncomeStatementRepository incomeStatementRepository;
	@Autowired
	private DividendRepository dividendRepository;
	@Autowired
	private MonthlyRevenueRepository monthlyRevenueRepository;
	@Autowired
	private BalanceSheetRepository balanceSheetRepository;
	@Autowired
	private EPSRepository epsRepository;

	// 取得資產損益表計算結果
	public List<BalanceSheetDto> balanceSheet(List<String> stockIds, String year, Short season) throws Exception {
		List<BalanceSheetEntity> list = balanceSheetRepository.findByStockIdInAndYearAndSeason(stockIds, year, season);
		// 查詢股價
		Map<String, Map<String, String>> openDataPriceMap = getPriceFromOpenData();
		List<BalanceSheetDto> dtoList = new ArrayList<BalanceSheetDto>();
		// 去年
		String preYear = String.valueOf(Integer.valueOf(year) - 1);
		List<BalanceSheetEntity> perYearList = balanceSheetRepository.findByStockIdInAndYearAndSeason(stockIds, preYear,
				season);
		// 前季
		String preSeasonYear = season == 1 ? preYear : year;
		Short preSeasonSeason = (short) (season == 1 ? 4 : season - 1);
		List<BalanceSheetEntity> preSeasonList = balanceSheetRepository.findByStockIdInAndYearAndSeason(stockIds,
				preSeasonYear, preSeasonSeason);

		for (BalanceSheetEntity e : list) {
			String stockId = e.getStockId();
			try {
				BalanceSheetDto dto = new BalanceSheetDto();
				dto.setStockId(stockId);
				dto.setStockName(e.getStockName());
				dto.setCapitalReserve(e.getCapitalReserve());
				dto.setPreRecepitShares(e.getPreRecepitShares());
				dto.setReferenceShareValue(e.getReferenceShareValue());
				dto.setRetainedSurplus(e.getRetainedSurplus());
				dto.setTotalAssets(e.getTotalAssets());
				dto.setTotalEquity(e.getTotalEquity());
				dto.setTotalLiabilities(e.getTotalLiabilities());
				dto.setTreasuryStock(e.getTreasuryStock());
				if (e.getCancelCapital() == null) {
					dto.setTotalCapitalStock(e.getCapitalStock());
				} else {
					dto.setTotalCapitalStock(e.getCapitalStock() - e.getCancelCapital());
				}
				// 去年
				Optional<BalanceSheetEntity> preYearBalanceSheetOptional = perYearList.stream()
						.filter(balanceSheet -> stockId.equals(balanceSheet.getStockId())).findFirst();
				if (preYearBalanceSheetOptional.isPresent()) {
					BalanceSheetEntity preYearEntity = preYearBalanceSheetOptional.get();
					Long preYearCaptialStock = preYearEntity.getCancelCapital() == null
							? preYearEntity.getCapitalStock()
							: preYearEntity.getCapitalStock() - preYearEntity.getCancelCapital();
					Float preYearTotalCapitalStock = StockUtil
							.decimal(StockUtil.calPercentage(dto.getTotalCapitalStock(), preYearCaptialStock));
					dto.setPreYearTotalCapitalStock(preYearTotalCapitalStock);
					Float preYearCapitalReserve = StockUtil.decimal(
							StockUtil.calPercentage(dto.getCapitalReserve(), preYearEntity.getCapitalReserve()));
					dto.setPreYearCapitalReserve(preYearCapitalReserve);
					Float preYearTotalAssets = StockUtil
							.decimal(StockUtil.calPercentage(dto.getTotalAssets(), preYearEntity.getTotalAssets()));
					dto.setPreYearTotalAssets(preYearTotalAssets);
					Float preYearTotalLiabilities = StockUtil.decimal(
							StockUtil.calPercentage(dto.getTotalLiabilities(), preYearEntity.getTotalLiabilities()));
					dto.setPreYearTotalLiabilities(preYearTotalLiabilities);
					Float preYearRetainedSurplus = StockUtil.decimal(
							StockUtil.calPercentage(dto.getRetainedSurplus(), preYearEntity.getRetainedSurplus()));
					dto.setPreYearRetainedSurplus(preYearRetainedSurplus);
					Float preYearTotalEquity = StockUtil
							.decimal(StockUtil.calPercentage(dto.getTotalEquity(), preYearEntity.getTotalEquity()));
					dto.setPreYearTotalEquity(preYearTotalEquity);
//					Float preYearTreasuryStock = StockUtil
//							.decimal(StockUtil.calPercentage(dto.getTreasuryStock(), preYearEntity.getTreasuryStock()));
//					Float preYearRecepitShares = StockUtil.decimal(
//							StockUtil.calPercentage(dto.getPreRecepitShares(), preYearEntity.getPreRecepitShares()));
//					Float preYearReferenceShareValue = StockUtil.decimal(
//							(Float.valueOf(dto.getReferenceShareValue() - preYearEntity.getReferenceShareValue())
//									/ Math.abs(preYearEntity.getReferenceShareValue())) * 100);
					dto.setPreYearTreasuryStock(preYearEntity.getTreasuryStock());
					dto.setPreYearPreRecepitShares(preYearEntity.getPreRecepitShares());
					dto.setPreYearReferenceShareValue(preYearEntity.getReferenceShareValue());
				}
				// 股價
				Map<String, String> openDataMap = openDataPriceMap.get(stockId);
				if (openDataMap != null) {
					if (openDataMap.containsKey("volume") && !StringUtils.isEmpty(openDataMap.get("closePrice"))) {
						Integer volume = Integer.valueOf(openDataMap.get("volume")) / 1000;
						dto.setVolume(volume);
					}
					if (openDataMap.containsKey("closePrice") && !StringUtils.isEmpty(openDataMap.get("closePrice"))) {
						dto.setClosePrice(Float.valueOf(openDataMap.get("closePrice")));
					}
				}

				// 前季
				Optional<BalanceSheetEntity> preSeasonBalanceSheetOptional = preSeasonList.stream()
						.filter(balanceSheet -> stockId.equals(balanceSheet.getStockId())).findFirst();
				if (preSeasonBalanceSheetOptional.isPresent()) {
					BalanceSheetEntity preSeasonEntity = preSeasonBalanceSheetOptional.get();
					Float preSeasonTotalAssets = StockUtil
							.decimal(StockUtil.calPercentage(e.getTotalAssets(), preSeasonEntity.getTotalAssets()));// 總資產
					Float preSeasonTotalLiabilities = StockUtil.decimal(
							StockUtil.calPercentage(e.getTotalLiabilities(), preSeasonEntity.getTotalLiabilities()));// 總負債
					Float preSeasonTotalCapitalStock = StockUtil
							.decimal(StockUtil.calPercentage(e.getCapitalStock(), preSeasonEntity.getCapitalStock()));// 總股本
					Float preSeasonCapitalReserve = StockUtil.decimal(
							StockUtil.calPercentage(e.getCapitalReserve(), preSeasonEntity.getCapitalReserve()));// 資本公積
					Float preSeasonRetainedSurplus = StockUtil.decimal(
							StockUtil.calPercentage(e.getRetainedSurplus(), preSeasonEntity.getRetainedSurplus()));// 保留盈餘
					Float preSeasonTotalEquity = StockUtil
							.decimal(StockUtil.calPercentage(e.getTotalEquity(), preSeasonEntity.getTotalEquity()));// 權益總計
//					Float preSeasonPreRecepitShares = StockUtil.decimal(
//							StockUtil.calPercentage(e.getPreRecepitShares(), preSeasonEntity.getPreRecepitShares()));// 預收股款
//					Float preSeasonTreasuryStock = StockUtil
//							.decimal(StockUtil.calPercentage(e.getTreasuryStock(), preSeasonEntity.getTreasuryStock()));// 權益總計
//					Float preSeasonReferenceShareValue = StockUtil.decimal(
//							(Float.valueOf(dto.getReferenceShareValue() - preSeasonEntity.getReferenceShareValue())
//									/ Math.abs(preSeasonEntity.getReferenceShareValue())) * 100);// 每股參考淨值
					dto.setPreSeasonTotalAssets(preSeasonTotalAssets);// 總資產
					dto.setPreSeasonTotalLiabilities(preSeasonTotalLiabilities);// 總負債
					dto.setPreSeasonTotalCapitalStock(preSeasonTotalCapitalStock);// 總股本
					dto.setPreSeasonCapitalReserve(preSeasonCapitalReserve);// 資本公積
					dto.setPreSeasonRetainedSurplus(preSeasonRetainedSurplus);// 保留盈餘
					dto.setPreSeasonTotalEquity(preSeasonTotalEquity);// 權益總計
					dto.setPreSeasonPreRecepitShares(preSeasonEntity.getPreRecepitShares());// 預收股款
					dto.setPreSeasonTreasuryStock(preSeasonEntity.getTreasuryStock());// 庫藏股票
					dto.setPreSeasonReferenceShareValue(preSeasonEntity.getReferenceShareValue());// 每股參考淨值
				}

				dtoList.add(dto);
			} catch (Exception ex) {
				log.info("stockId:" + stockId);
				throw ex;
			}
		}
		return dtoList;

	}

	// 取得月營收計算結果
	public List<MonthlyRevenueDto> monthlyRevenue(List<String> stockIds, String year, Integer month) throws Exception {
		List<MonthlyRevenueEntity> list = monthlyRevenueRepository.findByStockIdInAndYearAndMonth(stockIds, year,
				month);
		// 查詢股價
		Map<String, Map<String, String>> openDataPriceMap = getPriceFromOpenData();
		List<MonthlyRevenueDto> monthlyRevenueDtoList = new ArrayList<MonthlyRevenueDto>();
		for (MonthlyRevenueEntity e : list) {
			String stockId = e.getStockId();
			try {
				MonthlyRevenueDto dto = new MonthlyRevenueDto();
				dto.setStockId(stockId);
				dto.setStockName(e.getStockName());
				dto.setIndustry(e.getIndustry());
				Map<String, String> openDataMap = openDataPriceMap.get(stockId);
				if (openDataMap != null && openDataMap.containsKey("closePrice")) {
					Integer volume = Integer.valueOf(openDataMap.get("volume")) / 1000;
					dto.setClosePrice(Float.valueOf(openDataMap.get("closePrice")));
					dto.setVolume(volume);
				}
				dto.setPreMonthlyCompareGrandTotalPrecentage(e.getPreMonthlyCompareGrandTotalPrecentage());
				dto.setPreMonthlyComparePrecentage(e.getPreMonthlyComparePrecentage());
				dto.setPreYearComparePrecentage(e.getPreYearComparePrecentage());
				monthlyRevenueDtoList.add(dto);
			} catch (Exception ex) {
				log.info("stockId:" + stockId);
				throw ex;
			}
		}
		return monthlyRevenueDtoList;
	}

	// 取得現金股利計算結果
	public List<DividendDto> dividend(List<String> stockIds, String year) throws Exception {
		List<DividendEntity> list = dividendRepository.findByStockIdInAndYear(stockIds, year);
		List<DividendDto> dividendDtoList = new ArrayList<DividendDto>();
		Map<String, Map<String, String>> openDatePriceMap = getPriceFromOpenData();
		Short season = 4;
		Integer yearInt = Integer.parseInt(year);
		for (DividendEntity e : list) {
			DividendDto dto = new DividendDto();
			String stockId = e.getStockId();
			dto.setStockId(stockId);
			dto.setStockName(e.getStockName());
			// opendata csv沒資料或是收盤價是空值
			Map<String, String> openDataStockPriceMap = openDatePriceMap.get(stockId);
			// 透過csv取得收盤價
			if (openDataStockPriceMap != null && !"".equals(openDataStockPriceMap.get("closePrice"))) {
				Float price = Float.valueOf(openDataStockPriceMap.get("closePrice"));
				Integer volume = Integer.valueOf(openDataStockPriceMap.get("volume")) / 1000;
				dto.setVolume(volume);
				dto.setClosePrice(price);
			}
//			EPSEntity epsEntity = epsRepository.findByStockIdAndYearAndSeason(stockId, String.valueOf(yearInt - 1),
//					season);
//			EPSEntity preYearEPSEntity = epsRepository.findByStockIdAndYearAndSeason(stockId,
//					String.valueOf(yearInt - 2), season);
			dto.setTotalCashDividend(e.getCashDividend() + e.getStockDividend() + e.getCapitalDividend());
			dto.setTotalStockDividend(
					e.getIncreaseCashDividend() + e.getIncreaseStockDividend() + e.getIncreaseCapitalDividend());
			dto.setTotalDividend(dto.getTotalCashDividend() + dto.getTotalStockDividend());
//			Float nonOperatingIncome = 0f;
			// 除於1000避免long太大超過float最大值
//			if (epsEntity != null && !StringUtils.isEmpty(epsEntity.getNonOperatingIncome())) {
//				nonOperatingIncome = (float) (epsEntity.getNonOperatingIncome() / 1000);
//			}
//			Float operatingIncome = 0f;
//			if (epsEntity != null) {
//				operatingIncome = (float) (epsEntity.getOperatingIncome() / 1000);
			// 今年EPS
//				dto.setEps(epsEntity.getEps());
//			}
			// 業外與業內比例
//			dto.setNonOperatingPercentage(StockUtil.decimal((nonOperatingIncome / operatingIncome) * 100));
			dto.setTotalYieldRate(e.getTotalYieldRate());
			// 股利配股率
//			dto.setPayoutRatio(StockUtil.decimal((dto.getTotalDividend() / epsEntity.getEps()) * 100));
			dto.setDividendYear(e.getDividendYear());
//			前年EPS
//			if (preYearEPSEntity != null) {
//				dto.setPreYearEPS(preYearEPSEntity.getEps());
//			}
			dividendDtoList.add(dto);
		}
		return dividendDtoList;
	}

	// 取得損益表計算結果
	public Map<String, List<IncomeStatementDto>> incomeStatement(List<String> stockIds, String year, Short season)
			throws Exception {
		// 去年的年與季
		String preYear = String.valueOf(Integer.valueOf(year) - 1);
		// 查詢股價
		Map<String, Map<String, String>> openDataPriceMap = stockIds.size() > 0 ? getPriceFromOpenData() : null;
		// 去年
		List<IncomeStatementEntity> preYearIncomeStatementList = incomeStatementRepository
				.findByStockIdInAndYearAndSeason(stockIds, preYear, season);
		// 今年
		List<IncomeStatementEntity> incomeStatementList = incomeStatementRepository
				.findByStockIdInAndYearAndSeason(stockIds, year, season);
		Map<String, List<IncomeStatementDto>> emailMap = new HashMap<String, List<IncomeStatementDto>>();
		for (String industry : StockUtil.industryArray) {
			List<IncomeStatementEntity> list = incomeStatementList.stream()
					.filter(e -> industry.equals(e.getIndustry())).collect(Collectors.toList());
			if (list.size() <= 0) {
				continue;
			}
			List<IncomeStatementDto> emailList = new ArrayList<IncomeStatementDto>();
			emailMap.put(industry, emailList);
			for (IncomeStatementEntity e : list) {
				IncomeStatementDto dto = new IncomeStatementDto();
				dto.setIndustry(industry);
				String stockId = e.getStockId();
				try {
					dto.setStockId(stockId);
					dto.setStockName(e.getStockName());
					Map<String, String> openDataMap = openDataPriceMap.get(stockId);
					if (openDataMap != null && openDataMap.containsKey("closePrice")) {
						dto.setClosePrice(Float.valueOf(openDataMap.get("closePrice")));
						dto.setVolume((Integer.valueOf(openDataMap.get("volume")) / 1000));
					}
					dto.setEps(e.getEps());
					if (e.getNonOperatingIncome() != null) {
						dto.setNonOperatingIncomePercentage(
								StockUtil.decimal(Float.valueOf(e.getNonOperatingIncome()) / e.getIncome() * 100f));
					}
					// 毛利率
					if (!"金融保險業".equals(e.getIndustry())) {
						dto.setGrossMargin(StockUtil
								.decimal((e.getNetOperatingProfit().floatValue() / e.getIncome().floatValue()) * 100));
					}
					// 本夢比
					if (openDataMap != null || openDataMap.containsKey("closePrice")) {
						dto.setPriceToDreamRatio(StockUtil.decimal(Float.valueOf(openDataMap.get("closePrice"))
								/ StockUtil.getPredictionEPS(e.getEps(), season)));
					}
					// 去年
					Optional<IncomeStatementEntity> preYearIncomeStatementOptional = preYearIncomeStatementList.stream()
							.filter(incomeStatement -> stockId.equals(incomeStatement.getStockId())).findFirst();
					if (preYearIncomeStatementOptional.isPresent()) {
						IncomeStatementEntity preYearIncomeStatement = preYearIncomeStatementOptional.get();
						dto.setPreYearEPS(preYearIncomeStatement.getEps());
						// 毛利率
						if (!"金融保險業".equals(preYearIncomeStatement.getIndustry())) {
							dto.setPreYearGrossMargin(
									StockUtil.decimal((preYearIncomeStatement.getNetOperatingProfit().floatValue()
											/ preYearIncomeStatement.getIncome().floatValue()) * 100));
						}
						dto.setPreYearContiueNetIncomePercentage(StockUtil.decimal(StockUtil
								.calPercentage(e.getPeriodIncome(), preYearIncomeStatement.getPeriodIncome())));
						dto.setPreYearParentNetIncomePercentage(
								StockUtil.decimal(StockUtil.calPercentage(e.getParentCompanyPeriodIncome(),
										preYearIncomeStatement.getParentCompanyPeriodIncome())));
						dto.setPreYearComprehensiveNetIncomePercentage(
								StockUtil.decimal(StockUtil.calPercentage(e.getComprehensiveParentCompanyPeriodIncome(),
										preYearIncomeStatement.getComprehensiveParentCompanyPeriodIncome())));
					}
					// 前季
					Optional<IncomeStatementEntity> preSeasonIncomeStatementOptional = getPreIncomeStatement(stockId,
							year, String.valueOf(season));
					// 第1季
					if (season == 1) {
						IncomeStatementEntity preSeasonIncomeStatement = preSeasonIncomeStatementOptional.get();
						dto.setPreSeasonEPS(preSeasonIncomeStatement.getEps());
						// 毛利率
						if (!"金融保險業".equals(preSeasonIncomeStatement.getIndustry())) {
							dto.setPreSeasonGrossMargin(
									StockUtil.decimal((preSeasonIncomeStatement.getNetOperatingProfit().floatValue()
											/ preSeasonIncomeStatement.getIncome().floatValue()) * 100));
						}
						dto.setPreSeasonContiueNetIncomePercentage(StockUtil.decimal(StockUtil
								.calPercentage(e.getPeriodIncome(), preSeasonIncomeStatement.getPeriodIncome())));
						dto.setPreSeasonParentNetIncomePercentage(
								StockUtil.decimal(StockUtil.calPercentage(e.getParentCompanyPeriodIncome(),
										preSeasonIncomeStatement.getParentCompanyPeriodIncome())));
						dto.setPreSeasonComprehensiveNetIncomePercentage(
								StockUtil.decimal(StockUtil.calPercentage(e.getComprehensiveParentCompanyPeriodIncome(),
										preSeasonIncomeStatement.getComprehensiveParentCompanyPeriodIncome())));
					}
					// 第234季
					else if (preSeasonIncomeStatementOptional.isPresent()) {
						Optional<IncomeStatementEntity> currentSeasonIncomeSatementOptional = getPreIncomeStatement(
								stockId, year, String.valueOf(season));
						if (currentSeasonIncomeSatementOptional.isPresent()
								&& preSeasonIncomeStatementOptional.isPresent()) {
							IncomeStatementEntity currentSeasonIncomeSatement = currentSeasonIncomeSatementOptional
									.get();
							IncomeStatementEntity preSeasonIncomeStatement = preSeasonIncomeStatementOptional.get();
							dto.setPreSeasonEPS(preSeasonIncomeStatement.getEps());
							// 毛利率
							if (!"金融保險業".equals(e.getIndustry())) {
								dto.setPreSeasonGrossMargin(
										StockUtil.decimal((preSeasonIncomeStatement.getNetOperatingProfit().floatValue()
												/ preSeasonIncomeStatement.getIncome().floatValue()) * 100));
							}
							dto.setPreSeasonContiueNetIncomePercentage(StockUtil
									.decimal(StockUtil.calPercentage(currentSeasonIncomeSatement.getPeriodIncome(),
											preSeasonIncomeStatement.getPeriodIncome())));
							dto.setPreSeasonParentNetIncomePercentage(StockUtil.decimal(
									StockUtil.calPercentage(currentSeasonIncomeSatement.getParentCompanyPeriodIncome(),
											preSeasonIncomeStatement.getParentCompanyPeriodIncome())));
							dto.setPreSeasonComprehensiveNetIncomePercentage(StockUtil.decimal(StockUtil.calPercentage(
									currentSeasonIncomeSatement.getComprehensiveParentCompanyPeriodIncome(),
									preSeasonIncomeStatement.getComprehensiveParentCompanyPeriodIncome())));
						}
					}
					emailList.add(dto);
				} catch (Exception ex) {
					log.info("stockId:" + stockId);
					throw ex;
				}
			}
//			log.info(industry + ":新增" + list.size() + "筆資料");
		}
		return emailMap;
	}

	// 取得單季資產損益表
//	private Optional<BalanceSheetEntity> getPreSeasonBalanceSheet(String stockId, String year, String season) {
//		// 第一季
//		List<Short> seasonList = new ArrayList<Short>();
//		if ("1".equals(season)) {
//			String preYear = String.valueOf(Integer.valueOf(year) - 1);
//			// 固定用第4季-第3季
//			seasonList.add((short) 3);
//			seasonList.add((short) 4);
//			return getSingleBalanceSheet(balanceSheetRepository
//					.findByStockIdAndYearAndSeasonInOrderBySeasonDesc(stockId, preYear, seasonList));
//		}
//		// 第234季
//		Short seasonShort = new Short(season);
//		seasonList.add((short) (seasonShort));
//		seasonList.add((short) (seasonShort - 1));
//		return getSingleBalanceSheet(
//				balanceSheetRepository.findByStockIdAndYearAndSeasonInOrderBySeasonDesc(stockId, year, seasonList));
//
//	}

	// 兩季相減 取得單季變化
//	private Optional<BalanceSheetEntity> getSingleBalanceSheet(List<BalanceSheetEntity> balanceSheetList) {
//		return balanceSheetList.stream().reduce((Q4, Q3) -> {
//			Long capitalReserve = null;
//			if (Q4.getCapitalReserve() != null && Q3.getCapitalReserve() != null) {
//				capitalReserve = Q4.getCapitalReserve() - Q3.getCapitalReserve();// 資本公積
//			}
//			Long otherRights = null;
//			if (Q4.getOtherRights() != null && Q3.getOtherRights() != null) {
//				otherRights = Q4.getOtherRights() - Q3.getOtherRights();// 其他權益
//			}
//			Long treasuryStock = null;
//			if (Q4.getTreasuryStock() != null && Q3.getTreasuryStock() != null) {
//				treasuryStock = Q4.getTreasuryStock() - Q3.getTreasuryStock();// 庫藏股票
//			}
//			Long attributableParentCompanyTotalEquity = null;
//			if (Q4.getAttributableParentCompanyTotalEquity() != null
//					&& Q3.getAttributableParentCompanyTotalEquity() != null) {
//				attributableParentCompanyTotalEquity = Q4.getAttributableParentCompanyTotalEquity()
//						- Q3.getAttributableParentCompanyTotalEquity();// 歸屬於母公司業主之權益合計
//			}
//			Long nonControllingInterests = null;
//			if (Q4.getNonControllingInterests() != null && Q3.getNonControllingInterests() != null) {
//				nonControllingInterests = Q4.getNonControllingInterests() - Q3.getNonControllingInterests();// 非控制權益
//			}
//			Long cancelCapital = null;
//			if (Q4.getCancelCapital() != null && Q3.getCancelCapital() != null) {
//				cancelCapital = Q4.getCancelCapital() - Q3.getCancelCapital();// 註銷股本
//			}
//			Long preRecepitShares = null;
//			if (Q4.getPreRecepitShares() != null && Q3.getPreRecepitShares() != null) {
//				preRecepitShares = Q4.getPreRecepitShares() - Q3.getPreRecepitShares();// 預收股款
//			}
//			Long subsidiaryTreasuryStock = null;
//			if (Q4.getSubsidiaryTreasuryStock() != null && Q3.getSubsidiaryTreasuryStock() != null) {
//				subsidiaryTreasuryStock = Q4.getSubsidiaryTreasuryStock() - Q3.getSubsidiaryTreasuryStock(); // 母公司暨子公司所持有之母公司庫藏股股數（單位：股）
//			}
//			return new BalanceSheetEntity(null, Q4.getStockId(), Q4.getStockName(),
//					Q4.getTotalAssets() - Q3.getTotalAssets(), // 流動資產
//					Q4.getNonCurrentAssets() - Q3.getNonCurrentAssets(), // 非流動資產
//					Q4.getTotalAssets() - Q3.getTotalAssets(), // 總資產
//					Q4.getCurrentLiabilities() - Q3.getCurrentLiabilities(), // 流動負債
//					Q4.getNonCurrentLiabilities() - Q3.getNonCurrentLiabilities(), // 非流動負債
//					Q4.getTotalLiabilities() - Q3.getTotalLiabilities(), // 總負債
//					Q4.getCapitalStock() - Q3.getCapitalStock(), // 股本
//					capitalReserve, // 資本公積
//					Q4.getPreRecepitShares() - Q3.getPreRecepitShares(), // 保留盈餘
//					otherRights, // 其他權益
//					treasuryStock, // 庫藏股票
//					attributableParentCompanyTotalEquity, // 歸屬於母公司業主之權益合計
//					nonControllingInterests, // 非控制權益
//					Q4.getTotalEquity() - Q3.getTotalEquity(), // 權益總計
//					cancelCapital, // 註銷股本
//					preRecepitShares, // 預收股款
//					subsidiaryTreasuryStock, // 母公司暨子公司所持有之母公司庫藏股股數（單位：股）
//					Q4.getReferenceShareValue(), // 每股參考淨值
//					Q4.getYear(), Q4.getSeason(), Q4.getAnnouncementDate());
//		});
//	}

	// 取得單季損益表
	private Optional<IncomeStatementEntity> getPreIncomeStatement(String stockId, String year, String season) {
		// 第一季
		List<Short> seasonList = new ArrayList<Short>();
		if ("1".equals(season)) {
			String preYear = String.valueOf(Integer.valueOf(year) - 1);
			// 固定用第4季-第3季
			seasonList.add((short) 3);
			seasonList.add((short) 4);
			return getSingleIncomeStatement(incomeStatementRepository
					.findByStockIdAndYearAndSeasonInOrderBySeasonDesc(stockId, preYear, seasonList));
		}
		// 第234季
		Short seasonShort = new Short(season);
		seasonList.add((short) (seasonShort));
		seasonList.add((short) (seasonShort - 1));
		return getSingleIncomeStatement(
				incomeStatementRepository.findByStockIdAndYearAndSeasonInOrderBySeasonDesc(stockId, year, seasonList));
	}

	// 計算單季損益表
	private Optional<IncomeStatementEntity> getSingleIncomeStatement(List<IncomeStatementEntity> incomeStatementList) {
		return incomeStatementList.stream().reduce((Q4, Q3) -> {
			Long nonOperatingIncome = null;
			if (Q4.getNonOperatingIncome() != null && Q3.getNonOperatingIncome() != null) {
				nonOperatingIncome = Q4.getNonOperatingIncome() - Q3.getNonOperatingIncome();
			}
			Long parentCompanyPeriodIncome = null;
			if (Q4.getParentCompanyPeriodIncome() != null && Q3.getParentCompanyPeriodIncome() != null) {
				parentCompanyPeriodIncome = Q4.getParentCompanyPeriodIncome() - Q3.getParentCompanyPeriodIncome();
			}
			Long comprehensiveParentCompanyPeriodIncome = null;
			if (Q4.getComprehensiveParentCompanyPeriodIncome() != null
					&& Q3.getComprehensiveParentCompanyPeriodIncome() != null) {
				comprehensiveParentCompanyPeriodIncome = Q4.getComprehensiveParentCompanyPeriodIncome()
						- Q3.getComprehensiveParentCompanyPeriodIncome();
			}
			Long netOperatingProfit = null;
			if (Q4.getNetOperatingProfit() != null && Q3.getNetOperatingProfit() != null) {
				netOperatingProfit = Q4.getNetOperatingProfit() - Q3.getNetOperatingProfit();
			}
			Float eps = (float) Math.round((Q4.getEps() - Q3.getEps()) * 1000) / 1000;
			return new IncomeStatementEntity(null, Q4.getStockId(), Q4.getStockName(), Q4.getIndustry(),
					Q4.getIncome() - Q3.getIncome(), // 營業收入
					null, // 營業成本
					null, // 未實現銷貨（損）益
					null, // 已實現銷貨（損）益
					netOperatingProfit, // 營業毛利淨額
					null, // 營業費用
					null, // 其他收益及費損淨額
					null, // 營業利益（損失）
					nonOperatingIncome, // 營業外收入及支出
					null, // 稅前淨利（淨損）
					null, // 所得稅費用（利益）
					Q4.getPeriodIncome() - Q3.getPeriodIncome(), // 繼續營業單位本期淨利（淨損）
					null, // 停業單位損益
					null, // 其他綜合損益（淨額）
					parentCompanyPeriodIncome, // 淨利（淨損）歸屬於母公司業主
					comprehensiveParentCompanyPeriodIncome, // 綜合損益總額歸屬於母公司業主
					eps, // 基本每股盈餘（元）
					Q4.getYear(), Q4.getSeason(), null);
		});
	}

	// 取得全部股價
	public Map<String, Map<String, String>> getPriceFromOpenData() throws Exception, IOException {
//		String stockPriceResponse = restTemplate.getForObject(OPEN_DATA_URL, String.class);
		String stockPriceResponse = readStockPrice();
		if (stockPriceResponse == null) {
			stockPriceResponse = downloadStockPrice();
		}
		String[] messageArray = stockPriceResponse.split("\n");
		Map<String, Map<String, String>> stockMap = new HashMap<>();
		for (int i = 1; i < messageArray.length; i++) {
			String[] stockInfo = messageArray[i].replaceAll("\"", "").split(",");
			try {
				Map<String, String> stockPriceMap = new HashMap<String, String>();
				stockPriceMap.put("stockId", stockInfo[0]);
				stockPriceMap.put("stockName", stockInfo[1]);
				stockPriceMap.put("volume", stockInfo[2]);
				stockPriceMap.put("turnover", stockInfo[3]);
				stockPriceMap.put("openingPrice", stockInfo[4]);
				stockPriceMap.put("highPrice", stockInfo[5]);
				stockPriceMap.put("lowPrice", stockInfo[6]);
				stockPriceMap.put("closePrice", stockInfo[7]);
				stockPriceMap.put("change", stockInfo[8]);
//			stockPriceMap.put("transaction", stockInfo[9]);
				stockMap.put(stockInfo[0], stockPriceMap);
			} catch (Exception ex) {
				log.info(stockInfo[0] + ":" + stockInfo);
			}
		}
		return stockMap;
	}

	private String downloadStockPrice() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<byte[]> response = restTemplate.exchange(OPEN_DATA_URL, HttpMethod.GET, entity, byte[].class);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-hh");
		log.info("下載盤後股價資料:stockPrice" + sdf.format(Calendar.getInstance().getTime()) + ".csv");
		Files.write(Paths.get(
				".\\src\\main\\resources\\tmp\\stockPrice" + sdf.format(Calendar.getInstance().getTime()) + ".csv"),
				response.getBody());
		if (response.getBody() == null) {
			throw new Exception();
		}
		return readStockPrice();
	}

	private String readStockPrice() {
		StringBuilder resultStringBuilder = new StringBuilder();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-hh");
		try {
			File initialFile = new File(
					".\\src\\main\\resources\\tmp\\stockPrice" + sdf.format(Calendar.getInstance().getTime()) + ".csv");
			InputStream targetStream = new FileInputStream(initialFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(targetStream));
			String line;
			while ((line = br.readLine()) != null) {
				resultStringBuilder.append(line + "\n");
			}
		} catch (Exception e) {
			return null;
		}
		return resultStringBuilder.toString();
	}

}
