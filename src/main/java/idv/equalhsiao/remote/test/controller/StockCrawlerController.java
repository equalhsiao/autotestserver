package idv.equalhsiao.remote.test.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.houbb.opencc4j.util.ZhConverterUtil;

import idv.equalhsiao.remote.test.config.CommonResp;
import idv.equalhsiao.remote.test.config.SeleniumProperties;
import idv.equalhsiao.remote.test.po.BalanceSheetEntity;
import idv.equalhsiao.remote.test.po.BalanceSheetRepository;
import idv.equalhsiao.remote.test.po.DividendEntity;
import idv.equalhsiao.remote.test.po.DividendRepository;
import idv.equalhsiao.remote.test.po.EPSEntity;
import idv.equalhsiao.remote.test.po.EPSRepository;
import idv.equalhsiao.remote.test.po.FuturesEntity;
import idv.equalhsiao.remote.test.po.FuturesRepository;
import idv.equalhsiao.remote.test.po.ImportantMessageEntity;
import idv.equalhsiao.remote.test.po.ImportantMessageRepository;
import idv.equalhsiao.remote.test.po.IncomeStatementEntity;
import idv.equalhsiao.remote.test.po.IncomeStatementRepository;
import idv.equalhsiao.remote.test.po.MonthlyRevenueEntity;
import idv.equalhsiao.remote.test.po.MonthlyRevenueRepository;
import idv.equalhsiao.remote.test.po.StockEntity;
import idv.equalhsiao.remote.test.po.StockIndustryPriceEntity;
import idv.equalhsiao.remote.test.po.StockIndustryPriceRepository;
import idv.equalhsiao.remote.test.po.StockRepository;
import idv.equalhsiao.remote.test.service.StockService;
import idv.equalhsiao.remote.test.utils.StockUtil;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "stock")
@Slf4j
public class StockCrawlerController {

	private final short PAGE_LOADING_SECOND = 10;

	// 公開資訊觀測站首頁
	private final String MOPS_INDEX_URL = "https://mops.twse.com.tw/mops/web/index";

	// 公開資訊觀測站股價API
	private final String MOPS_API_URL = "http://mis.twse.com.tw/stock/api/getStockInfo.jsp?ex_ch=tse_";

	@Autowired
	private WebDriver webDriver;

	@Bean
	public WebDriverWait wait(WebDriver webDriver) {
		return new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
	}

	@Autowired
	private WebDriverWait wait;

	@Autowired
	private SeleniumProperties seleniumProperties;
	@Autowired
	private MonthlyRevenueRepository monthlyRevenueRepository;
	@Autowired
	private DividendRepository dividendRepository;
	@Autowired
	private StockRepository stockRepository;
	@Autowired
	private EPSRepository epsRepository;
	@Autowired
	private IncomeStatementRepository incomeStatementRepository;

	@Autowired
	private BalanceSheetRepository balanceSheetRepository;

	@Autowired
	private ImportantMessageRepository importantMessageRepository;

	@Autowired
	private StockIndustryPriceRepository stockIndustryPriceRepository;

	@Autowired
	private FuturesRepository futuresRepository;

//	@Autowired
//	private MailService mailService;

	@Autowired
	private StockService stockService;

	// 期貨
	@GetMapping("/getFutures")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getFutures(@RequestParam(value = "year", required = false) String year,
			@RequestParam(value = "month", required = false) Integer month,
			@RequestParam(value = "day", required = false) Integer day) throws ParseException {
		final String MOPS_URL = "https://www.cnyes.com/futures/basicmetal.aspx?ga=nav";
		webDriver.get(MOPS_URL);
		Date now = Calendar.getInstance().getTime();
		// 轉成西元年
		if (year == null) {
			year = String.valueOf(Integer.valueOf(now.getYear()) + 1900);
		}
		if (month == null) {
			month = now.getMonth() + 1;
		}
		if (day == null) {
			day = now.getDate();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		Date date = sdf.parse(year + "/" + month + "/" + day);
		log.info("爬取期貨漲跌" + year + "年 " + month + "月 " + day + "日 開始");
		List<WebElement> tables = wait.until(ExpectedConditions
				.presenceOfNestedElementsLocatedBy(By.xpath("//div[@class='tab']"), By.tagName("table")));
		List<WebElement> titles = wait.until(ExpectedConditions
				.presenceOfNestedElementsLocatedBy(By.xpath("//div[@class='tab']"), By.tagName("h3")));
		for (int i = 0; i < titles.size(); i++) {
			WebElement title = titles.get(i);
			String category = title.getText().replace("[", "").replace("]", "").trim();
			WebElement table = tables.get(i);
			List<WebElement> trs = wait
					.until(ExpectedConditions.visibilityOfAllElements(table.findElements(By.tagName("tr"))));
			for (int j = 1; j < trs.size(); j++) {
				WebElement tr = trs.get(j);
				List<WebElement> tds = tr.findElements(By.tagName("td"));
				try {
					String name = tds.get(1).getText();
					String exchange = tds.get(2).getText();
					String quotationUnit = tds.get(3).getText();
					Float closePrice = Float.valueOf(tds.get(4).getText().replaceAll(",", ""));
					Float riseFall = Float.valueOf(tds.get(5).getText().replaceAll(",", ""));
					Float riseFallPercentage = Float.valueOf(tds.get(6).getText().replaceAll(",", ""));
					Float openingPrice = Float.valueOf(tds.get(7).getText().replaceAll(",", ""));
					Float highestPrice = Float.valueOf(tds.get(8).getText().replaceAll(",", ""));
					Float lowestPrice = Float.valueOf(tds.get(9).getText().replaceAll(",", ""));
					Integer volume = Integer.valueOf(tds.get(10).getText());
					Integer openPosition = Integer.valueOf(tds.get(11).getText());
					String getDate = tds.get(0).getText();
					// 因getDate:取得時間有不一致所以只能單筆查詢
					FuturesEntity futuresEntity = futuresRepository.findByCategoryAndNameAndExchangeAndGetDate(category,
							name, exchange, getDate);

					if (futuresEntity == null) {
						futuresRepository.save(new FuturesEntity(null, category, name, exchange, quotationUnit,
								closePrice, riseFall, riseFallPercentage, openingPrice, highestPrice, lowestPrice,
								volume, openPosition, getDate));
					}
				} catch (Exception ex) {
					log.info(tr.getAttribute("innerHTML"));
					throw ex;
				}
			}

		}
//		for (WebElement table : tables) {
//			String title = table.findElement(By.xpath("//preceding-sibling::h3")).getText();
//		}
		log.info("爬取期貨漲跌" + year + "年 " + month + "月 " + day + "日 結束");
		return new CommonResp();
	}

	// 中股行業漲跌
	@GetMapping("/getCNStockIndustryPrice")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getCNStockIndustryPrice(
			@RequestParam(value = "year", required = false) String year,
			@RequestParam(value = "month", required = false) Integer month,
			@RequestParam(value = "day", required = false) Integer day) throws ParseException, InterruptedException {
		final String MOPS_URL = "http://quote.eastmoney.com/center/boardlist.html#industry_board";
		try {
			webDriver.get(MOPS_URL);
		} catch (TimeoutException ex) {
			log.info("timeoutexception ignore");
		}
		Date now = Calendar.getInstance().getTime();
		// 轉成西元年
		if (year == null) {
			year = String.valueOf(Integer.valueOf(now.getYear()) + 1900);
		}
		if (month == null) {
			month = now.getMonth() + 1;
		}
		if (day == null) {
			day = now.getDate();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		Date date = sdf.parse(year + "/" + month + "/" + day);
		log.info("爬取中股行業漲跌" + year + "年 " + month + "月 " + day + "日 開始");
		List<WebElement> pages = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
				By.xpath("//a[@class='paginate_button' or @class='paginate_button current']")));
		List<StockIndustryPriceEntity> list = stockIndustryPriceRepository.findByCountryAndUseDate("CN", date);
		for (int j = 0; j < pages.size(); j++) {
			WebElement page = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
					By.xpath("//a[@class='paginate_button' or @class='paginate_button current']"))).get(j);
			wait.until(ExpectedConditions.elementToBeClickable(page)).click();
			List<WebElement> trs = wait.until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[@class='odd' or @class='even']")));
			log.info("page:" + j);
			Thread.sleep(3000);
			for (int i = 0; i < trs.size(); i++) {
				List<WebElement> newTrs = wait.until(ExpectedConditions
						.presenceOfAllElementsLocatedBy(By.xpath("//tr[@class='odd' or @class='even']")));
				WebElement tr = null;
				List<WebElement> tds = null;
				String mainIndustry = null;
				Float riseOrFallPercentage = null;
				log.info("tr:" + i);
				boolean staleElement = true;
				while (staleElement) {
					try {
						newTrs = wait.until(ExpectedConditions
								.presenceOfAllElementsLocatedBy(By.xpath("//tr[@class='odd' or @class='even']")));
						tr = newTrs.get(i);
						tds = tr.findElements(By.tagName("td"));
						mainIndustry = tr.findElements(By.tagName("td")).get(1).getText();
						staleElement = false;
						WebElement td5 = tds.get(5);
						if (StringUtils.isEmpty(td5.getText())) {
							break;
						}
						riseOrFallPercentage = Float.valueOf(td5.getText().replaceAll(",", "").replaceAll("%", ""));
					} catch (StaleElementReferenceException e) {
						Thread.sleep(1000);
						staleElement = true;
						newTrs = wait.until(ExpectedConditions
								.presenceOfAllElementsLocatedBy(By.xpath("//tr[@class='odd' or @class='even']")));
						tr = newTrs.get(i);
						tds = tr.findElements(By.tagName("td"));
						log.info("retry");
					} catch (IndexOutOfBoundsException ex) {
						break;
					} catch (Exception ex) {
						throw ex;
					}
				}
				if (newTrs == null || riseOrFallPercentage == null) {
					break;
				}
				String subIndustry = ZhConverterUtil.convertToTraditional(mainIndustry);
				Optional<StockIndustryPriceEntity> entity = list.stream()
						.filter(e -> e.getSubIndustry().equals(subIndustry)).findFirst();
				if (entity.isPresent()) {
					continue;
				}
				stockIndustryPriceRepository.save(new StockIndustryPriceEntity(null, "CN", mainIndustry, subIndustry,
						riseOrFallPercentage, null, null, sdf.parse(year + "/" + month + "/" + day)));

			}
		}
		log.info("爬取中股行業漲跌" + year + "年 " + month + "月 " + day + "日 結束");
		return new CommonResp();
	}

	// 美股行業漲跌
	@GetMapping("/getUSStockIndustryPrice")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getUSStockIndustryPrice(
			@RequestParam(value = "year", required = false) String year,
			@RequestParam(value = "month", required = false) Integer month,
			@RequestParam(value = "day", required = false) Integer day) throws ParseException {
		final String MOPS_URL = "http://www.aastocks.com/tc/usq/market/industry/industry-performance.aspx";
		webDriver.get(MOPS_URL);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date now = cal.getTime();

		// 轉成西元年
		if (year == null) {
			year = String.valueOf(Integer.valueOf(now.getYear()) + 1900);
		}
		if (month == null) {
			month = now.getMonth() + 1;
		}
		if (day == null) {
			day = now.getDate();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		log.info("爬取美股行業漲跌" + year + "年 " + month + "月 " + day + "日 開始");
		List<WebElement> trs = wait
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[@class='indview_tr ']")));
		for (WebElement tr : trs) {
			List<WebElement> tds = tr.findElements(By.tagName("td"));
			String mainIndustry = tds.get(0).getText();
			String subIndustry = tds.get(1).getText();
			Float riseOrFallPercentage = null;
			String td2Text = tds.get(2).getText();
			if (td2Text.startsWith("+")) {
				riseOrFallPercentage = Float.valueOf(td2Text.substring(1).replaceAll(",", "").replaceAll("%", ""));
			} else {
				riseOrFallPercentage = Float.valueOf(td2Text.replaceAll(",", "").replaceAll("%", ""));
			}
			Float yesterdayRiseOrFallPercentage = null;
			String td3Text = tds.get(3).getText();
			if (td3Text.startsWith("+")) {
				yesterdayRiseOrFallPercentage = Float
						.valueOf(td3Text.substring(1).replaceAll(",", "").replaceAll("%", ""));
			} else {
				yesterdayRiseOrFallPercentage = Float.valueOf(td3Text.replaceAll(",", "").replaceAll("%", ""));
			}
			Float averagePERatio = Float.valueOf(tds.get(4).getText().replaceAll(",", "").replaceAll("%", ""));
			stockIndustryPriceRepository
					.save(new StockIndustryPriceEntity(null, "US", mainIndustry, subIndustry, riseOrFallPercentage,
							yesterdayRiseOrFallPercentage, averagePERatio, sdf.parse(year + "/" + month + "/" + day)));
		}
		log.info("爬取美股行業漲跌" + year + "年 " + month + "月 " + day + "日 結束");
		return new CommonResp();
	}

	// 重大訊息
	@GetMapping("/getImportantMessage")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getImportantMessage(@RequestParam(value = "year") String year,
			@RequestParam(value = "month") Integer month, @RequestParam(value = "day", required = false) Integer day)
			throws ParseException {
		final String MOPS_URL = "https://mops.twse.com.tw/mops/web/t05st02";
		webDriver.get(MOPS_URL);
		if (day == null) {
			day = Calendar.getInstance().getTime().getDate();
		}
		log.info("爬取重大訊息自結" + year + "年 " + month + "月 " + day + "日 開始");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='year']"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='year']"))).sendKeys(year);
		String monthString = month < 10 ? "0" + month : String.valueOf(month);
		new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='month']"))))
				.selectByValue(monthString);
		String dayString = day < 10 ? "0" + day : String.valueOf(day);
		new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='day']"))))
				.selectByValue(dayString);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='search']/input[@value=' 查詢 ']")))
				.click();
		final String keyWord1 = "因本公司有價證券於集中市場達公布注意交易資訊";
		List<WebElement> trs = null;
		try {
			trs = wait.until(ExpectedConditions
					.presenceOfAllElementsLocatedBy(By.xpath("//td[contains(text(),'" + keyWord1 + "')]/..")));
		} catch (TimeoutException ex) {
			log.info("未找到相關重大資訊");
			return new CommonResp();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		for (WebElement tr : trs) {
			List<WebElement> tds = tr.findElements(By.tagName("td"));
			String announcementDate = (Integer.valueOf(tds.get(0).getText().trim().substring(0, 3)) + 1911)
					+ tds.get(0).getText().substring(4);
			String announcementTime = tds.get(1).getText();
			String stockId = tds.get(2).getText().trim();
			String stockName = tds.get(3).getText().trim();
			String innerHTML = tds.get(5).getAttribute("innerHTML");
			List<WebElement> inputs = tds.get(5).findElements(By.tagName("input"));
			Integer serialNumber = Integer.valueOf(inputs.get(5).getAttribute("value"));

			String newInnerHTML = innerHTML.substring(innerHTML.indexOf("3.財務業務資訊:") + "3.財務業務資訊:".length(),
					innerHTML.indexOf("4.有無「臺灣證券交易所股份有限公司"));
			Map<String, String> incomeMap = findImportantMessage(newInnerHTML, "營業收入", "Income");
			Integer currentIncome = Integer.valueOf(incomeMap.get("currentIncome").replaceAll(",", ""));
			Float preYearIncomePercentage = Float.valueOf(incomeMap.get("preYearIncomePercentage").replaceAll(",", ""));
			Integer preSeasonIncome = Integer.valueOf(incomeMap.get("preSeasonIncome").replaceAll(",", ""));
			Float preSeasonIncomePercentage = Float
					.valueOf(incomeMap.get("preSeasonIncomePercentage").replaceAll(",", ""));
			Integer recentFourSeasonIncome = Integer
					.valueOf(incomeMap.get("recentFourSeasonIncome").replaceAll(",", ""));

			Map<String, String> netProfitBeforeTaxMap = findImportantMessage(newInnerHTML, "稅前淨利",
					"NetProfitBeforeTax");
			Integer currentNetProfitBeforeTax = Integer
					.valueOf(netProfitBeforeTaxMap.get("currentNetProfitBeforeTax").replaceAll(",", ""));
			Float preYearNetProfitBeforeTaxPercentage = Float
					.valueOf(netProfitBeforeTaxMap.get("preYearNetProfitBeforeTaxPercentage").replaceAll(",", ""));
			Integer preSeasonNetProfitBeforeTax = Integer
					.valueOf(netProfitBeforeTaxMap.get("preSeasonNetProfitBeforeTax").replaceAll(",", ""));
			Float preSeasonNetProfitBeforeTaxPercentage = Float
					.valueOf(netProfitBeforeTaxMap.get("preSeasonNetProfitBeforeTaxPercentage").replaceAll(",", ""));
			Integer recentFourSeasonNetProfitBeforeTax = Integer
					.valueOf(netProfitBeforeTaxMap.get("recentFourSeasonNetProfitBeforeTax").replaceAll(",", ""));

			Map<String, String> periodIncomeMap = findImportantMessage(newInnerHTML, "本期淨利", "PeriodIncome");
			Integer currentPeriodIncome = Integer
					.valueOf(periodIncomeMap.get("currentPeriodIncome").replaceAll(",", ""));
			Float preYearPeriodIncomePercentage = Float
					.valueOf(periodIncomeMap.get("preYearPeriodIncomePercentage").replaceAll(",", ""));
			Integer preSeasonPeriodIncome = Integer
					.valueOf(periodIncomeMap.get("preSeasonPeriodIncome").replaceAll(",", ""));
			Float preSeasonPeriodIncomePercentage = Float
					.valueOf(periodIncomeMap.get("preSeasonPeriodIncomePercentage").replaceAll(",", ""));
			Integer recentFourSeasonPeriodIncome = Integer
					.valueOf(periodIncomeMap.get("recentFourSeasonPeriodIncome").replaceAll(",", ""));

			Map<String, String> epsMap = findImportantMessage(newInnerHTML, "每股盈餘(元)", "EPS");

			Float currentEps = Float.valueOf(epsMap.get("currentEPS").replaceAll(",", ""));
			Float preYearEpsPercentage = Float.valueOf(epsMap.get("preYearEPSPercentage").replaceAll(",", ""));
			Float preSeasonEps = Float.valueOf(epsMap.get("preSeasonEPS").replaceAll(",", ""));
			Float preSeasonEpsPercentage = Float.valueOf(epsMap.get("preSeasonEPSPercentage").replaceAll(",", ""));
			Float recentFourSeasonEps = Float.valueOf(epsMap.get("recentFourSeasonEPS").replaceAll(",", ""));
			importantMessageRepository.save(new ImportantMessageEntity(null, stockId, stockName, serialNumber,
					currentIncome, preYearIncomePercentage, preSeasonIncome, preSeasonIncomePercentage,
					recentFourSeasonIncome, currentNetProfitBeforeTax, preYearNetProfitBeforeTaxPercentage,
					preSeasonNetProfitBeforeTax, preSeasonNetProfitBeforeTaxPercentage,
					recentFourSeasonNetProfitBeforeTax, currentPeriodIncome, preYearPeriodIncomePercentage,
					preSeasonPeriodIncome, preSeasonPeriodIncomePercentage, recentFourSeasonPeriodIncome, currentEps,
					preYearEpsPercentage, preSeasonEps, preSeasonEpsPercentage, recentFourSeasonEps,
					new Timestamp(sdf.parse(announcementDate + " " + announcementTime).getTime()), year, month, day));

		}
		log.info("爬取重大訊息自結" + year + "年 " + month + "月 " + day + "日 結束");
		return new CommonResp();
	}

	private Map<String, String> findImportantMessage(String innerHTML, String keyWord, String fix) {
		int charSize = innerHTML.indexOf(keyWord) + keyWord.length();
		Map<String, String> map = new HashMap<String, String>();
		String composeWord = "";
		while (map.size() < 5) {
			try {
				char c = innerHTML.charAt(charSize);
				if (c == '.' || c == ',' || Character.isDigit(c)) {
					composeWord += c;
				} else if (c == 32 || c == '%' || charSize + 1 == innerHTML.length()) {
					if (isNumeric(composeWord.replaceAll(",", ""))) {
						if (map.size() == 0) {
							map.put("current" + fix, composeWord);
						} else if (map.size() == 1) {
							map.put("preYear" + fix + "Percentage", composeWord);
						} else if (map.size() == 2) {
							map.put("preSeason" + fix, composeWord);
						} else if (map.size() == 3) {
							map.put("preSeason" + fix + "Percentage", composeWord);
						} else if (map.size() == 4) {
							map.put("recentFourSeason" + fix, composeWord);
						}
						composeWord = "";
					}

				}
//				
//				else {
//					map.put("recentFourSeasons" + fix, composeWord);
//					composeWord = "";
//				}
				charSize++;
			} catch (Exception ex) {
				log.info("map:" + map);
				throw ex;
			}
		}
		return map;
	}

	private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

	private boolean isNumeric(String strNum) {
		if (strNum == null) {
			return false;
		}
		return pattern.matcher(strNum).matches();
	}

	// 取得資產損益表
	@GetMapping("/getBalanceSheet")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getBalanceSheet(@RequestParam(value = "year") String year,
			@RequestParam(value = "season") Short season) {
		final String MOPS_URL = "https://mops.twse.com.tw/mops/web/t163sb05";
		webDriver.get(MOPS_URL);
		log.info("爬取資產損益表" + year + "年 第" + season + "季開始");
		new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='TYPEK']"))))
				.selectByIndex(0);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='year']"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='year']"))).sendKeys(year);
		new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='season']"))))
				.selectByValue("0" + season);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='search']/input[@value=' 查詢 ']")))
				.click();
		List<WebElement> trs = wait
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[@class='even']")));
		Long exitDataCount = balanceSheetRepository.countByYearAndSeason(year, season);
		log.info("exitDataCount:" + exitDataCount);
		log.info("trs:" + trs.size());
		Calendar now = Calendar.getInstance();
		Timestamp announcementDate = new Timestamp(now.getTimeInMillis());
		List<BalanceSheetEntity> exitList = balanceSheetRepository.findByYearAndSeason(year, season);
		int count = 0;
		for (WebElement tr : trs) {
			List<WebElement> tds = tr.findElements(By.tagName("td"));
			String stockId = tds.get(0).getText();
			try {
				if (exitList.stream().anyMatch(e -> stockId.equals(e.getStockId()))) {
					continue;
				}
				String stockName = tds.get(1).getText();
				Long currentAssets = null;
				Long nonCurrentAssets = null;
				Long totalAssets = null;
				Long currentLiabilities = null;
				Long nonCurrentLiabilities = null;
				Long totalLiabilities = null;
				Long capitalStock = null;
				Long capitalReserve = null;
				Long retainedSurplus = null;
				Long otherRights = null;
				Long treasuryStock = null;
				Long attributableParentCompanyTotalEquity = null;
				Long nonControllingInterests = null;
				Long totalEquity = null;
				Long cancelCapital = null;
				Long preRecepitShares = null;
				Long subsidiaryTreasuryStock = null;
				Float referenceShareValue = null;
				int size = tds.size();
				if (size == 23) {
					currentAssets = Long.valueOf(tds.get(2).getText().replaceAll(",", ""));
					nonCurrentAssets = Long.valueOf(tds.get(3).getText().replaceAll(",", ""));
					totalAssets = Long.valueOf(tds.get(4).getText().replaceAll(",", ""));
					currentLiabilities = Long.valueOf(tds.get(5).getText().replaceAll(",", ""));
					nonCurrentLiabilities = Long.valueOf(tds.get(6).getText().replaceAll(",", ""));
					totalLiabilities = Long.valueOf(tds.get(7).getText().replaceAll(",", ""));
					capitalStock = Long.valueOf(tds.get(8).getText().replaceAll(",", ""));
					capitalReserve = checkText(tds.get(10).getText());
					retainedSurplus = Long.valueOf(tds.get(11).getText().replaceAll(",", ""));
					otherRights = checkText(tds.get(12).getText());
					treasuryStock = checkText(tds.get(13).getText());
					attributableParentCompanyTotalEquity = checkText(tds.get(14).getText());
					nonControllingInterests = checkText(tds.get(17).getText());
					totalEquity = Long.valueOf(tds.get(18).getText().replaceAll(",", ""));
					cancelCapital = checkText(tds.get(19).getText());
					preRecepitShares = checkText(tds.get(20).getText());
					subsidiaryTreasuryStock = checkText(tds.get(21).getText());
					referenceShareValue = Float.valueOf(tds.get(22).getText().replaceAll(",", ""));
					balanceSheetRepository.save(new BalanceSheetEntity(null, stockId, stockName, currentAssets,
							nonCurrentAssets, totalAssets, currentLiabilities, nonCurrentLiabilities, totalLiabilities,
							capitalStock, capitalReserve, retainedSurplus, otherRights, treasuryStock,
							attributableParentCompanyTotalEquity, nonControllingInterests, totalEquity, cancelCapital,
							preRecepitShares, subsidiaryTreasuryStock, referenceShareValue, year, season,
							announcementDate));
					count++;
				} else if (Integer.valueOf(year) < 110 && season == 1 && size == 22) {
					currentAssets = Long.valueOf(tds.get(2).getText().replaceAll(",", ""));
					nonCurrentAssets = Long.valueOf(tds.get(3).getText().replaceAll(",", ""));
					totalAssets = Long.valueOf(tds.get(4).getText().replaceAll(",", ""));
					currentLiabilities = Long.valueOf(tds.get(5).getText().replaceAll(",", ""));
					nonCurrentLiabilities = Long.valueOf(tds.get(6).getText().replaceAll(",", ""));
					totalLiabilities = Long.valueOf(tds.get(7).getText().replaceAll(",", ""));
					capitalStock = Long.valueOf(tds.get(8).getText().replaceAll(",", ""));
					capitalReserve = checkText(tds.get(9).getText());
					retainedSurplus = Long.valueOf(tds.get(10).getText().replaceAll(",", ""));
					otherRights = checkText(tds.get(11).getText());
					treasuryStock = checkText(tds.get(12).getText());
					attributableParentCompanyTotalEquity = checkText(tds.get(13).getText());
					nonControllingInterests = checkText(tds.get(16).getText());
					totalEquity = Long.valueOf(tds.get(17).getText().replaceAll(",", ""));
					cancelCapital = checkText(tds.get(18).getText());
					preRecepitShares = checkText(tds.get(19).getText());
					subsidiaryTreasuryStock = checkText(tds.get(20).getText());
					referenceShareValue = Float.valueOf(tds.get(21).getText().replaceAll(",", ""));
					balanceSheetRepository.save(new BalanceSheetEntity(null, stockId, stockName, currentAssets,
							nonCurrentAssets, totalAssets, currentLiabilities, nonCurrentLiabilities, totalLiabilities,
							capitalStock, capitalReserve, retainedSurplus, otherRights, treasuryStock,
							attributableParentCompanyTotalEquity, nonControllingInterests, totalEquity, cancelCapital,
							preRecepitShares, subsidiaryTreasuryStock, referenceShareValue, year, season,
							announcementDate));
					count++;
				}

			} catch (Exception ex) {
				log.info("stockId:" + stockId);
				throw ex;
			}
		}
		log.info("資產損益表新增" + count + "筆資料");
		log.info("爬取資產損益表" + year + "年 第" + season + "季結束");
		return new CommonResp();
	}

	// 取得綜合損益表
	@GetMapping("/getIncomeStatement")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getIncomeStatement(@RequestParam(value = "year") String year,
			@RequestParam(value = "season") Short season) throws Exception {
		final String MOPS_URL = "https://mops.twse.com.tw/mops/web/t163sb04";
		webDriver.get(MOPS_URL);
		log.info("查詢綜合損益表" + year + "年 第" + season + "季開始");
//		WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
		new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='TYPEK']"))))
				.selectByIndex(0);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='year']"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='year']"))).sendKeys(year);
		new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='season']"))))
				.selectByValue("0" + season);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='search']/input[@value=' 查詢 ']")))
				.click();
		List<WebElement> trs = wait
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[@class='even']")));
		Calendar today = Calendar.getInstance();
		Timestamp now = new Timestamp(today.getTimeInMillis());
		Map<String, List<IncomeStatementEntity>> stockMap = new HashMap<String, List<IncomeStatementEntity>>();
		List<IncomeStatementEntity> allIncomeStatement = incomeStatementRepository.findByYearAndSeason(year, season);
		Long step1 = Calendar.getInstance().getTimeInMillis();
		int count = 0;
		log.info("爬取綜合損益表開始");
		for (WebElement tr : trs) {
			List<WebElement> tds = tr.findElements(By.tagName("td"));
			String stockId = tds.get(0).getText();
			try {
				if (allIncomeStatement.stream().anyMatch(e -> stockId.equals(e.getStockId()))) {
					continue;
				}
				String stockName = tds.get(1).getText();
				Long operatingCost = null;
				Long income = null;
				Long unrealizedSales = null;
				Long realizedSales = null;
				Long netOperatingProfit = null;
				Long operatingExpenses = null;
				Long otherIncome = null;
				Long businessInterest = null;
				Long nonOperatingIncome = null;
				Long netProfitBeforeTax = null;
				Long incomeTaxExpense = null;
				Long periodIncome = null;
				Long discountinuedUnitProfit = null;
				Long otherComprehensiveProfit = null;
				Long parentCompanyPeriodIncome = null;
				Long comprehensiveParentCompanyPeriodIncome = null;
				Float eps = null;
				if (tds.size() == 30) {
					income = checkText(tds.get(2).getText());// 營業收入
					operatingCost = checkText(tds.get(3).getText());// 營業成本
					netOperatingProfit = checkText(tds.get(6).getText());// 營業毛利(毛損)
					unrealizedSales = checkText(tds.get(7).getText());// 未實現銷貨(損)益
					realizedSales = checkText(tds.get(8).getText());// 已實現銷貨(損)益
					operatingExpenses = checkText(tds.get(10).getText());// 營業費用
					otherIncome = checkText(tds.get(11).getText());// 其他收益及費損淨額
					businessInterest = checkText(tds.get(12).getText());// 營業利益（損失）
					nonOperatingIncome = checkText(tds.get(13).getText()); // 營業外收入及支出
					netProfitBeforeTax = checkText(tds.get(14).getText());// 稅前淨利（淨損）
					incomeTaxExpense = checkText(tds.get(15).getText());// 所得稅費用（利益）
					periodIncome = checkText(tds.get(16).getText());// 繼續營業單位本期淨利（淨損）
					discountinuedUnitProfit = checkText(tds.get(17).getText());// 停業單位損益
					otherComprehensiveProfit = checkText(tds.get(20).getText());// 其他綜合損益（淨額）
					parentCompanyPeriodIncome = checkText(tds.get(23).getText()); // 淨利（淨損）歸屬於母公司業主
					comprehensiveParentCompanyPeriodIncome = checkText(tds.get(26).getText());// 綜合損益總額歸屬於母公司業主
					eps = Float.valueOf(tds.get(29).getText().replace(",", ""));// 基本每股盈餘（元）
				} else if (tds.size() == 23) {
					income = checkText(tds.get(2).getText());// 營業收入
					operatingCost = checkText(tds.get(3).getText());// 營業成本
					operatingExpenses = checkText(tds.get(4).getText());// 營業費用
					businessInterest = checkText(tds.get(5).getText());// 營業利益
					nonOperatingIncome = checkText(tds.get(6).getText());// 營業外收入及支出
					netProfitBeforeTax = checkText(tds.get(7).getText());// 繼續營業單位稅前純益（純損）
					incomeTaxExpense = checkText(tds.get(8).getText());// 所得稅費用（利益）
					periodIncome = checkText(tds.get(9).getText());// 繼續營業單位本期純益（純損）
					discountinuedUnitProfit = checkText(tds.get(10).getText());// 停業單位損益
					otherComprehensiveProfit = checkText(tds.get(13).getText());// 其他綜合損益（稅後淨額）
					parentCompanyPeriodIncome = checkText(tds.get(16).getText());// 淨利（淨損）歸屬於母公司業主
					comprehensiveParentCompanyPeriodIncome = checkText(tds.get(19).getText());// 綜合損益總額歸屬於母公司業主
					eps = Float.valueOf(tds.get(22).getText().replace(",", ""));// 基本每股盈餘（元）
				} else if (tds.size() == 22) {
					income = checkText(tds.get(2).getText());// 利息淨收益
					income = income + checkText(tds.get(3).getText());// 利息以外淨損益
					operatingExpenses = checkText(tds.get(5).getText());// 營業費用
					netProfitBeforeTax = checkText(tds.get(6).getText());// 繼續營業單位稅前淨利（淨損）
					incomeTaxExpense = checkText(tds.get(7).getText());// 所得稅費用（利益）
					periodIncome = checkText(tds.get(8).getText());// 繼續營業單位本期稅後淨利（淨損）
					discountinuedUnitProfit = checkText(tds.get(9).getText());// 停業單位損益
					otherComprehensiveProfit = checkText(tds.get(12).getText());// 其他綜合損益（稅後淨額）
					parentCompanyPeriodIncome = checkText(tds.get(15).getText());// 淨利（損）歸屬於母公司業主
					comprehensiveParentCompanyPeriodIncome = checkText(tds.get(18).getText());// 綜合損益總額歸屬於母公司業主
					eps = Float.valueOf(tds.get(21).getText().replace(",", ""));// 基本每股盈餘（元）
				} else if (tds.size() == 18) {
					income = checkText(tds.get(2).getText());// 收入
					operatingCost = checkText(tds.get(3).getText());// 支出
					netProfitBeforeTax = checkText(tds.get(4).getText());// 繼續營業單位稅前淨利（淨損）
					incomeTaxExpense = checkText(tds.get(5).getText());// 所得稅費用（利益）
					periodIncome = checkText(tds.get(6).getText());// 繼續營業單位本期淨利（淨損）
					discountinuedUnitProfit = checkText(tds.get(7).getText());// 停業單位損益
					otherComprehensiveProfit = checkText(tds.get(9).getText());// 其他綜合損益（稅後淨額）
					parentCompanyPeriodIncome = checkText(tds.get(11).getText());// 淨利（淨損）歸屬於母公司業主
					comprehensiveParentCompanyPeriodIncome = checkText(tds.get(14).getText());// 綜合損益總額歸屬於母公司業主
					eps = Float.valueOf(tds.get(17).getText().replace(",", ""));// 基本每股盈餘（元）
				}
				IncomeStatementEntity entity = incomeStatementRepository.findByStockIdAndYearAndSeason(stockId, year,
						season);
				if (entity == null) {
					StockEntity stockBaseInfo = stockRepository.findByStockId(stockId);
					String industry = stockBaseInfo.getIndustry();
					entity = new IncomeStatementEntity(null, stockId, stockName, industry, income, operatingCost,
							unrealizedSales, realizedSales, netOperatingProfit, operatingExpenses, otherIncome,
							businessInterest, nonOperatingIncome, netProfitBeforeTax, incomeTaxExpense, periodIncome,
							discountinuedUnitProfit, otherComprehensiveProfit, parentCompanyPeriodIncome,
							comprehensiveParentCompanyPeriodIncome, eps, year, season, now);
					new ArrayList<IncomeStatementEntity>(100);
					List<IncomeStatementEntity> list = stockMap.getOrDefault(industry,
							new LinkedList<IncomeStatementEntity>());
					list.add(entity);
					stockMap.put(industry, stockMap.getOrDefault(industry, list));
					incomeStatementRepository.save(entity);
					count++;
				}
			} catch (Exception e) {
				log.info("stockId:" + stockId);
				throw e;
			}
		}
		log.info("綜合損益表新增" + count + "筆資料");
		Long step2 = (Calendar.getInstance().getTimeInMillis() - step1) / 1000;
		log.info("爬取綜合損益表結束 執行時間:" + step2 + "秒");
//		log.info("準備email開始");
//		String preYear = String.valueOf(Integer.valueOf(year) - 1);
//		Map<String, List<IncomeStatementDto>> emailMap = stockService.incomeStatement(stockIds, year, season);
//		if (emailMap.size() > 0) {
//			Mail mail = mailService.genMail("綜合損益表 " + year + "年 第" + season + "季");
//			Context context = new Context();
//			context.setVariable("templateName", "incomeStatement");
//			context.setVariable("year", year);
//			context.setVariable("season", season);
//			context.setVariable("preYear", preYear);
//			context.setVariable("incomeStatementMap", emailMap);
//			mailService.sendMail(mail, context);
//		}
//		Long step3 = ((Calendar.getInstance().getTimeInMillis() - step1) / 1000) - step2;
//		log.info("準備email結束 執行時間:" + step3 + "秒");
		log.info("查詢綜合損益表" + year + "年 第" + season + "季結束");
		return new CommonResp();
	}

	private Long checkText(String text) {
		return "--".equals(text) ? null : Long.valueOf(text.replace(",", ""));
	}

	// 停用:取得單季EPS
	private Optional<EPSEntity> getPreEPS(String stockId, String year, String season) {
		// 第一季
		List<Short> seasonList = new ArrayList<Short>();
		if ("1".equals(season)) {
			String preYear = String.valueOf(Integer.valueOf(year) - 1);
			// 固定用第4季-第3季
			seasonList.add((short) 3);
			seasonList.add((short) 4);
			List<EPSEntity> preYearList = epsRepository.findByStockIdAndYearAndSeasonInOrderBySeasonDesc(stockId,
					preYear, seasonList);
			return preYearList.stream()
					.reduce((Q4, Q3) -> new EPSEntity(null, Q4.getStockId(), Q4.getStockFullName(), Q4.getIndustry(),
							Q4.getYear(), Q4.getEps() - Q3.getEps(), Q4.getOperatingIncome() - Q3.getOperatingIncome(),
							Q4.getBusinessInterest() - Q3.getBusinessInterest(),
							Q4.getNonOperatingIncome() - Q3.getNonOperatingIncome(),
							Q4.getNetIncome() - Q3.getNetIncome(), Q4.getAnnouncementDate(), Q4.getSeason()));
		}
		// 第234季
		Short seasonShort = new Short(season);
		seasonList.add((short) (seasonShort));
		seasonList.add((short) (seasonShort - 1));
		List<EPSEntity> currentYearList = epsRepository.findByStockIdAndYearAndSeasonInOrderBySeasonDesc(stockId, year,
				seasonList);
		return currentYearList.stream()
				.reduce((a, b) -> new EPSEntity(null, a.getStockId(), a.getStockFullName(), a.getIndustry(),
						a.getYear(), a.getEps() - b.getEps(), a.getOperatingIncome() - b.getOperatingIncome(),
						a.getBusinessInterest() - b.getBusinessInterest(),
						a.getNonOperatingIncome() - b.getNonOperatingIncome(), a.getNetIncome() - b.getNetIncome(),
						a.getAnnouncementDate(), a.getSeason()));
	}

	// 停用:查詢季EPS
	@GetMapping("/getEPS")
	@Transactional
	private CommonResp<Map<String, Object>> getEPSSesaonReport(@RequestParam(value = "year") String year,
			@RequestParam(value = "season") String season) throws Exception {
		log.info("查詢季EPS" + year + "年:" + season + "季開始");
		Short seasonShort = Short.valueOf(season);
		Short preSeasonShort = StockUtil.preSeasonCal(seasonShort);

		String preYear = String.valueOf(Integer.valueOf(year) - 1);
		String preSeasonYear = preSeasonShort > seasonShort ? preYear : year;
		Map<String, List<EPSEntity>> industryGroup = getEPS(year, season);
		Map<String, Map<String, String>> stockMap = stockService.getPriceFromOpenData();
//		if (industryGroup.size() > 0) {
//			MimeMessage mimeMessage = mailSender.createMimeMessage();
//			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
//			StringBuffer htmlMsg = new StringBuffer();
//			for (String industry : industryGroup.keySet()) {
//				List<EPSEntity> industryList = industryGroup.get(industry);
//				htmlMsg.append("<table border=1 >");
//				htmlMsg.append("<thead>");
//				htmlMsg.append("<tr bgcolor='#6EFF6E'><th>代號</th><th>名稱</th><th>當前股價</th><th>成交量(張)</th><th>EPS(" + year
//						+ "年 第" + season + "季)</th><th>去年EPS(" + preYear + "年 第" + season
//						+ "季)</th><th>營業收入(仟)</th><th>業外收入(仟)</th><th>稅後淨利(仟)</th><th>同期稅後淨利(仟)</th><th>同期稅後淨利增減</th><th>業外佔比</th><th>前季EPS("
//						+ preSeasonYear + "年 第" + preSeasonShort + "季)</th><th>前季稅後淨利(仟)</th><th>前季稅後淨利增減</th></tr>");
//				htmlMsg.append("</thead>");
//				htmlMsg.append("<tbody>");
//				if (industryList.size() > 0) {
//					htmlMsg.append("<tr><td colspan='15' align='center' bgcolor='#BFFFFF' >" + industry + "</td></tr>");
//				}
//				for (EPSEntity e : industryList) {
//					try {
//						htmlMsg.append("<tr>");
//						htmlMsg.append("<td><a href='https://goodinfo.tw/StockInfo/StockDetail.asp?STOCK_ID="
//								+ e.getStockId() + "'>" + e.getStockId() + "</a></td>");
//						htmlMsg.append("<td>" + e.getStockFullName() + "</td>");
//						Map<String, String> openDataMap = stockMap.get(e.getStockId());
//						htmlMsg.append("<td>" + openDataMap.get("closePrice") + "</td>");
//						htmlMsg.append("<td>" + (Integer.valueOf(openDataMap.get("volume")) / 1000) + "</td>");
//
//						EPSEntity preYearEPSEntity = epsRepository.findByStockIdAndYearAndSeason(e.getStockId(),
//								preYear, seasonShort);
//						htmlMsg.append("<td align='right' >" + e.getEps() + "</td>");
//						if (preYearEPSEntity == null) {
//							htmlMsg.append("<td align='right' >NA</td>");
//						} else {
//							htmlMsg.append("<td align='right' >" + preYearEPSEntity.getEps() + "</td>");
//						}
//						htmlMsg.append("<td align='right' >" + e.getOperatingIncome() + "</td>");
//						htmlMsg.append("<td align='right' >" + e.getNonOperatingIncome() + "</td>");
//						htmlMsg.append("<td align='right' >" + e.getNetIncome() + "</td>");
//						if (preYearEPSEntity == null) {
//							htmlMsg.append("<td align='right' >NA</td>");
//							htmlMsg.append("<td align='right' >NA</td>");
//						} else {
//							htmlMsg.append("<td align='right' >" + preYearEPSEntity.getNetIncome() + "</td>");
//							Float netIncomeRate = (Float.valueOf(e.getNetIncome() - preYearEPSEntity.getNetIncome())
//									/ Math.abs(preYearEPSEntity.getNetIncome())) * 100;
//							htmlMsg.append("<td align='right' bgcolor='#FFFF63' >" + netIncomeRate + "%</td>");
//						}
//						htmlMsg.append("<td align='right' >"
//								+ Float.valueOf(e.getNonOperatingIncome()) / e.getOperatingIncome() * 100f + "</td>");
//						Optional<EPSEntity> preSeasonEPSEntity = getPreEPS(e.getStockId(), year, season);
//						if ("1".equals(season)) {
//							if (preSeasonEPSEntity.isPresent()) {
//								EPSEntity entity = preSeasonEPSEntity.get();
//								htmlMsg.append("<td align='right' >" + entity.getEps() + "</td>");
//								htmlMsg.append("<td align='right' >" + entity.getNetIncome() + "</td>");
//								Float netIncomeRate = (Float.valueOf(e.getNetIncome() - entity.getNetIncome())
//										/ Math.abs(entity.getNetIncome())) * 100;
//								htmlMsg.append("<td align='right' bgcolor='#FFFF63'>" + netIncomeRate + "%</td>");
//							} else {
//								htmlMsg.append("<td align='right' >NA</td>");
//								htmlMsg.append("<td align='right' >NA</td>");
//								htmlMsg.append("<td align='right' >NA</td>");
//							}
//
//						} else {
//							if (preSeasonEPSEntity.isPresent()) {
//								EPSEntity entity = preSeasonEPSEntity.get();
//								Float currentEPS = (e.getEps() - entity.getEps());
//								Long currentNetIncome = (e.getNetIncome() - entity.getNetIncome());
//								htmlMsg.append("<td align='right' >" + currentEPS + "</td>");
//								htmlMsg.append("<td align='right' >" + currentNetIncome + "</td>");
//								Float netIncomeRate = (Float.valueOf(currentNetIncome - entity.getNetIncome())
//										/ Math.abs(currentNetIncome)) * 100;
//								htmlMsg.append("<td align='right' bgcolor='#FFFF63'>" + netIncomeRate + "%</td>");
//							} else {
//								htmlMsg.append("<td align='right' >NA</td>");
//								htmlMsg.append("<td align='right' >NA</td>");
//								htmlMsg.append("<td align='right' >NA</td>");
//							}
//
//						}
//						htmlMsg.append("</tr>");
//					} catch (Exception exception) {
//						log.info("stockId:" + e.getStockId());
//						throw exception;
//					}
//				}
//				htmlMsg.append("</tbody>");
//				htmlMsg.append("</table>");
//			}
//			helper.setTo(RECIPIENT);
//			helper.setFrom("xyz90161@gmail.com");
//			helper.setSubject("累計EPS統計資訊" + year + "年 第" + season + "季");
//			helper.setText(htmlMsg.toString(), true); // Use this or above line.
//			mailSender.send(mimeMessage);
//		}
		log.info("查詢季EPS" + year + "年:" + season + "季結束");
		return new CommonResp();

	}

	// 爬EPS資料
	private Map<String, List<EPSEntity>> getEPS(String year, String season) {
		final String MOPS_URL = "https://mops.twse.com.tw/mops/web/t163sb19";
		webDriver.get(MOPS_URL);
//		WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
		new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='code']"))))
				.selectByIndex(0);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='year']"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='year']"))).sendKeys(year);
		new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='season']"))))
				.selectByValue("0" + season);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='search']/input[@value=' 查詢 ']")))
				.click();

		List<WebElement> trs = wait.until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//td[@class='even' or @class='odd']/..")));
		Short seasonShort = Short.valueOf(season);
		Map<String, List<EPSEntity>> industryGroup = new HashMap<>();
		Calendar now = Calendar.getInstance();
		for (WebElement tr : trs) {
			List<WebElement> tds = tr.findElements(By.tagName("td"));
			if (tds.size() < 2) {
				continue;
			}
			String stockId = tds.get(0).getText().trim();
			try {
				String stockFullName = tds.get(1).getText().trim();
				String industry = tds.get(2).getText().trim();
				Float eps = Float.valueOf(tds.get(3).getText().trim());
				Long operatingIncome = Long.valueOf(tds.get(5).getText().replaceAll(",", "").trim());// 營業收入
				String businessInterestStr = tds.get(6).getText();
				Long businessInterest = 0l;
				if (!"--".equals(businessInterestStr))
					businessInterest = Long.valueOf(tds.get(6).getText().replaceAll(",", "").trim());// 營業利益
				String nonOperatingIncomeStr = tds.get(7).getText();
				Long nonOperatingIncome = 0l;
				if (!"--".equals(nonOperatingIncomeStr))
					nonOperatingIncome = Long.valueOf(tds.get(7).getText().replaceAll(",", "").trim());// 營業外收入
				Long netIncome = Long.valueOf(tds.get(8).getText().replaceAll(",", "").trim());// 稅後淨利
				Date announcementTime = now.getTime();
				List<EPSEntity> list = industryGroup.getOrDefault(industry, new ArrayList<EPSEntity>());
				EPSEntity stockEntity = epsRepository.findByStockIdAndYearAndSeason(stockId, year, seasonShort);
				if (stockEntity == null) {
					stockEntity = new EPSEntity(null, stockId, stockFullName, industry, year, eps, operatingIncome,
							businessInterest, nonOperatingIncome, netIncome, announcementTime, Short.valueOf(season));
					list.add(stockEntity);
					industryGroup.put(industry, industryGroup.getOrDefault(industry, list));
				}
				epsRepository.save(stockEntity);
			} catch (Exception e) {
				log.info("stockId:" + stockId);
				throw e;
			}
		}
		return industryGroup;
	}

	// 上市公司更新
	@GetMapping("/getStockInfo")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getStockInfo() throws InterruptedException {
		log.info("查詢上市公司開始");
		// 基本資料查詢彙總表
		final String MOPS_URL = "https://mops.twse.com.tw/mops/web/t51sb01";
		webDriver.get(MOPS_URL);
//		WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
		Select select = new Select(
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='code']"))));
		select.selectByIndex(0);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='search']/input[@value=' 查詢 ']")))
				.click();
		List<WebElement> trs = wait.until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[@class='even' or @class='odd']")));
		List<StockEntity> stockList = new LinkedList<>();
		for (WebElement tr : trs) {
			List<WebElement> tds = tr.findElements(By.tagName("td"));
			String stockId = tds.get(0).getText().trim();
			String stockName = tds.get(2).getText().trim();
			String industry = tds.get(3).getText().trim();
			StockEntity stockEntity = stockRepository.findByStockIdAndStockNameAndIndustry(stockId, stockName,
					industry);
			if (stockEntity == null) {
				stockEntity = new StockEntity(null, stockId, stockName, industry);
			}
			stockRepository.save(stockEntity);
		}
		log.info("已新增" + stockList.size() + "筆資料");
		Thread.sleep(3000);
		log.info("查詢上市公司結束");
		return new CommonResp();
	}

	// 月營收
	@GetMapping("/getReleaseTime")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getReleaseTime(@RequestParam(value = "year") String year,
			@RequestParam(value = "monthly") Integer monthly) throws Exception {
		log.info("執行月營收查詢開始");
		final Integer revenueMonthly = monthly - 1;
		// 營業收入統計表
		final String MOPS_URL = "https://mops.twse.com.tw/nas/t21/sii/t21sc03_110_" + revenueMonthly + "_0.html";
		webDriver.get(MOPS_URL);
		List<WebElement> tables = webDriver
				.findElements(By.xpath("//table[@border='5' and @bordercolor='#FF6600']/../../../.."));
		Calendar calendar = Calendar.getInstance();
		calendar.set(2021, revenueMonthly, 1, 0, 0, 0);
		Timestamp startDate = new Timestamp(calendar.getTimeInMillis());
		calendar.set(2021, monthly, 31, 23, 59, 59);
		Timestamp endDate = new Timestamp(calendar.getTimeInMillis());
		List<MonthlyRevenueEntity> oldMonthlyRevenueList = monthlyRevenueRepository
				.findByAnnouncementTimeBetweenAndYearAndMonth(startDate, endDate, year, revenueMonthly);
		int total = 0;
		Set<MonthlyRevenueEntity> tmpEntity = new HashSet<MonthlyRevenueEntity>();
		for (int i = 1; i < tables.size(); i++) {
			WebElement table = tables.get(i);
			String industry = table.findElement(By.xpath(".//tbody/tr/th[contains(text(),'產業別')]")).getText()
					.replace("產業別：", "");
			WebElement newTable = table.findElement(By.xpath(".//tbody/tr/td/table/tbody"));
			List<WebElement> trs = newTable.findElements(By.xpath(".//tr[@align='right']"));
			Timestamp announcementTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
			int count = 0;
			for (int j = 0; j < trs.size() - 1; j++) {
				WebElement tr = trs.get(j);
				List<WebElement> tds = tr.findElements(By.xpath(".//td"));
				char firstChar = tds.get(0).getText().charAt(0);
				if (Character.isDigit(firstChar)) {
					String stockId = tds.get(0).getText();
					String stockName = tds.get(1).getText();
					Integer monthlyRevenue = stringToInt(tds.get(2).getText()); // 當月營收
					Integer preMonthlyRevenue = stringToInt(tds.get(3).getText()); // 上月營收
					Integer preYearMonthlyRevenue = stringToInt(tds.get(4).getText()); // 去年當月營收
					Float preMonthlyComparePrecentage = stringToFloat(tds.get(5).getText()); // 上月比較增減(%)
					Float preYearComparePrecentage = stringToFloat(tds.get(6).getText()); // 去年同月增減(%)
					Integer revenueGrandTotal = stringToInt(tds.get(7).getText()); // 當月累計營收
					Integer preYearRevenueGrandTotal = stringToInt(tds.get(8).getText()); // 去年累計營收
					Float preMonthlyCompareGrandTotalPrecentage = stringToFloat(tds.get(9).getText()); // 前期比較增減(%)
					String remark = tds.get(10).getText();// 備註
					Set<MonthlyRevenueEntity> oldMonthlyRevenue = oldMonthlyRevenueList.stream()
							.filter(e -> e.getStockId().equals(stockId)).collect(Collectors.toSet());
					if (oldMonthlyRevenue.size() <= 0) {
						MonthlyRevenueEntity entity = new MonthlyRevenueEntity(null, stockId, stockName, revenueMonthly,
								industry, announcementTime, monthlyRevenue, preMonthlyRevenue, preYearMonthlyRevenue,
								preMonthlyComparePrecentage, preYearComparePrecentage, revenueGrandTotal,
								preYearRevenueGrandTotal, preMonthlyCompareGrandTotalPrecentage, remark, year);
						monthlyRevenueRepository.save(entity);
						tmpEntity.add(entity);
						count++;
					}
				}
			}
			total += count;
			log.info("產業別:" + industry + " 新增:" + count);
		}
		log.info("總新增:" + total);
//		if (tmpEntity.size() > 0) {
//			List<MonthlyRevenueDto> monthlyRevenueDtoList = new ArrayList<MonthlyRevenueDto>();
//			// 查詢股價
//			Map<String, Map<String, String>> openDataPriceMap = stockService.getPriceFromOpenData();
//			for (MonthlyRevenueEntity e : tmpEntity) {
//				String stockId = e.getStockId();
//				try {
//					MonthlyRevenueDto dto = new MonthlyRevenueDto();
//					dto.setStockId(stockId);
//					dto.setStockName(stockId);
//					dto.setIndustry(e.getIndustry());
//					Map<String, String> openDataMap = openDataPriceMap.get(stockId);
//					if (openDataMap != null && openDataMap.containsKey("closePrice")) {
//						Integer volume = Integer.valueOf(openDataMap.get("volume")) / 1000;
//						dto.setClosePrice(Float.valueOf(openDataMap.get("closePrice")));
//						dto.setVolume(volume);
//					}
//					dto.setPreMonthlyCompareGrandTotalPrecentage(e.getPreMonthlyCompareGrandTotalPrecentage());
//					dto.setPreMonthlyComparePrecentage(e.getPreMonthlyComparePrecentage());
//					dto.setPreYearComparePrecentage(e.getPreYearComparePrecentage());
//					monthlyRevenueDtoList.add(dto);
//				} catch (Exception ex) {
//					log.info("stockId:" + stockId);
//					throw ex;
//				}
//			}
//			Mail mail = mailService.genMail("月營收報告 " + year + "年 " + revenueMonthly + "月");
//			Context context = new Context();
//			context.setVariable("templateName", "monthlyRevenue");
//			context.setVariable("year", year);
//			context.setVariable("monthlyRevenueList", monthlyRevenueDtoList);
//			mailService.sendMail(mail, context);
//		}
		log.info("執行月營收查詢結束");

		return new CommonResp();
	}

	// 現金股利
	@GetMapping("/getDividend")
	@Transactional
	@Synchronized
	private CommonResp<Map<String, Object>> getDividend(@RequestParam(value = "range") String range,
			@RequestParam(value = "year") String year) throws Exception {
		log.info("查詢" + year + "年現金股利開始 範圍:" + range);
		// 股利分派情形
		final String MOPS_URL = "https://mops.twse.com.tw/mops/web/t05st09_new";
		webDriver.get(MOPS_URL);
		WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='year']"))).sendKeys(year);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='search']/input[@value=' 查詢 ']")))
				.click();
		while (webDriver.getWindowHandles().size() < 2) {
			Thread.sleep(1000);
		}
		for (String winHandle : webDriver.getWindowHandles()) {
			webDriver.switchTo().window(winHandle);
		}
		Map<String, Map<String, String>> stockMap = stockService.getPriceFromOpenData();
		List<WebElement> trs = new ArrayList<>();
		DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		// 以今天日期搜尋
		if ("today".equals(range)) {
			DateFormat todaySDF = new SimpleDateFormat("MM/dd");
			Date now = Calendar.getInstance().getTime();
//			now.setDate(now.getDate() - 2);
			trs = webDriver
					.findElements(By.xpath("//td[contains(text(),'" + year + "/" + todaySDF.format(now) + "')]/.."));
		} else if ("year".equals(range)) {
			// 以今年年度搜尋
			trs = webDriver.findElements(By.xpath("//tr[@class='even' or @class='odd']"));
		}
		Calendar today = Calendar.getInstance();
		Timestamp now = new Timestamp(today.getTimeInMillis());
		List<String> stockIds = new ArrayList<String>();
		if (trs.size() > 0) {
			for (WebElement tr : trs) {
				List<WebElement> tds = tr.findElements(By.tagName("td"));
				String[] stock = tds.get(0).getText().split("-");
				String stockId = stock[0].trim();
				// 股利所屬年度(季)度
				String dividendYear = tds.get(2).getText().replace("\n", "").trim();
				DividendEntity entity = dividendRepository.findByStockIdAndYearAndDividendYear(stockId, year,
						dividendYear);
				if (entity != null) {
					continue;
				}
				String stockName = stock[1].trim();
				if ("測試帳號".equals(stockName))
					continue;
				Float price = 0f;
				// opendata csv沒資料或是收盤價是空值
				Map<String, String> openDataStockPriceMap = stockMap.get(stockId);
				// 透過csv取得收盤價
				if (stockMap.containsKey(stockId) && openDataStockPriceMap != null
						&& !"".equals(openDataStockPriceMap.get("closePrice"))) {
					price = Float.valueOf(openDataStockPriceMap.get("closePrice"));
				} else {
					// 沒有交易資料 可能該股被禁止交易
					continue;
				}
				// 盈餘分配之現金股利(元/股)
				Float cashDividend = Float.parseFloat(tds.get(11).getText());
				Float stockDividend = null;
				Float capitalDividend = null;
				Float increaseCashDividend;
				Float increaseStockDividend;
				Float increaseCapitalDividend;
				Long shareTotal;
				Long dividendTotal;
				if (Integer.parseInt(year) < 110) {
					// 法定盈餘公積發放之現金(元/股)
					stockDividend = Float.parseFloat(tds.get(12).getText());
					// 資本公積發放之現金(元/股)
					capitalDividend = 0f;
					dividendTotal = Long.parseLong(tds.get(13).getText().replace(",", ""));
					increaseCashDividend = Float.parseFloat(tds.get(14).getText());
					increaseStockDividend = Float.parseFloat(tds.get(15).getText());
					increaseCapitalDividend = 0f;
					shareTotal = Long.parseLong(tds.get(16).getText().replace(",", ""));
				} else {
					// 法定盈餘公積發放之現金(元/股)
					stockDividend = Float.parseFloat(tds.get(12).getText());
					// 資本公積發放之現金(元/股)
					capitalDividend = Float.parseFloat(tds.get(13).getText());
					dividendTotal = Long.parseLong(tds.get(14).getText().replace(",", ""));
					increaseCashDividend = Float.parseFloat(tds.get(15).getText());
					increaseStockDividend = Float.parseFloat(tds.get(16).getText());
					increaseCapitalDividend = Float.parseFloat(tds.get(17).getText());
					shareTotal = Long.parseLong(tds.get(18).getText().replace(",", ""));
				}
				Float totalDividend = cashDividend + stockDividend + capitalDividend;
				Float totalYieldRate = StockUtil.decimal((totalDividend / price) * 100);
//				String announcementDateStr = tds.get(5).getText();
//				Date announcementDate = null;
//				if (announcementDateStr.length() > 1) {
//					announcementDate = sdf.parse(strYear + tds.get(5).getText().substring(3).trim());
//				} else {
//					announcementDate = sdf.parse(strYear + "/01/01");
//				}
				stockIds.add(stockId);
				dividendRepository.save(new DividendEntity(null, stockId, stockName, year, cashDividend, stockDividend,
						capitalDividend, dividendTotal, increaseCashDividend, increaseStockDividend,
						increaseCapitalDividend, shareTotal, price, totalYieldRate, now, dividendYear));
			}
			log.info("現金股利新增" + stockIds.size() + "筆資料");
		}
		webDriver.close();
		for (String winHandle : webDriver.getWindowHandles()) {
			webDriver.switchTo().window(winHandle);
		}
		log.info("查詢現金股利結束");
		return new CommonResp();
	}

	private Integer stringToInt(String s) {
		return Integer.valueOf(s.replaceAll(",", ""));
	}

	private Float stringToFloat(String s) {
		if (s.trim().isEmpty()) {
			return 0f;
		}
		return Float.valueOf(s.replaceAll(",", ""));
	}
}
