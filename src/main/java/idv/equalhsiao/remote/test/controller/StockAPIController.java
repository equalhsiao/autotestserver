package idv.equalhsiao.remote.test.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import idv.equalhsiao.remote.test.config.CommonResp;
import idv.equalhsiao.remote.test.dto.DividendDto;
import idv.equalhsiao.remote.test.dto.IncomeStatementDto;
import idv.equalhsiao.remote.test.dto.MonthlyRevenueDto;
import idv.equalhsiao.remote.test.po.DividendEntity;
import idv.equalhsiao.remote.test.po.DividendRepository;
import idv.equalhsiao.remote.test.po.IncomeStatementRepository;
import idv.equalhsiao.remote.test.po.MonthlyRevenueEntity;
import idv.equalhsiao.remote.test.po.MonthlyRevenueRepository;
import idv.equalhsiao.remote.test.po.StockIndustryPriceEntity;
import idv.equalhsiao.remote.test.po.StockIndustryPriceRepository;
import idv.equalhsiao.remote.test.service.StockService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "stock")
public class StockAPIController {

	@Autowired
	private StockIndustryPriceRepository stockIndustryPriceRepository;
	@Autowired
	private MonthlyRevenueRepository monthlyRevenueRepository;
	@Autowired
	private DividendRepository dividendRepository;
	@Autowired
	private IncomeStatementRepository incomeStatementRepository;
	@Autowired
	private StockService stockService;

	// 查詢行業帳跌
	@GetMapping("/stockIndustryPrice")
	private CommonResp<Map<String, List<StockIndustryPriceEntity>>> stockIndustryPrice(
			@RequestParam(value = "industry") String industry,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date announcementDateStart,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date announcementDateEnd) {
		List<StockIndustryPriceEntity> list = stockIndustryPriceRepository.findBySubIndustryContainingAndUseDateBetween(industry,
				announcementDateStart, announcementDateEnd);
		CommonResp<Map<String, List<StockIndustryPriceEntity>>> resp = new CommonResp<Map<String, List<StockIndustryPriceEntity>>>();
		Map<String, List<StockIndustryPriceEntity>> map = new HashMap<String, List<StockIndustryPriceEntity>>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		for (StockIndustryPriceEntity e : list) {
			String useDate = sdf.format(e.getUseDate());
			List<StockIndustryPriceEntity> tmpList = map.getOrDefault(useDate,
					new ArrayList<StockIndustryPriceEntity>());
			tmpList.add(e);
			map.put(useDate, tmpList);
		}
		resp.setData(map);
		return resp;
	}

	// 損益表 for UI
	@GetMapping("/getSeasonReport")
	private CommonResp<List<IncomeStatementDto>> getSeasonReport(@RequestParam(value = "year") String year,
			@RequestParam(value = "season") Short season, @RequestParam(value = "industry") String industry,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date announcementDateStart,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date announcementDateEnd) throws Exception {
		List<String> stockIds = null;
		if ("ALL".equals(industry)) {
			if (announcementDateStart == null || announcementDateEnd == null) {
				stockIds = incomeStatementRepository.findStockIdByYearAndSeason(year, season);
			} else {
				stockIds = incomeStatementRepository.findStockIdByYearAndSeasonAndAnnouncementDate(year, season,
						announcementDateStart, announcementDateEnd);
			}
		} else {
			if (announcementDateStart == null || announcementDateEnd == null) {
				stockIds = incomeStatementRepository.findStockIdByYearAndSeasonAndIndustry(year, season, industry);
			} else {
				stockIds = incomeStatementRepository.findStockIdByYearAndSeasonAndIndustryAndAnnouncementDate(year,
						season, industry, announcementDateStart, announcementDateEnd);
			}
		}
		Map<String, List<IncomeStatementDto>> incomeStatementMap = stockService.incomeStatement(stockIds, year, season);
		List<IncomeStatementDto> respList = new ArrayList<IncomeStatementDto>();
		for (String key : incomeStatementMap.keySet()) {
			List<IncomeStatementDto> list = incomeStatementMap.get(key);
			respList.addAll(list);
		}
		CommonResp<List<IncomeStatementDto>> resp = new CommonResp<List<IncomeStatementDto>>();
		resp.setData(respList);
		return resp;
	}

	// 營收查詢 for UI
	@GetMapping("/monthlyRevenue")
	private CommonResp<List<MonthlyRevenueDto>> monthlyRevenue(@RequestParam(value = "year") String year,
			@RequestParam(value = "month") Integer month, @RequestParam(value = "industry") String industry,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date announcementDateStart,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date announcementDateEnd) throws Exception {
		List<MonthlyRevenueEntity> list = null;
		if ("ALL".equals(industry)) {
			list = monthlyRevenueRepository.findByAnnouncementTimeBetweenAndYearAndMonth(announcementDateStart,
					announcementDateEnd, year, month);
		} else {
			list = monthlyRevenueRepository.findByAnnouncementTimeBetweenAndIndustryAndYearAndMonth(announcementDateEnd,
					announcementDateEnd, industry, year, month);
		}
		List<MonthlyRevenueDto> respList = new ArrayList<MonthlyRevenueDto>();
		// 查詢股價
		Map<String, Map<String, String>> openDataPriceMap = list.size() > 0 ? stockService.getPriceFromOpenData()
				: null;
		for (MonthlyRevenueEntity e : list) {
			String stockId = e.getStockId();
			try {
				MonthlyRevenueDto dto = new MonthlyRevenueDto();
				dto.setStockId(stockId);
				dto.setIndustry(e.getIndustry());
				dto.setPreMonthlyCompareGrandTotalPrecentage(e.getPreMonthlyCompareGrandTotalPrecentage());
				dto.setPreMonthlyComparePrecentage(e.getPreMonthlyComparePrecentage());
				dto.setPreYearComparePrecentage(e.getPreYearComparePrecentage());
				dto.setStockName(industry);
				Map<String, String> openDataMap = openDataPriceMap.get(stockId);
				if (openDataMap != null && openDataMap.containsKey("closePrice")
						&& !StringUtils.isEmpty(openDataMap.get("closePrice"))) {
					dto.setVolume((Integer.valueOf(openDataMap.get("volume")) / 1000));
					Float closePrice = Float.valueOf(openDataMap.get("closePrice"));
					dto.setClosePrice(closePrice);
				}
				respList.add(dto);
			} catch (Exception ex) {
				log.info("stockId:" + stockId);
				throw ex;

			}
		}
		CommonResp<List<MonthlyRevenueDto>> resp = new CommonResp<List<MonthlyRevenueDto>>();
		resp.setData(respList);
		return resp;
	}

	// 現金股利查詢 for UI
	@GetMapping("/dividend")
	private CommonResp<List<DividendDto>> dividend(@RequestParam(value = "year") String year,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date announcementDateStart,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date announcementDateEnd) throws Exception {
		List<DividendDto> respList = new ArrayList<DividendDto>();
		List<DividendEntity> list = dividendRepository.findByAnnouncementDateBetweenAndYear(announcementDateStart,
				announcementDateEnd, year);
		// 查詢股價
		Map<String, Map<String, String>> openDataPriceMap = list.size() > 0 ? stockService.getPriceFromOpenData()
				: null;

		for (DividendEntity e : list) {
			DividendDto dto = new DividendDto();
			String stockId = e.getStockId();
			Map<String, String> openDataMap = openDataPriceMap.get(stockId);
			Float totalCashDividend = e.getCashDividend() + e.getCapitalDividend() + e.getStockDividend();
			Float totalStockDividend = e.getIncreaseCashDividend() + e.getIncreaseCapitalDividend()
					+ e.getIncreaseStockDividend();
			Float totalDividend = totalCashDividend + totalStockDividend;
			dto.setYear(e.getYear());
			dto.setStockId(e.getStockId());
			dto.setStockName(e.getStockName());
			dto.setAnnouncementDate(e.getAnnouncementDate());
			if (openDataMap != null && openDataMap.containsKey("closePrice")) {
				dto.setVolume((Integer.valueOf(openDataMap.get("volume")) / 1000));
				Float closePrice = Float.valueOf(openDataMap.get("closePrice"));
				dto.setClosePrice(closePrice);
				dto.setTotalYieldRate((totalDividend / closePrice) * 100f);
			}
			dto.setDividendYear(e.getDividendYear());
			dto.setTotalCashDividend(totalCashDividend);
			dto.setTotalStockDividend(totalStockDividend);
			dto.setTotalDividend(totalDividend);
			respList.add(dto);
		}
		CommonResp<List<DividendDto>> resp = new CommonResp<List<DividendDto>>();
		resp.setData(respList);
		return resp;
	}

}
