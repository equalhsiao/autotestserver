package idv.equalhsiao.remote.test.controller;


import idv.equalhsiao.remote.test.common.Task;
import idv.equalhsiao.remote.test.common.TaskRunner;
import idv.equalhsiao.remote.test.config.ACCProperties;
import idv.equalhsiao.remote.test.config.AccNotificationProperties;
import idv.equalhsiao.remote.test.config.CommonResp;
import idv.equalhsiao.remote.test.config.SeleniumProperties;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "pettyCash")
@Slf4j
public class ACCController extends TaskRunner {

  private final ACCProperties aCCProperties;
  private final AccNotificationProperties accNotificationProperties;
  private final SeleniumProperties seleniumProperties;
  private WebDriver webDriver;
  @Value("${page.loading.second}")
  private int PAGE_LOADING_SECOND;

  @Value("${spring.resources.static-locations}")
  private String staticLocations;

  @Autowired
  private ResourceLoader resourceLoader;

  @Autowired
  private ResourcePatternResolver resourcePatternResolver;

//  @Autowired
//  private PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver;

  @Autowired
  public ACCController(ACCProperties aCCProperties,
      AccNotificationProperties accNotificationProperties,
      SeleniumProperties seleniumProperties,
      WebDriver webDriver
  ) {
    this.accNotificationProperties = accNotificationProperties;
    this.aCCProperties = aCCProperties;
    log.info("Target url:" + aCCProperties.getDomain());
    this.seleniumProperties = seleniumProperties;
    this.webDriver = webDriver;
  }


  /**
   * 切換頁面到零用金
   *
   * @param wait
   * @throws Exception
   */
  private void switchToPettyCash(WebDriverWait wait) throws Exception {
    final String INDEX_URL = aCCProperties.getDomain();
    final String PETTYCASH_URL = aCCProperties.getDomain() + "/pettyCash";
    webDriver.get(PETTYCASH_URL);
    //若重導到首頁則登入
    if (INDEX_URL.equals(webDriver.getCurrentUrl())) {
      //登入成功切換到測試頁面
      if (!login(webDriver, wait)) {
        throw new Exception("登入失敗");
      }
    }
  }

  /**
   * 登入
   *
   * @param webDriver
   * @param wait
   * @return
   */
  private boolean login(WebDriver webDriver, WebDriverWait wait) {
    try {
      //登入
      WebElement account = wait
          .until(ExpectedConditions.visibilityOfElementLocated(By.id("Account")));
      account.sendKeys(aCCProperties.getAccount());
      WebElement password = wait
          .until(ExpectedConditions.visibilityOfElementLocated(By.id("Password")));
      password.sendKeys(aCCProperties.getPassword());
      WebElement signInBtn = findElementBySearchText(webDriver, By.tagName("button"), "登 入");
      signInBtn.submit();
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("main")));
      return true;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }


  /**
   * 取得最新一筆record的jwsNo
   *
   * @param wait
   * @return
   * @throws Exception
   */
  private String findLastJwsNo(WebDriverWait wait) throws Exception {
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("main")));
    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("ant-table-body")));
    List<WebElement> journalWorkSheetList = wait.until(ExpectedConditions
        .visibilityOfAllElementsLocatedBy(
            By.xpath("//div[@class='ant-table-content']//div[@class='ant-table-body']//tr")));
    if (journalWorkSheetList.size() <= 0) {
      throw new Exception("測試失敗 未取得傳票工作底稿單號");
    }
//    String jwsNo = journalWorkSheetList.get(0).getAttribute("data-row-key");
    String jwsNo = journalWorkSheetList.get(0).findElement(By.xpath("//td[contains(text(),'JWS')]"))
        .getText();
    if (StringUtils.isEmpty(jwsNo)) {
      throw new Exception("測試失敗 未取得傳票工作底稿單號");
    }
    return jwsNo;
  }

  /**
   * 檢查ant message中的訊息是否成功
   *
   * @param wait
   * @param checkText
   * @return
   */
  private String messageErrorHandle(WebDriverWait wait, String... checkText) {
    Integer checkMillisecond = accNotificationProperties.getCheckMillisecond();
    Integer finalCheckCount = accNotificationProperties.getCheckCount();
    Integer finalRetryCount = accNotificationProperties.getCheckRetryCount();
    int checkRetryCount = 0;
    int checkCount = 0;

    String text = null;
    while (checkCount < finalCheckCount) {
      try {
        WebElement test = wait.until(
            ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='ant-message']")));
        text = test.getText();
        if (!StringUtils.isEmpty(text)) {
          log.info("text:" + text);
        }
        for (String s : checkText) {
          //如果不為空值且為檢查的字串則回傳空字串
          if (!"".equals(text.trim()) && s.equals(text.trim())) {
            return "";
          }
        }
        Thread.sleep(checkMillisecond);
        checkCount++;
      } catch (Exception exception) {
        //重試
        checkRetryCount++;
        if (checkRetryCount < finalRetryCount) {
          checkCount--;
        } else {
          //找不到表示成功
          return "";
        }
      }
    }
    //不為檢查字串則返回錯誤訊息
    return text;
  }

  /**
   * 亂數產生invoiceNo
   *
   * @return
   */
  private String genInvoiceNo() {
    Random r = new Random();
    String invoiceNo = "";
    while (invoiceNo.length() < 2) {
      char upCase = (char) (r.nextInt(26) + 65);//得到 65 - 90 的隨機數
      invoiceNo += upCase;
    }
    while (invoiceNo.length() < 10) {
      int no = r.nextInt(9);
      invoiceNo += no;
    }
    return invoiceNo;
  }

  /**
   * 收入外銷憑證設定
   *
   * @param wait
   * @param exportMode
   * @param declarationNo
   * @param uniformInvoiceNo
   * @param amount
   * @param isDeduct
   */
  private void setExportTransaction(WebDriverWait wait, String exportMode, String declarationNo,
      String uniformInvoiceNo, String amount, String isDeduct) {

    //外銷方式
    wait.until(ExpectedConditions.elementToBeClickable(By.id("exportMode"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(
        By.xpath("//ul[@role='listbox']/li[contains(text(),'" + exportMode + "')]")))
        .click();

    //報單號碼
    if (StringUtils.isEmpty(declarationNo)) {
      wait.until(ExpectedConditions.elementToBeClickable(By.id("declarationNo")))
          .sendKeys(genInvoiceNo());
    } else {
      wait.until(ExpectedConditions.elementToBeClickable(By.id("declarationNo")))
          .sendKeys(declarationNo);
    }
    //買受人
    wait.until(ExpectedConditions.elementToBeClickable(By.id("buyer")))
        .sendKeys(uniformInvoiceNo);

    //交易金額
    wait.until(ExpectedConditions.elementToBeClickable(By.id("totalAmount")))
        .sendKeys(amount);

    //是否申報營業稅
    wait.until(ExpectedConditions.elementToBeClickable(By.id("isDeduct"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(
        By.xpath("//ul[@role='listbox']/li[contains(text(),'" + isDeduct + "')]"))).click();

  }

  /**
   * 支出與收入台幣發票設定
   *
   * @param wait
   * @param cashAccount
   * @param isDeduct
   * @param taxDataType
   * @param taxChargeType
   * @param amount
   * @param uniformInvoiceNo
   */
  private void setNTDInvoiceTransaction
  (WebDriverWait wait, String cashAccount, String isDeduct,
      String taxDataType, String taxChargeType, String amount, String uniformInvoiceNo) {

    //是否可扣抵
    selectIsDeduct(wait, isDeduct);

    //課稅類別

    //發票稅別類型
    wait.until(ExpectedConditions.elementToBeClickable(By.id("taxChargeType"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(
        By.xpath("//ul[@role='listbox']/li[contains(text(),'" + taxChargeType + "')]")))
        .click();

    //台幣才執行
    if (cashAccount.indexOf("NTD") >= 0) {
      if ("應稅".equals(taxChargeType)) {
        //稅別類型
        wait.until(ExpectedConditions.elementToBeClickable(By.id("taxDataType"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//ul[@role='listbox']/li[contains(text(),'" + taxDataType + "')]")))
            .click();
        if ("混稅".equals(taxDataType)) {
          //未稅金額
          wait.until(ExpectedConditions.elementToBeClickable(By.id("taxAbleAmount")))
              .sendKeys(amount);
          //混稅: 零稅金額, 免稅金額其一須不為0
          wait.until(ExpectedConditions.elementToBeClickable(By.id("zeroTaxAmount")))
              .sendKeys("50");
        } else if ("單一稅別".equals(taxDataType)) {
          //未稅金額
          wait.until(ExpectedConditions.elementToBeClickable(By.id("dutyFreeAmount")))
              .sendKeys(amount);
        }
      } else if ("零稅率".equals(taxChargeType) || "免稅".equals(taxChargeType) || "土地"
          .equals(taxChargeType)) {
        //總金額
        wait.until(ExpectedConditions.elementToBeClickable(By.id("totalAmount")))
            .sendKeys(amount);
      }
    }

    //銷售人統編
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("uniformInvoiceNo")))
        .sendKeys(uniformInvoiceNo);
  }


  /**
   * 取得分錄資訊
   *
   * @param wait
   * @return
   */
  private List<String> getEntryList(WebDriverWait wait) {
    List<String> entryList = new ArrayList<>();
    WebElement entryRow = wait.until(ExpectedConditions.visibilityOfElementLocated(
        By.xpath(
            "//div[contains(text(),'借方')]/../../div[@class='u-mt-10 u-mb-10 f-w-200']/div[@class='ant-row']")));
    List<WebElement> entryRowElements = entryRow.findElements(By.className("ant-col"));
    boolean accountIdCheck = false;
    boolean accountAmountCheck = false;
    String fullEntry = new String();
    for (int i = 0; i < entryRowElements.size(); i++) {
      WebElement entryElement = entryRowElements.get(i);
      String entryClass = entryElement.getAttribute("class");
      //科目
      if (entryClass.indexOf("ant-col-9") >= 0) {
        accountIdCheck = true;
        fullEntry = fullEntry + " " + entryElement.getText();
      }
      //金額
      if (entryClass.indexOf("ant-col-3") >= 0) {
        accountAmountCheck = true;
        fullEntry = fullEntry + " " + entryElement.getText();
      }
      //有科目與金額時加入list
      if (accountIdCheck && accountAmountCheck) {
        entryList.add(fullEntry);
        //reset
        accountIdCheck = false;
        accountAmountCheck = false;
        fullEntry = new String();
      }
    }
    return entryList;
  }


  /**
   * 找尋前端notification錯誤訊息
   *
   * @param wait
   * @return
   * @throws Exception
   */
  private String notificationErrorHandle(WebDriverWait wait) throws Exception {
    try {
      WebElement notificationElement = null;
      try {
        notificationElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//div[@class='ant-notification-notice-description']")));
      }
      //找不到該元素表示沒有錯誤訊息
      catch (TimeoutException timeoutException) {
        return "";
      }
      //找到錯誤訊息表示新增失敗
      if (notificationElement.isDisplayed()) {
        return notificationElement.getText();
      }
    } catch (Exception exception) {
      return exception.getMessage();
    }
    return "";
  }


  /**
   * 點擊目錄選擇零用金
   *
   * @param wait
   */
  private void clickFunction(WebDriverWait wait) {
    wait.until(ExpectedConditions.elementToBeClickable(By.className("anticon-dollar")))
        .click();
    wait.until(ExpectedConditions.elementToBeClickable(By.linkText("零用金"))).click();
  }

  /**
   * 選擇零用金
   *
   * @param wait
   * @param cashAccount
   */
  private void selectCashAccount(WebDriverWait wait, String cashAccount) {
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("main")));
    wait.until(ExpectedConditions.elementToBeClickable(By.id("cashAccountId"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(
        By.xpath("//ul[@role='listbox']/li[contains(text(),'" + cashAccount + "')]"))).click();
  }

  /**
   * 選擇營運主體
   *
   * @param wait
   * @param entityId
   */
  private void selectEntity(WebDriverWait wait, String entityId) {
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("main")));
    wait.until(ExpectedConditions.elementToBeClickable(By.id("entityId"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(
        By.xpath("//ul[@role='listbox']/li[contains(text(),'" + entityId + "')]"))).click();
  }

  /**
   * 點擊麵包屑頁籤
   *
   * @param wait
   * @param function
   */
  private void switchTab(WebDriverWait wait, String function) {
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("main")));
    wait.until(ExpectedConditions.visibilityOf(wait.until(ExpectedConditions
        .elementToBeClickable(By.xpath("//div[@role='tab' and text()='" + function + "']")))))
        .click();
  }

  /**
   * 選擇交易類型
   *
   * @param wait
   * @param transactionTypeId
   */
  private void selectTransactionType(WebDriverWait wait, String transactionTypeId) {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("tapTransactionTypeId"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(
        By.xpath("//ul[@role='listbox']/li[contains(text(),'" + transactionTypeId + "')]")))
        .click();
  }

  /**
   * 檢查是否有錯誤 錯誤則返回錯誤訊息
   *
   * @param wait
   * @param response
   * @throws Exception
   */
  private void checkTransactonErrorMessage(WebDriverWait wait,
      CommonResp<Map<String, Object>> response) throws Exception {
    String errorMessage = messageErrorHandle(wait, "新增憑證中", "新增憑證成功");
    if (wait.until(
        ExpectedConditions
            .invisibilityOfElementLocated(By.xpath("//div[@class='ant-message']")))
        &&
        wait.until(
            ExpectedConditions
                .invisibilityOfElementLocated(By.xpath("//div[@class='ant-notification']")))
    ) {
      if (!"".equals(errorMessage)) {
        response.setRm(errorMessage);
      }
    } else {
      throw new Exception("新增憑證失敗");
    }
  }

  /**
   * 選擇交易類型
   *
   * @param wait
   * @param tradingType
   */
  private void selectTradingType(WebDriverWait wait, String tradingType) {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("tradingType"))).click();
    //用elementToBeClickable會有bug
    wait.until(ExpectedConditions.visibilityOfElementLocated(
        By.xpath("//ul[@role='listbox']/li[text()='" + tradingType + "']"))).click();
  }

  /**
   * 選擇交易名稱
   *
   * @param wait
   * @param tradingName
   */
  private void selectTradingName(WebDriverWait wait, String tradingName) {
    List<WebElement> selects = webDriver.findElements(By.className("ant-form-item-no-colon"));
    for (WebElement select : selects) {
      if ("名稱".equals(select.getText())) {
        select.findElement(By.xpath("./../.."))
            .findElement(By.className("ant-form-item-control-wrapper"))
            .findElement(By.className("ant-select-selection")).click();
        break;
      }
    }
    wait.until(ExpectedConditions.elementToBeClickable(
        By.xpath("//ul[@role='listbox']/li[contains(text(),'" + tradingName + "')]"))).click();
  }

  /**
   * 選擇是否可扣抵
   *
   * @param wait
   * @param isDeduct
   */
  private void selectIsDeduct(WebDriverWait wait, String isDeduct) {
    //是否可扣抵
    WebElement isDeductElement = wait
        .until(ExpectedConditions.elementToBeClickable(By.id("isDeduct")));
    isDeductElement.click();
//
    List<WebElement> list = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
        By.xpath("//ul[@role='listbox']/li[text()='" + isDeduct + "']")));
    for (WebElement l : list) {
      if (l.isDisplayed()) {
        l.click();
      }
    }
  }

  /**
   * 選擇是否固定資產
   *
   * @param wait
   * @param isFixedAsset
   */
  private void selectIsFixedAsset(WebDriverWait wait, String isFixedAsset) {
    //是否固定資產
    WebElement isDeductElement = wait
        .until(ExpectedConditions.elementToBeClickable(By.id("isFixedAsset")));
    isDeductElement.click();
    List<WebElement> list = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
        By.xpath("//ul[@role='listbox']/li[text()='" + isFixedAsset + "']")));
    for (WebElement l : list) {
      if (l.isDisplayed()) {
        l.click();
      }
    }
  }

  /**
   * 產生主旨/備註
   *
   * @param cateGory
   * @param transactionTypeId
   * @param tradingType
   * @param tradingName
   * @param certificateType
   * @param taxChargeType
   * @param uniformInvoiceNo
   * @param isDeduct
   * @param amount
   * @return
   */
  private String genRemark(String cateGory, String transactionTypeId, String tradingType,
      String tradingName, String certificateType, String taxChargeType, String uniformInvoiceNo,
      String isDeduct, String amount) {
    return "支出類型:" + cateGory + ",交易類型" + transactionTypeId + ",交易對象:" + tradingType + ",對象名稱:"
        + tradingName + ",憑證類型:" + certificateType + "課稅類別:" + taxChargeType + ",統一編號:"
        + uniformInvoiceNo + ",是否申報營業稅:" + isDeduct + ",交易金額:" + amount;
  }

  /**
   * 設定錯誤訊息到返回資料中
   *
   * @param wait
   * @param responseData
   * @param checkText
   * @throws Exception
   */
  private void setErrorMessage(WebDriverWait wait, Map<String, Object> responseData,
      String... checkText)
      throws Exception {
    //message錯誤處理
    String messageError = messageErrorHandle(wait, checkText);
    if (!StringUtils.isEmpty(messageError)) {
      responseData.put("messageError", messageError);
    }
    //notification錯誤處理
    String notificationError = notificationErrorHandle(wait);
    if (!StringUtils.isEmpty(notificationError)) {
      responseData.put("notificationError", notificationError);
    }
    //瀏覽器錯誤
    List<LogEntry> logs = webDriver.manage().logs().get(LogType.BROWSER).getAll();
    //本機環境會有警告 但Level為SERVER的警告 所以本機略過
    if (logs.size() > 0) {
      responseData
          .put("logError", logs.stream().filter(
              log -> "SEVERE".equals(log.getLevel().getName()) && !log.getMessage()
                  .startsWith("Warning"))
              .collect(Collectors.toList()));
    }
  }

  private boolean containsKey(Map<String, Object> responseData, String key) {
    if (responseData.containsKey(key)) {
      log.info(key + ":" + responseData.get(key));
      return true;
    }
    return false;
  }

  /**
   * 有任何錯誤則印出後拋出錯誤訊息
   *
   * @param responseData
   * @param errorMessage
   * @throws Exception
   */
  private void checkError(Map<String, Object> responseData, String errorMessage) throws Exception {
    boolean logError = false;
    if (responseData.containsKey("logError")) {
      logError = true;
      ((List<LogEntry>) responseData.get("logError")).stream().forEach(
          e -> {
            log.info("logError:" + e.getMessage());
          }
      );
    }
    if (containsKey(responseData, "messageError") || containsKey(responseData,
        "notificationError") || logError) {
      throw new Exception(errorMessage);
    }
  }

  @GetMapping("/report")
  private String test() throws IOException {

    String filePath = staticLocations + "*.html";
    Resource[] resources = resourcePatternResolver.getResources(filePath);
    BufferedReader in = new BufferedReader(
        new InputStreamReader(resources[resources.length - 1].getInputStream(), Charset
            .forName("UTF-8")));
    String inputLine;
    StringBuilder stringBuilder = new StringBuilder();
    while ((inputLine = in.readLine()) != null) {
      stringBuilder.append(inputLine);
    }
    in.close();
    return stringBuilder.toString();
  }

  @GetMapping("/")
  private CommonResp<Map<String, Object>> init() {

    return new CommonResp<Map<String, Object>>();
  }

  @GetMapping("/receivable/create")
  private CommonResp<Map<String, Object>> receivableCreate(
      @RequestParam(value = "entity") String entityId,
      @RequestParam(value = "cashAccount") String cashAccount,
      @RequestParam(value = "cateGory") String cateGory,
      @RequestParam(value = "transactionTypeId") String transactionTypeId,
      @RequestParam(value = "tradingType") String tradingType,
      @RequestParam(value = "tradingName") String tradingName,
      @RequestParam(value = "certificateType") String certificateType,
      @RequestParam(value = "uniformInvoiceNo") String uniformInvoiceNo,
      @RequestParam(value = "isDeduct", required = false, defaultValue = "否") String isDeduct,
      @RequestParam(value = "amount", required = false, defaultValue = "1000") String amount,
      @RequestParam(value = "taxChargeType", required = false, defaultValue = "應稅") String taxChargeType,
      @RequestParam(value = "taxDataType", required = false, defaultValue = "單一稅別") String taxDataType,
      @RequestParam(value = "isFixedAsset", required = false, defaultValue = "否") String isFixedAsset,
      @RequestParam(value = "invoiceBook") String invoiceBook,
      @RequestParam(value = "certificateNo", required = false) String certificateNo,
      @RequestParam(value = "exportMode", required = false) String exportMode,
      @RequestParam(value = "declarationType", required = false) String declarationType,
      @RequestParam(value = "certifiedDocument", required = false) String certifiedDocument,
      @RequestParam(value = "declarationNo", required = false) String declarationNo,
      @RequestParam(value = "keep", required = false, defaultValue = "true") boolean keep,
      HttpServletRequest request
  ) throws Exception {
    WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
    TaskRunner taskRunner = new TaskRunner(webDriver, seleniumProperties);

    taskRunner.addTask(new Task("切換到零用金") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        switchToPettyCash(wait);
      }
    });

    taskRunner.addTask(new Task("選擇營運主體") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectEntity(wait, entityId);
      }
    });

    taskRunner.addTask(new Task("切換功能") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        clickFunction(wait);
      }
    });

    taskRunner.addTask(new Task("檢查頁面是否正常切換") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        final String PETTYCASH_URL = aCCProperties.getDomain() + "pettyCash";
        if (!PETTYCASH_URL
            .equals(webDriver.getCurrentUrl())) {
          clickFunction(wait);
        }
      }
    });

    taskRunner.addTask(new Task("選擇現金科目") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        selectCashAccount(wait, cashAccount);
      }
    });

    taskRunner.addTask(new Task("切換功能頁籤") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        switchTab(wait, "收入");
      }
    });

    taskRunner.addTask(new Task("點擊新增") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        //span 不能點擊 但需要等新增這個按鈕初始化完成
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//button[@type='button']/span[contains(text(),'新 增')]")));
        List<WebElement> buttons = webDriver.findElements(By.tagName("button"));
        for (WebElement button : buttons) {
          if ("新 增".equals(button.getText())) {
            button.click();
            break;
          }
        }
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.className("ant-modal-content")));
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.className("ant-modal-body")));
      }
    });

    taskRunner.addTask(new Task("選擇收入類型") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
        checkWait.until(ExpectedConditions.elementToBeClickable(By.id("transactionCategory")))
            .click();
        checkWait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//ul[@role='listbox']/li[contains(text(),'" + cateGory + "')]")))
            .click();
      }
    });

    taskRunner.addTask(new Task("選擇交易類型") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        selectTransactionType(wait, transactionTypeId);
      }
    });
    taskRunner.addTask(new Task("點擊新增憑證") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        //span 不能點擊 但需要等新增這個按鈕初始化完成
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//button[@type='button']/span[contains(text(),'新增憑證')]")));
        findElementBySearchText(webDriver, By.tagName("button"), "新增憑證").click();
      }
    });

    taskRunner.addTask(new Task("輸入憑證資料") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        //憑證類型
        // 收入類型為一般時 憑證類型發票disabled不可選擇
        if (!"一般".equals(cateGory)) {
          wait.until(ExpectedConditions.elementToBeClickable(By.id("certificateType")))
              .click();
          wait.until(ExpectedConditions.elementToBeClickable(
              By.xpath(
                  "//ul[@role='listbox']/li[contains(text(),'" + certificateType + "')]")))
              .click();
        }

        //發票本選擇
        //收入憑證類型若為Invoice則不可選擇發票本與發票
        if (!"Invoice".equals(certificateType)) {
          wait.until(ExpectedConditions.elementToBeClickable(By.id("invoiceBook"))).click();
          wait.until(ExpectedConditions.elementToBeClickable(
              By.xpath("//ul[@role='listbox']/li[contains(text(),'" + invoiceBook + "')]")))
              .click();
          //發票選擇 若無則取第一筆
          wait.until(ExpectedConditions.elementToBeClickable(By.id("certificateNo"))).click();
          if (StringUtils.isEmpty(certificateNo)) {
            List<WebElement> list = webDriver.findElements(
                By.xpath(
                    "//ul[@role='listbox']/li[contains(text(),'" + invoiceBook.substring(0, 2)
                        + "')]")
            );
            wait.until(ExpectedConditions.elementToBeClickable(
                list.stream().filter(e -> !StringUtils.isEmpty(e.getText()))
                    .collect(Collectors.toList())
                    .get(0))).click();
          } else {
            wait.until(ExpectedConditions.elementToBeClickable(
                By.xpath(
                    "//ul[@role='listbox']/li[contains(text(),'" + certificateNo + "')]")))
                .click();
          }
        }
        if ("發票".equals(certificateType)) {
          //是否可扣抵

          //台幣
          if (cashAccount.indexOf("NTD") >= 0) {
            setNTDInvoiceTransaction(wait, cashAccount, isDeduct, taxDataType, taxChargeType,
                amount, uniformInvoiceNo);
          } else {
            //課稅類別
            wait.until(ExpectedConditions.elementToBeClickable(By.id("taxChargeType")))
                .click();
            wait.until(ExpectedConditions.elementToBeClickable(
                By.xpath(
                    "//ul[@role='listbox']/li[contains(text(),'" + taxChargeType + "')]")))
                .click();

            //是否申報營業稅
            selectIsDeduct(wait, isDeduct);

            //銷售人統編
            wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("uniformInvoiceNo")))
                .sendKeys(uniformInvoiceNo);
            if ("應稅".equals(taxChargeType)) {
              //外幣未稅金額
              wait.until(ExpectedConditions.elementToBeClickable(By.id("taxAbleAmount")))
                  .sendKeys(amount);
            } else {
              //免稅 零稅率 土地
              //總金額
              wait.until(ExpectedConditions.elementToBeClickable(By.id("totalAmount")))
                  .sendKeys(amount);
            }

          }
        } else if ("出口報單-經海關".equals(certificateType)) {
          setExportTransaction(wait, exportMode, declarationNo, uniformInvoiceNo, amount,
              isDeduct);
          //報單類別
          wait.until(ExpectedConditions.elementToBeClickable(By.id("declarationType")))
              .click();
          wait.until(ExpectedConditions.elementToBeClickable(
              By.xpath(
                  "//ul[@role='listbox']/li[contains(text(),'" + declarationType + "')]")))
              .click();
        } else if ("出口報單-非經海關".equals(certificateType)) {
          setExportTransaction(wait, exportMode, declarationNo, uniformInvoiceNo, amount,
              isDeduct);
          //證明文件
          wait.until(ExpectedConditions.elementToBeClickable(By.id("certifiedDocument")))
              .click();
          wait.until(ExpectedConditions.elementToBeClickable(
              By.xpath(
                  "//ul[@role='listbox']/li[contains(text(),'" + certifiedDocument + "')]")))
              .click();
          wait.until(ExpectedConditions.elementToBeClickable(By.id("totalAmount")))
              .sendKeys(amount);
        } else if ("Invoice".equals(certificateType)) {
          wait.until(ExpectedConditions.elementToBeClickable(By.id("totalAmount")))
              .sendKeys(amount);
        }
      }
    });

    // TODO bug 取得button後無法點擊
//        WebElement insertTransactionButtonParent =
//            wait.until(ExpectedConditions.visibilityOfElementLocated(
//                By.xpath(
//                    "//div[@class='ant-modal-header']/div[@class='ant-modal-title' and text()='交易資料']//..//..")
//            ));
//        WebElement insertTransactionButton = wait.until(ExpectedConditions.elementToBeClickable(insertTransactionButtonParent.findElement(By.xpath("//span[contains(text(),'確 定')]//.."))));
//        Actions action = new Actions(webDriver);
//        action.clickAndHold(insertTransactionButton).build().perform();

    taskRunner.addTask(new Task("新增憑證") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        //span 不能點擊 但需要等新增這個按鈕初始化完成
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//button[@type='button']/span[contains(text(),'確 定')]")));
        List<WebElement> buttons = webDriver.findElements(By.tagName("button"));
        WebElement insertTransactionButton = null;
        for (WebElement button : buttons) {
          if ("確 定".equals(button.getText())) {
            if ("交易資料".equals(button.findElement(By.xpath(
                "./../../../../div[@class='ant-modal-header']/div[@class='ant-modal-title']"))
                .getText())) {
              insertTransactionButton = button;
            }
          }
        }
        wait.until(ExpectedConditions.elementToBeClickable(insertTransactionButton)).click();
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.className("ant-modal-content")));
      }
    });

    taskRunner.addTask(new Task("檢查是否成功") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        checkTransactonErrorMessage(wait, response);
      }
    });

    taskRunner.addTask(new Task("選擇付款對象") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectTradingType(wait, tradingType);
      }
    });

    taskRunner.addTask(new Task("選擇名稱") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectTradingName(wait, tradingName);
      }
    });

    taskRunner.addTask(new Task("主旨輸入測試內容") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(
            By.id("REMARK"))).sendKeys(
            genRemark(cateGory, transactionTypeId, tradingType,
                tradingName, certificateType, taxChargeType, uniformInvoiceNo,
                isDeduct, amount));
      }
    });

    taskRunner.addTask(new Task("取得分錄資訊") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        Map<String, Object> responseData = response.getData();
        responseData.put("entryList", getEntryList(wait));
      }
    });
    taskRunner.addTask(new Task("新增傳票工作底稿單") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        //span 不能點擊 但需要等新增這個按鈕初始化完成
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//button[@type='button']/span[contains(text(),'確 定')]")));
        List<WebElement> buttons = webDriver.findElements(By.tagName("button"));
        WebElement insertButton = null;
        for (WebElement button : buttons) {
          if ("確 定".equals(button.getText())) {
            if ("新增零用金收入".equals(button.findElement(By.xpath(
                "./../../../../div[@class='ant-modal-header']/div[@class='ant-modal-title']"))
                .getText())) {
              insertButton = button;
            }
          }
        }
        wait.until(ExpectedConditions.elementToBeClickable(insertButton)).click();
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.className("ant-modal-content")));
      }
    });

    taskRunner.addTask(new Task("檢查是否成功") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        Map<String, Object> responseData = response.getData();
        WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
        setErrorMessage(checkWait, responseData, "新增中", "新增成功");
        checkError(responseData, "新增失敗");
      }
    });

    //錯誤有四種
    // 1.notification(後端邏輯)錯誤
    // 2.message(前端邏輯)錯誤
    // 3.selenium(自動化測試)錯誤
    // 4.javascript(前端程式)錯誤
    taskRunner.addTask(new Task("取得最新的一筆jwsNo回傳") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {

        String jwsNo = findLastJwsNo(wait);
        log.info("新增成功:" + jwsNo);
        response.setRm("新增成功:" + jwsNo);
        Map<String, Object> responseData = response.getData();
        responseData.put("jwsNo", jwsNo);
      }
    });

    return taskRunner.handle(true, keep, request);
  }

  /**
   * 支出建立
   *
   * @param entityId
   * @param cashAccount
   * @param cateGory
   * @param transactionTypeId
   * @param tradingType
   * @param tradingName
   * @param certificateType
   * @param uniformInvoiceNo
   * @param isDeduct
   * @param amount
   * @param taxChargeType
   * @param taxDataType
   * @param isFixedAsset
   * @param keep
   * @param request
   * @return
   * @throws Exception
   */
  @GetMapping("/payable/create")
  private CommonResp<Map<String, Object>> payableCreate(
      @RequestParam(value = "entity") String entityId,
      @RequestParam(value = "cashAccount") String cashAccount,
      @RequestParam(value = "cateGory") String cateGory,
      @RequestParam(value = "transactionTypeId") String transactionTypeId,
      @RequestParam(value = "tradingType") String tradingType,
      @RequestParam(value = "tradingName") String tradingName,
      @RequestParam(value = "certificateType") String certificateType,
      @RequestParam(value = "uniformInvoiceNo") String uniformInvoiceNo,
      @RequestParam(value = "isDeduct", required = false, defaultValue = "否") String isDeduct,
      @RequestParam(value = "amount", required = false, defaultValue = "1000") String amount,
      @RequestParam(value = "taxChargeType", required = false, defaultValue = "應稅") String taxChargeType,
      @RequestParam(value = "taxDataType", required = false, defaultValue = "單一稅別") String taxDataType,
      @RequestParam(value = "isFixedAsset", required = false, defaultValue = "否") String isFixedAsset,
      @RequestParam(value = "keep", required = false, defaultValue = "true") boolean keep,
      HttpServletRequest request
  ) throws Exception {
    final String invoiceNo = genInvoiceNo();
    WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
    TaskRunner taskRunner = new TaskRunner(webDriver, seleniumProperties);
    taskRunner.addTask(new Task("切換到零用金") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        switchToPettyCash(wait);
      }
    });

    taskRunner.addTask(new Task("切換營運主體") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectEntity(wait, entityId);
      }
    });

    taskRunner.addTask(new Task("切換功能") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        clickFunction(wait);
      }
    });

    taskRunner.addTask(new Task("檢查頁面是否正常切換") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        final String PETTYCASH_URL = aCCProperties.getDomain() + "pettyCash";
        if (!PETTYCASH_URL
            .equals(webDriver.getCurrentUrl())) {
          clickFunction(wait);
        }
      }
    });

    taskRunner.addTask(new Task("選擇現金科目") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        selectCashAccount(wait, cashAccount);
      }
    });

    taskRunner.addTask(new Task("切換頁籤") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        switchTab(wait, "支出");
      }
    });

    taskRunner.addTask(new Task("點擊新增") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        //span 不能點擊 但需要等新增這個按鈕初始化完成
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//button[@type='button']/span[contains(text(),'新 增')]")));
        List<WebElement> buttons = webDriver.findElements(By.tagName("button"));
        for (WebElement button : buttons) {
          if ("新 增".equals(button.getText())) {
            button.click();
            break;
          }
        }
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.className("ant-modal-content")));
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.className("ant-modal-body")));
      }
    });

    taskRunner.addTask(new Task("切換支出類型") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
        checkWait.until(ExpectedConditions.elementToBeClickable(By.id("CATEGORY"))).click();
        checkWait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//ul[@role='listbox']/li[contains(text(),'" + cateGory + "')]")))
            .click();
      }
    });
    taskRunner.addTask(new Task("切換交易類型") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        selectTransactionType(wait, transactionTypeId);
      }
    });
    taskRunner.addTask(new Task("點擊新增憑證") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        //span 不能點擊 但需要等新增這個按鈕初始化完成
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//button[@type='button']/span[contains(text(),'新增憑證')]")));
        findElementBySearchText(webDriver, By.tagName("button"), "新增憑證").click();
      }
    });

    taskRunner.addTask(new Task("輸入憑證") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        //台幣
        //憑證類型
        wait.until(ExpectedConditions.elementToBeClickable(By.id("certificateType"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//ul[@role='listbox']/li[contains(text(),'" + certificateType + "')]")))
            .click();

        //是否固定資產
        selectIsFixedAsset(wait, isFixedAsset);

        //憑證號碼
        wait.until(ExpectedConditions.elementToBeClickable(By.id("certificateNo")))
            .sendKeys(invoiceNo);
        //有錯誤訊息 憑證號碼重複之類的警告
        while (webDriver.findElements(By.xpath("//div[@class='ant-message']")).size() > 0) {
          //重算憑證號碼
          String newInvoiceNo = genInvoiceNo();
          wait.until(ExpectedConditions.elementToBeClickable(By.id("certificateNo")))
              .sendKeys(newInvoiceNo);
        }
        //台幣
        if (cashAccount.indexOf("NTD") >= 0) {
          //憑證日期
          if ("發票".equals(certificateType)) {
            setNTDInvoiceTransaction(wait, cashAccount, isDeduct, taxDataType, taxChargeType,
                amount, uniformInvoiceNo);
          } else if ("收據".equals(certificateType)) {
            wait.until(ExpectedConditions.elementToBeClickable(By.id("taxAbleAmount")))
                .sendKeys(amount);
          } else if ("Invoice".equals(certificateType)) {
            wait.until(ExpectedConditions.elementToBeClickable(By.id("totalAmount")))
                .sendKeys(amount);
          }
        } //外幣
        else {
          if ("發票".equals(certificateType)) {
            //是否可扣抵
            selectIsDeduct(wait, isDeduct);
            //外幣未稅金額
            wait.until(ExpectedConditions.elementToBeClickable(By.id("taxAbleAmount")))
                .sendKeys(amount);
          } else if ("收據".equals(certificateType)) {
            wait.until(ExpectedConditions.elementToBeClickable(By.id("totalAmount")))
                .sendKeys(amount);
          } else if ("Invoice".equals(certificateType)) {
            wait.until(ExpectedConditions.elementToBeClickable(By.id("totalAmount")))
                .sendKeys(amount);
          }
          //銷售人統編
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("uniformInvoiceNo")))
              .sendKeys(uniformInvoiceNo);
        }
      }
    });

    //TODO bug 外幣是否固定資產
//        WebElement isFixedAssetElemet = wait
//            .until(ExpectedConditions.elementToBeClickable(By.id("isFixedAsset")));
//        isFixedAssetElemet.click();
//        wait.until(ExpectedConditions.visibilityOf(isFixedAssetElemet
//            .findElement(By.xpath("//..//ul[@role='listbox']/li[text()='" + isFixedAsset + "']"))))
//            .click();

    taskRunner.addTask(new Task("確認新增") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        //span 不能點擊 但需要等新增這個按鈕初始化完成
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//button[@type='button']/span[contains(text(),'確 定')]")));
        List<WebElement> buttons = webDriver.findElements(By.tagName("button"));
        WebElement insertTransactionButton = null;
        for (WebElement button : buttons) {
          if ("確 定".equals(button.getText())) {
            if ("交易資料".equals(button.findElement(By.xpath(
                "./../../../../div[@class='ant-modal-header']/div[@class='ant-modal-title']"))
                .getText())) {
              insertTransactionButton = button;
            }
          }
        }
        wait.until(ExpectedConditions.elementToBeClickable(insertTransactionButton)).click();
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.className("ant-modal-content")));
      }
    });

    taskRunner.addTask(new Task("檢查是否成功新增憑證") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        checkTransactonErrorMessage(wait, response);
      }
    });

    taskRunner.addTask(new Task("選擇付款對象") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectTradingType(wait, tradingType);
      }
    });

    taskRunner.addTask(new Task("選擇名稱") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectTradingName(wait, tradingName);
      }
    });

    taskRunner.addTask(new Task("主旨輸入測試內容") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(
            By.id("REMARK"))).sendKeys(
            genRemark(cateGory, transactionTypeId, tradingType,
                tradingName, certificateType, taxChargeType, uniformInvoiceNo,
                isDeduct, amount)
        );
      }
    });

    taskRunner.addTask(new Task("取得分錄資訊") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        Map<String, Object> responseData = response.getData();
        responseData.put("entryList", getEntryList(wait));
      }
    });

    taskRunner.addTask(new Task("新增傳票工作底稿單") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        //span 不能點擊 但需要等新增這個按鈕初始化完成
        wait.until(ExpectedConditions.elementToBeClickable(
            By.xpath("//button[@type='button']/span[contains(text(),'確 定')]")));
        List<WebElement> buttons = webDriver.findElements(By.tagName("button"));
        WebElement insertButton = null;
        for (WebElement button : buttons) {
          if ("確 定".equals(button.getText())) {
            if ("新增零用金支出".equals(button.findElement(By.xpath(
                "./../../../../div[@class='ant-modal-header']/div[@class='ant-modal-title']"))
                .getText())) {
              insertButton = button;
            }
          }
        }
        wait.until(ExpectedConditions.elementToBeClickable(insertButton)).click();
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.className("ant-modal-content")));
      }
    });

    taskRunner.addTask(new Task("檢查是否成功") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        Map<String, Object> responseData = response.getData();
        WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
        setErrorMessage(checkWait, responseData, "新增中", "新增成功");
        checkError(responseData, "新增失敗");
        if (responseData.containsKey("messageError") || responseData
            .containsKey("notificationError")
            || responseData.containsKey("logError")) {
          throw new Exception("新增失敗");
        }
      }
    });

    taskRunner.addTask(new Task("取得最新的一筆jwsNo回傳") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        Map<String, Object> responseData = response.getData();
        String jwsNo = findLastJwsNo(wait);
        log.info("新增成功:" + jwsNo);
        response.setRm("新增成功:" + jwsNo);
        responseData.put("jwsNo", jwsNo);
      }
    });
    return taskRunner.handle(true, keep, request);
  }

  /**
   * 餘額查詢
   *
   * @param needLogin
   * @param keep
   * @param request
   * @return
   * @throws Exception
   */
  @GetMapping("/balance")
  private CommonResp<Map<String, Object>> balanceQuery(
      @RequestParam(value = "needLogin", defaultValue = "true") boolean needLogin,
      @RequestParam(value = "keep", defaultValue = "true") boolean keep,
      HttpServletRequest request
  ) throws Exception {
    WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
    TaskRunner taskRunner = new TaskRunner(webDriver, seleniumProperties);
    taskRunner.addTask(new Task("切換到零用金") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        if (needLogin) {
          switchToPettyCash(wait);
        }
      }
    });

    taskRunner.addTask(new Task("切換功能") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) {
        clickFunction(wait);
      }
    });

    taskRunner.addTask(new Task("切換頁籤") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        switchTab(wait, "餘額查詢");
      }
    });

    //錯誤有四種
    // 1.notification(後端邏輯)錯誤
    // 2.message(前端邏輯)錯誤
    // 3.selenium(自動化測試)錯誤
    // 4.javascript(前端程式)錯誤
    taskRunner.addTask(new Task("檢查是否成功") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
        Map<String, Object> responseData = response.getData();
        setErrorMessage(checkWait, responseData, "新增中", "新增成功");
        checkError(responseData, "餘額查詢失敗");

        if (responseData.containsKey("messageError") || responseData
            .containsKey("notificationError")
            || responseData.containsKey("logError")) {
          throw new Exception("刪除失敗");
        }
      }
    });

    return taskRunner.handle(needLogin, keep, request);
  }

  /**
   * 刪除
   *
   * @param entityId
   * @param cashAccount
   * @param function
   * @param jwsNo
   * @param needLogin
   * @param keep
   * @param request
   * @return
   * @throws Exception
   */
  @GetMapping("/delete")
  private CommonResp<Map<String, Object>> delete(
      @RequestParam(value = "entity") String entityId,
      @RequestParam(value = "cashAccount") String cashAccount,
      @RequestParam(value = "function") String function,
      @RequestParam(value = "jwsNo") String jwsNo,
      @RequestParam(value = "needLogin", defaultValue = "true") boolean needLogin,
      @RequestParam(value = "keep", defaultValue = "true") boolean keep,
      HttpServletRequest request
  ) throws Exception {
    WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
    TaskRunner taskRunner = new TaskRunner(webDriver, seleniumProperties);
    taskRunner.addTask(new Task("切換到零用金") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        if (needLogin) {
          switchToPettyCash(wait);
        }
      }
    });

    taskRunner.addTask(new Task("選擇營運主體") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectEntity(wait, entityId);
      }
    });

    taskRunner.addTask(new Task("選擇現金科目") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectCashAccount(wait, cashAccount);
      }
    });

    taskRunner.addTask(new Task("切換頁籤") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        switchTab(wait, function);
      }
    });

    taskRunner.addTask(new Task("點擊刪除按鈕") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        WebElement tr = wait.until(ExpectedConditions
            .visibilityOfElementLocated(By.xpath("//td[text()='" + jwsNo + "']//..")));
        WebElement tableBody = wait
            .until(
                ExpectedConditions
                    .visibilityOfElementLocated(By.className("ant-table-body")));
        int x = webDriver.findElement(By.xpath("//span[text()='刪除']")).getLocation().getX();
        int y = tr.getLocation().getY();
        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        //滑動table到最右邊
        jse.executeScript("arguments[0].scroll(arguments[1],arguments[2])", tableBody, x, y);
        //點擊刪除
        jse.executeScript(
            "Array.from(document.querySelectorAll('td')).find(e=>e.textContent === '" + jwsNo
                + "').parentElement.getElementsByTagName('button')[1].click()");
        //點擊確定
        jse.executeScript(
            "document.getElementsByClassName(\"ant-popover-buttons\")[0].getElementsByTagName('button')[1].click()");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("main")));
      }
    });

    taskRunner.addTask(new Task("檢查是否成功") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        Map<String, Object> responseData = response.getData();
        WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
        setErrorMessage(checkWait, responseData, "刪除中", "刪除成功");
        checkError(responseData, "刪除失敗");
      }
    });

    //錯誤有四種
    // 1.notification(後端邏輯)錯誤
    // 2.message(前端邏輯)錯誤
    // 3.selenium(自動化測試)錯誤
    // 4.javascript(前端程式)錯誤
    taskRunner.addTask(new Task("判斷是否刪除") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        try {
          WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
          //message錯誤處理
          messageErrorHandle(checkWait, "刪除中", "刪除成功");
          //notification錯誤處理
          notificationErrorHandle(checkWait);
          log.info("刪除成功:" + jwsNo);
        } catch (Exception e) {
          //若找不到則表示刪除成功
          boolean success =
              webDriver.findElements(By.xpath("//td[text()='" + jwsNo + "']")).size() <= 0;
          if (!success) {
            throw new Exception("刪除失敗");
          }
        }
      }
    });
    return taskRunner.handle(needLogin, keep, request);
  }


  /**
   * 結案
   *
   * @param entityId
   * @param cashAccount
   * @param function
   * @param jwsNo
   * @param needLogin
   * @param keep
   * @param request
   * @return
   * @throws Exception
   */
  @GetMapping("/close")
  private CommonResp<Map<String, Object>> close(
      @RequestParam(value = "entity") String entityId,
      @RequestParam(value = "cashAccount") String cashAccount,
      @RequestParam(value = "function") String function,
      @RequestParam(value = "jwsNo") String jwsNo,
      @RequestParam(value = "needLogin", defaultValue = "true") boolean needLogin,
      @RequestParam(value = "keep", defaultValue = "true") boolean keep,
      HttpServletRequest request
  ) throws Exception {
    WebDriverWait wait = new WebDriverWait(webDriver, PAGE_LOADING_SECOND);
    TaskRunner taskRunner = new TaskRunner(webDriver, seleniumProperties);
    taskRunner.addTask(
        new Task("登入切換到零用金") {
          @Override
          public void task(CommonResp<Map<String, Object>> response) throws Exception {
            if (needLogin) {
              switchToPettyCash(wait);
            }
          }
        }
    );

    taskRunner.addTask(
        new Task("目錄樹切換功能") {
          @Override
          public void task(CommonResp<Map<String, Object>> response) {
            clickFunction(wait);
          }
        }
    );

    taskRunner.addTask(
        new Task("麵包屑切換頁籤") {
          @Override
          public void task(CommonResp<Map<String, Object>> response) throws Exception {
            switchTab(wait, function);
          }
        }
    );

    taskRunner.addTask(new Task("選擇營運主體") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectEntity(wait, entityId);
      }
    });

    taskRunner.addTask(new Task("選擇現金科目") {
      @Override
      public void task(CommonResp<Map<String, Object>> response) throws Exception {
        selectCashAccount(wait, cashAccount);
      }
    });

    taskRunner.addTask(
        new Task("勾選checkbox") {
          @Override
          public void task(CommonResp<Map<String, Object>> response) throws Exception {
            JavascriptExecutor jse = (JavascriptExecutor) webDriver;
            jse.executeScript(
                "Array.from(document.querySelectorAll('td')).find(e=>e.textContent === '" + jwsNo
                    + "').parentElement.getElementsByTagName('input')[0].click()");
          }
        }
    );

    taskRunner.addTask(
        new Task("送出審核") {
          @Override
          public void task(CommonResp<Map<String, Object>> response) throws Exception {
            wait.until(
                ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='送出審核']/..")))
                .click();
            //確定
            wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(
                    "//div[@class='ant-popover-buttons']/button[@class='ant-btn ant-btn-primary ant-btn-sm']")))
                .click();

          }
        }
    );

    //錯誤有四種
    // 1.notification(後端邏輯)錯誤
    // 2.message(前端邏輯)錯誤
    // 3.selenium(自動化測試)錯誤
    // 4.javascript(前端程式)錯誤
    taskRunner.addTask(
        new

            Task("檢查是否成功") {
              @Override
              public void task(CommonResp<Map<String, Object>> response) throws Exception {
                WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
                Map<String, Object> responseData = response.getData();
                setErrorMessage(checkWait, responseData, "新增中", "新增成功");
                checkError(responseData, "查詢失敗");

                if (responseData.containsKey("messageError") || responseData
                    .containsKey("notificationError")
                    || responseData.containsKey("logError")) {
                  log.info("log error");
                  throw new Exception("刪除失敗");
                }
              }
            }
    );
    return taskRunner.handle(needLogin, keep, request);
  }

  private WebElement findElementBySearchText(WebDriver webDriver, By by, String text) {
    List<WebElement> list =
        webDriver.findElements((by)).stream().filter(element -> text.equals(element.getText()))
            .collect(Collectors.toList());
    return list.size() > 0 ? list.get(0) : null;
  }
}

