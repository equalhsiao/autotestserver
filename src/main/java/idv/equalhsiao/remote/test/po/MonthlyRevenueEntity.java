package idv.equalhsiao.remote.test.po;

import java.beans.Transient;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "monthlyrevenue")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class MonthlyRevenueEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String stockId;
	private String stockName;
	private Integer month;
	private String industry;
	private Timestamp announcementTime;
	private Integer monthlyRevenue;
	private Integer preMonthlyRevenue;
	private Integer preYearMonthlyRevenue;
	private Float preMonthlyComparePrecentage;
	private Float preYearComparePrecentage;
	private Integer revenueGrandTotal;
	private Integer preYearRevenueGrandTotal;
	private Float preMonthlyCompareGrandTotalPrecentage;
	private String remark;
	private String year;
}
