package idv.equalhsiao.remote.test.controller;

import idv.equalhsiao.remote.test.config.SeleniumConfiguration;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import idv.equalhsiao.remote.test.config.CommonResp;
import idv.equalhsiao.remote.test.config.SeleniumProperties;
import idv.equalhsiao.remote.test.config.UserProperties;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "TL-WR740N")
@Slf4j
public class TLwr740nController {

  private UserProperties userProperties;
  private SeleniumProperties seleniumProperties;

  @Autowired
  public TLwr740nController(UserProperties userProperties, SeleniumProperties seleniumProperties) {
    this.userProperties = userProperties;
    this.seleniumProperties = seleniumProperties;
  }


  @GetMapping("/restart")
  private CommonResp<Map<String, String>> restart() {
    CommonResp<Map<String, String>> response = new CommonResp<Map<String, String>>();
    WebDriver webDriver = new ChromeDriver(SeleniumConfiguration.setWebDriver(seleniumProperties));
    Map<String, String> map = new HashMap<String, String>();
    String username = userProperties.getUsername();
    String password = userProperties.getPassword();
    String initPage = userProperties.getInitpage();
    String ip = userProperties.getIp();
    String fullUrl = "http://" + username + ":" + password + "@" + ip + initPage;
    log.info("full url:" + fullUrl);
    webDriver.get(fullUrl);
//		String windowHandle = webDriver.getWindowHandle();
    webDriver.switchTo().frame("bottomLeftFrame");
    WebDriverWait wait = new WebDriverWait(webDriver, 10);
    WebElement parentMenuBtn = wait
        .until(ExpectedConditions.visibilityOfElementLocated(By.id("ol43")));
    parentMenuBtn.click();
    parentMenuBtn.findElement(By.tagName("a")).click();
    WebElement menuBtn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ol49")));
    menuBtn.click();
    menuBtn.findElement(By.tagName("a")).click();
//		webDriver.switchTo().parentFrame().switchTo().parentFrame().switchTo().frame("mainFrame");
    webDriver.switchTo().defaultContent().switchTo().frame("mainFrame");
    WebElement rebootBtn = wait
        .until(ExpectedConditions.visibilityOfElementLocated(By.id("reboot")));
    rebootBtn.click();
    webDriver.switchTo().alert().accept();

    log.info("result:" + webDriver.getPageSource());
    webDriver.close();
    webDriver.quit();
    response.setData(map);
    return response;
  }
}
