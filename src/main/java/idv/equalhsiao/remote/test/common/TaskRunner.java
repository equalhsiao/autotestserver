package idv.equalhsiao.remote.test.common;

import idv.equalhsiao.remote.test.config.CommonResp;
import idv.equalhsiao.remote.test.config.SeleniumConfiguration;
import idv.equalhsiao.remote.test.config.SeleniumProperties;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

@Slf4j
public class TaskRunner {

  private List<Task> taskList = new ArrayList<>();

  private SeleniumProperties seleniumProperties;

  private WebDriver webDriver;
  @Value("${page.loading.second}")
  private int pageLoadSecond;

  public TaskRunner(WebDriver webDriver, SeleniumProperties seleniumProperties) {
    this.webDriver = webDriver;
    this.seleniumProperties = seleniumProperties;
  }

  public TaskRunner() {
  }

  /**
   * 產生webDriver 設定選項
   */
  private void buildWebDriver() {
    log.info("building...");
    ChromeOptions chromeOptions = SeleniumConfiguration.setWebDriver(seleniumProperties);
    webDriver = new ChromeDriver(chromeOptions);
    webDriver.manage().timeouts().pageLoadTimeout(pageLoadSecond, TimeUnit.SECONDS);
  }


  /**
   * 初始化webDriver若不存在則重新new出
   */
  private void initWebDriver() {
    try {
      if (webDriver == null) {
        buildWebDriver();
      } else {
        SessionId sessionid = ((RemoteWebDriver) webDriver).getSessionId();
        if (sessionid == null) {
          buildWebDriver();
        }
      }
    } catch (WebDriverException e) {
      buildWebDriver();
    } finally {
      if (webDriver != null) {
        webDriver.manage().window().maximize();
      }
    }
  }

  /**
   * 截圖
   *
   * @param request
   * @throws Exception
   */
  private void saveScreenshot(HttpServletRequest request) throws Exception {
    SimpleDateFormat sdf = new SimpleDateFormat("E yyyyMMddhhmmss");
    String time = sdf.format(new Date());
    File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
    FileUtils.copyFile(scrFile, new File("/tmp" + request.getRequestURI() + time + ".png"));
  }

  /**
   * 找尋前端notification錯誤訊息
   *
   * @param wait
   * @return
   * @throws Exception
   */
  private String notificationErrorHandle(WebDriverWait wait) throws Exception {
    try {
      WebElement notificationElement = null;
      try {
        notificationElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//div[@class='ant-notification-notice-description']")));
      } catch (TimeoutException timeoutException) {
        return "";
      }
      //找到錯誤訊息表示新增失敗
      if (notificationElement.isDisplayed()) {
//        throw new Exception(notificationElement.getText());
        return notificationElement.getText();
      }
    } catch (Exception exception) {
      return exception.getMessage();
//      throw new Exception(exception.getMessage());
    }
    return "";
  }

  /**
   * 沒有驗證message的方法
   *
   * @param wait
   * @param responseData
   * @throws Exception
   */
  private void setErrorMessage(WebDriverWait wait, Map<String, Object> responseData)
      throws Exception {
    //notification錯誤處理
    String notificationError = notificationErrorHandle(wait);
    if (!StringUtils.isEmpty(notificationError)) {
      responseData.put("notificationError", notificationError);
    }
    //瀏覽器錯誤
    List<LogEntry> logs = webDriver.manage().logs().get(LogType.BROWSER).getAll();
    //本機環境會有警告 但Level為SERVER的警告 所以本機略過
    if (logs.size() > 0) {
      List<LogEntry> logFilterList = logs.stream().filter(
          log -> "SEVERE".equals(log.getLevel().getName()) && !log.getMessage()
              .startsWith("Warning"))
          .collect(Collectors.toList());
      if (logFilterList.size() > 0) {
        responseData
            .put("logError", logFilterList);
      }
    }
  }

  /**
   * 判斷是否包含key值
   *
   * @param responseData
   * @param key
   * @return
   */
  private boolean containsKey(Map<String, Object> responseData, String key) {
    if (responseData.containsKey(key)) {
      log.info(key + ":" + responseData.get(key));
      return true;
    }
    return false;
  }

  /**
   * 有任何錯誤則印出後拋出錯誤訊息
   *
   * @param responseData
   * @param errorMessage
   * @throws Exception
   */
  private void checkError(Map<String, Object> responseData, String errorMessage) throws Exception {

    boolean logError = false;
    if (responseData.containsKey("logError")) {
      logError = true;
      ((List<LogEntry>) responseData.get("logError")).stream().forEach(
          e -> {
            log.info("logError:" + e.getMessage());
          }
      );
    }
    //message notification browser log 其一錯誤
    if (containsKey(responseData, "messageError") || containsKey(responseData,
        "notificationError") || logError) {
      throw new Exception(errorMessage);
    }
  }

  public void addTask(Task task) {
    taskList.add(task);
  }

  public CommonResp<Map<String, Object>> handle(boolean needLogin, boolean keep,
      HttpServletRequest request)
      throws Exception {
    initWebDriver();
    CommonResp<Map<String, Object>> response = new CommonResp<Map<String, Object>>();
    Map<String, Object> responseData = new HashMap<String, Object>();
    response.setData(responseData);
    //第一層try catch 抓 selenium錯誤
    try {
      for (Task task : taskList) {
        task.run(response);
      }
    } catch (Exception e) {
      saveScreenshot(request);
      response.setRc("9999");
      response.setRm("發生錯誤");
      responseData.put("seleniumError", e.getMessage());
      e.printStackTrace();
      //第二層 try catch 抓 notification 與 log 錯誤 ( message 只在點擊新增後抓 )
      try {
        log.info("selenium error catch notification error and browser log error");
        WebDriverWait checkWait = new WebDriverWait(webDriver, 5);
        setErrorMessage(checkWait, responseData);
        checkError(responseData, "新增失敗");
      } catch (Exception ex) {
        response.setRc("9999");
        response.setRm(ex.getMessage());
      }
    } finally {
      //若要繼續 不關閉視窗
      if (!keep) {
        webDriver.close();
        webDriver.quit();
      }
    }
    return response;
  }
}
