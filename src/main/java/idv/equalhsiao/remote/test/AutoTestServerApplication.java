package idv.equalhsiao.remote.test;

import idv.equalhsiao.remote.test.config.ACCProperties;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class AutoTestServerApplication implements CommandLineRunner {


  @Autowired
  private ACCProperties accProperties;

  public static void main(String[] args) {
    SpringApplication.run(AutoTestServerApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    String homeDirectory = System.getProperty("user.home");
    //spring boot 啟動後是否執行測試
    if (accProperties.getInitStartTest()) {
      log.info("run start");
      boolean isWindows = System.getProperty("os.name")
          .toLowerCase().startsWith("windows");
      Process process;
      String command;
      //     template請參考   https://developer.aliyun.com/mirror/npm/package/newman-reporter-htmlfull2
      if (isWindows) {
        command = "cmd.exe /C newman run newman\\ACC_selenium_test(demo).postman_collection.json "
            + "-e newman\\ACC.postman_environment.json "
            + "-r htmlextra,cli,json,junit "
//            + "---reporter-htmlfull2-export "
//            + "newman\\newman-reporter-htmlfull-master\\examples\\original\\template-default.html "
//            + "--reporter-htmlfull2-template "
//            + "newman\\newman-reporter-htmlfull-master\\templates\\original\\template-default.hbs "
        ;
        process = Runtime.getRuntime()
            .exec(command);
      } else {
        command = "newman run newman/ACC_selenium_test(demo).postman_collection.json "
            + "-e newman/ACC.postman_environment.json "
            + "-r htmlextra,cli,json,junit "
//            + "-r html,cli,json,junit "
//            + "--reporter-html-export ./newman/newman-reporter-htmlfull-master/examples/original/template-default-colored.html "
//            + "--reporter-html-template ./newman/newman-reporter-htmlfull-master/templates/original/template-default-colored.hbs "
        ;
        // for container
        process = Runtime.getRuntime()
            .exec(command);

      }
      log.info(command);
      StreamGobbler streamGobbler =
          new StreamGobbler(process.getInputStream(), System.out::println);
      Executors.newSingleThreadExecutor().submit(streamGobbler);
      int exitCode = process.waitFor();
      log.info("exit code:" + exitCode);
      log.info("run end");
    }
  }

  private static class StreamGobbler implements Runnable {

    private InputStream inputStream;
    private Consumer<String> consumer;

    public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
      this.inputStream = inputStream;
      this.consumer = consumer;
    }

    @Override
    public void run() {
      new BufferedReader(new InputStreamReader(inputStream)).lines()
          .forEach(consumer);
    }
  }
}
