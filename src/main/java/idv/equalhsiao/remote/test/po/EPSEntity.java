package idv.equalhsiao.remote.test.po;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "eps")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class EPSEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String stockId;
	private String stockFullName;
	private String industry;
	private String year;
	private Float eps;
	private Long operatingIncome;
	private Long businessInterest;
	private Long nonOperatingIncome;
	private Long netIncome;
	private Date announcementDate;
	private Short season;

}
