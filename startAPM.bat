docker pull docker.elastic.co/elasticsearch/elasticsearch:7.9.2
docker pull docker.elastic.co/apm/apm-server:7.9.2
docker pull docker.elastic.co/kibana/kibana:7.9.2
"C:\Program Files\Git\bin\bash.exe" --login -c "docker stop apm-server && docker rm apm-server"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker stop kibana && docker rm kibana"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker stop elasticsearch && docker rm elasticsearch"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker network create elasticNetwork --driver=bridge"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e 'discovery.type=single-node' --network elasticNetwork -v /${PWD}/elasticsearch/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml docker.elastic.co/elasticsearch/elasticsearch:7.9.2"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker run -d --name apm-server --user=apm-server -p 8200:8200  --network elasticNetwork docker.elastic.co/apm/apm-server:7.9.2 --strict.perms=false  -e -E output.elasticsearch.hosts=["elasticsearch:9200"]"
"C:\Program Files\Git\bin\bash.exe" --login -c "docker run -d --name kibana -p 5601:5601 --network elasticNetwork -v /${PWD}/kibana/kibana.yml:/usr/share/kibana/config/kibana.yml docker.elastic.co/kibana/kibana:7.9.2"
::elasticsearch的硬碟容量預設給很小所以要加大
::解開被鎖住只能讀的index
::"C:\Program Files\Git\bin\bash.exe" --login -c " HEALTHCHECK --interval=5m --timeout=3s CMD curl -X PUT -H "Content-Type: application/json" http://192.168.99.100:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'|| exit 1"
::設置硬碟容量大小為50G,剩下20G則鎖住
::"C:\Program Files\Git\bin\bash.exe" --login -c "HEALTHCHECK --interval=5m --timeout=3s CMD curl -X PUT "http://192.168.99.100:9200/_cluster/settings?pretty" -H 'Content-Type: application/json' -d' { "transient": { "cluster.routing.allocation.disk.watermark.low": "50gb", "cluster.routing.allocation.disk.watermark.high": "20gb", "cluster.routing.allocation.disk.watermark.flood_stage": "10gb", "cluster.info.update.interval": "1m"}}' || exit 1"
pause