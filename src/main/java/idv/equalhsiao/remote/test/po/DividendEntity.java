package idv.equalhsiao.remote.test.po;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "dividend")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class DividendEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String stockId;
	private String stockName;
	private String year;
	private Float cashDividend;
	private Float stockDividend;
	private Float capitalDividend;
	private Long dividendTotal;
	private Float increaseCashDividend;
	private Float increaseStockDividend;
	private Float increaseCapitalDividend;
	private Long shareTotal;
	private Float stockPrice;
	private Float totalYieldRate;
	private Timestamp announcementDate;
	private String dividendYear;
}
