package idv.equalhsiao.remote.test.dto;

import java.util.Date;

import lombok.Data;

@Data
public class DividendDto {
	private String stockId;
	private String stockName;
	private Float closePrice;
	private Integer volume;
	private String year;
//	private Float cashDividend;
//	private Float stockDividend;
//	private Float capitalDividend;
	private Float totalCashDividend;//現金股利
	private Float totalStockDividend;//股票股利
	private Float totalDividend;//總股利
	private Float totalYieldRate;//殖利率
	private Date announcementDate;//公布時間
	private String dividendYear;//股利分配季度
//	private Float eps;//今年EPS
//	private Float preYearEPS;//去年EPS
//	private Float nonOperatingPercentage;//業外收入占比
//	private Float payoutRatio; //股票分配率
}
