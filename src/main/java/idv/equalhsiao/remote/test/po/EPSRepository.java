package idv.equalhsiao.remote.test.po;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EPSRepository extends JpaRepository<EPSEntity, Integer> {

	EPSEntity findByStockIdAndYearAndSeason(String stockId, String year, Short season);

	List<EPSEntity> findByStockIdAndYearIn(String stockId, List<String> year);

	List<EPSEntity> findByStockIdAndYear(String stockId, String year);

	List<EPSEntity> findByStockIdAndYearAndSeasonInOrderBySeasonDesc(String stockId, String year, List<Short> season);

}
