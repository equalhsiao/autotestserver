package idv.equalhsiao.remote.test.po;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<StockEntity, Integer> {

	StockEntity findByStockIdAndStockNameAndIndustry(String stockId, String stockName, String industry);

	StockEntity findByStockId(String stockId);

}
