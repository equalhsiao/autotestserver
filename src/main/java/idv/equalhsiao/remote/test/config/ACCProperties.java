package idv.equalhsiao.remote.test.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "spring.acc")
public class ACCProperties {
    private String domain;
    private String account;
    private String password;
    private Boolean initStartTest;
}
