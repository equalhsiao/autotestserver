package idv.equalhsiao.remote.test.po;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "balancesheet")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class BalanceSheetEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String stockId;
	private String stockName;
	private Long currentAssets;
	private Long nonCurrentAssets;
	private Long totalAssets;
	private Long currentLiabilities;
	private Long nonCurrentLiabilities;
	private Long totalLiabilities;
	private Long capitalStock;
	private Long capitalReserve;
	private Long retainedSurplus;
	private Long otherRights;
	private Long treasuryStock;
	private Long attributableParentCompanyTotalEquity;
	private Long nonControllingInterests;
	private Long totalEquity;
	private Long cancelCapital;
	private Long preRecepitShares;
	private Long subsidiaryTreasuryStock;
	private Float referenceShareValue;
	private String year;
	private Short season;
	private Timestamp announcementDate;
}
