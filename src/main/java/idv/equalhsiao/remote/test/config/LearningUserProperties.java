package idv.equalhsiao.remote.test.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "spring.learning")
public class LearningUserProperties {

	private String username;
	private String password;
	private String ip;
}
